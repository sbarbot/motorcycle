.\" Manpage for Motorcycle 1.0.0.
.\" Contact sbarbot@usc.edu to correct errors or typos.
.TH man 1 "Nov 28, 2020" "1.0.0" "motorcycle man page"
.SH NAME
motorcycle \- Cycles de Terremotos. Evaluates the evolution of slip on multiple parallel faults using the spectral integral method with the radiation damping approximation.
.SH SYNOPSIS

motorcycle [-h] [--dry-run] [--help] [--export-netcdf] [--epsilon] [--friction-law ( 1 | 2 | 3)] [--maximum-step] [--maximum-iterations] [--version]

.SH DESCRIPTION

Motorcycle computes the evolution of slip on multiple rate-and-state friction faults using the spectral boundary integral method with the radiation damping approximation.

.SH OPTIONS

.TP
.B \-h
print a short message and abort calculation
.TP
.B \-\-dry-run
write lightweight information files and abort calculation
.TP
.B \-\-help
print a short message and abort calculation
.TP
.B \-\-epsilon [1e-6]
set the relative accuracy of the 4/5th order Runge-Kutta integration method
.TP
.B \-\-export-netcdf
export time series of instantaneous velocity in a GMT compatible netcdf file
.TP
.B \-\-export-state
export the state vector to restart the simulation from current point.
.TP
.B \-\-friction-law ( 1 | 2 | 3 )
select the type of friction law [default: 1]
       1: multiplicative   form of rate-state friction (Barbot, 2019)
       2: additive         form of rate-state friction (Ruina, 1983)
       3: arcsinh          form of rate-state friction (Rice & Benzion, 1996)
.TP
.B \-\-maximum-step [Inf]
set the maximum time step
.TP
.B \-\-maximum-iterations [1000000]
set the maximum number of iterations
.TP
.B \-\-verbose
use --verbose=1 to reduce data sent to the standard output.

.SH ENVIRONMENT

The main programs are parallelized with MPI. Calling the programs with

.nf
mpirun -n 4 motorcycle-3d-ratestate
.fi

controls the number of MPI threads. The number of threads must be between 1 and N2, the number of rows of each fault.

.SH "INPUT PARAMETERS"

The lines starting with the '#' symbol are commented.

.TP
.B output directory (wdir)
All output files are written to the specified directory, including observation patches, observation volumes, observation points and netcdf files.

.TP
.B elastic moduli (lambda, mu)
The uniform Lame parameter (lambda) and the rigidity (mu). For the Earth, typical values are lambda=mu=30 GPa. All physical quantities are assumed in SI units (meter, Pascal, second).

.TP
.B time interval
Refers to the duration of the calculation in SI units (s), for example 3.15e7 for one year.

.TP
.B number of faults
The number of parallel faults involved. The center of the first fault is at position x1=x2=x3=0.

.TP
.B grid dimension (N1,N2)
The dimension of rectangular faults, where N1 is in the strike direction and N2 is in the dip direction. N1 must be a factor of two to facilitate the in-place real-to-complex Fourier transform. The number of fault patches is N1*N2 times the number of faults.

.TP
.B sampling (dx1,dx2)
The sampling size along the strike and dip directions, respectively. There is no numerical sampling in the fault-perpendicular direction.

.TP
.B distance from fault 1
The program expects physical properties for each patch of each fault. When multiple parallel faults are present, all but the first fault requires the distance from the first fault. This parameter shall not be provided when only one fault is present.

.TP
.B patch properties
The physical properties for each patch of each fault, in a sequence where the fast index is in the strike direction (N1). The properties are

# n tau0 mu0 sig a b L Vo G/(2Vs) Vl rake Dirichlet

where tau0 is the initial stress, mu0 and sig are the static coefficient of friction and the effective normal stress, a and b are the dynamic friction coefficient of rate-and-state friction, L is the characteristic weakening distance, Vo is the reference velocity, G/(2Vs) is the radiation damping coefficient, Vl is the loading rate, for example 1e-9m/s, rake is the loading direction relative to axis x1, and Dirichlet is a boolean ("T" or "F") to impose the velocity as a boundary condition. When tau0<0, the initial stress is set to the value that makes the fault slip at the velocity Vl. Prefix subsequent faults by their position x3 relative to fault 1.

.TP
.B number of observation patches
The number of patch element that will be monitored during the calculation. For these patches, the time series of dynamic variables and their time derivatives will be exported in wdir/patch-fault-01-00128-00129.dat, where 01, 00128, and 00129 will be substituted with the fault index, and the patch indices i1 and i2. These time series will include slip components, traction components, state variables, the log10 of the instantaneous velocity. The following columns of the file will contain the time derivatives of these variables. If the number is positive, this must be followed by

# n fault i2 rate

where fault is the fault index, i2 is the patch index, and rate is the sampling rate. A sampling rate of 1 exports all time steps.

.TP
.B number of events
Must be zero (not implemented.)

.SH "CALLING SEQUENCE"
The main program is run with OpenMP.

OMP_NUM_THREADS=4 motorcycle-ap-ratestate wdir/in.param

.SH "PHYSICAL UNITS"

All physical quantities are assumed to be in SI units (meter, Pascal, second). A good practice is to use MPa instead of Pa for the effective normal stress and initial shear stress.

.SH "REFERENCES"

Barbot S., "Modulation of fault strength during the seismic cycle by grain-size evolution around contact junctions", Tectonophysics, j.tecto.2019.05.004, 2019.

Barbot S., "A spectral boundary‐integral method for quasi‐dynamic ruptures of multiple parallel faults". Bull. Seism. Soc. Am., 10.1785/0120210004, 2021.

Nie S. and Barbot S., "Seismogenic and tremorgenic slow slip near the stability transition of frictional sliding". Earth and Planetary Science Letters, 10.1016/j.epsl.2021.117037, 2021.

.SH "BENCHMARKS"

Jiang J., Erickson B.A., Lambert V.R., Ampuero J.P., Ando R., Barbot S.D., Cattania C., Zilio L.D., Duan B., Dunham E.M. and Gabriel A.A., "Community‐driven code comparisons for three‐dimensional dynamic modeling of sequences of earthquakes and aseismic slip". J. Geophys. Res., 10.1029/2021JB023519, 2022.

.SH AUTHOR
Sylvain Barbot (sbarbot@usc.edu), 2020

.SH COPYRIGHT

MOTORCYCLE is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

MOTORCYCLE is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.

