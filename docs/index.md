(sec:index)=
# Motorcycle

```{image} motorcycle-icon.png
:alt: Motorcycle
:width: 25%
:align: center
```

# AUTHOR
Sylvain Barbot (<sbarbot@usc.edu>)

# About *Motorcycle*

 **Motorcycle** stands for *Cycles de Terremotos*. It computes slip evolution on multiple parallel faults during the seismic cycle using the spectral integral method. Slip evolution is controlled by rate- and state-dependent friction with radiation damping.

# APPLICATIONS

 The method targets the simulation of earthquake ruptures, afterslip, and slow-slip events during seismic cycles to explore fault dynamics. The code is written in **Fortran90** with **OpenMP** and/or **MPI parallelism**.

 Various codes corresponds to the two-dimensional anti-plane and in-plane strain approximations whereby the faults are one-dimensional planes and to three-dimensional models whereby the faults are two-dimensional planes:

  * `motorcycle-ap-ratestate-serial` computes simulations of seismic cycles within the anti-plane strain approximation with **OpenMP parallelism**.
  * `motorcycle-ap-ratestate-bath-serial` computes simulations of seismic cycles within the anti-plane strain approximation including thermal effects with **OpenMP parallelism**.
  * `motorcycle-ps-ratestate-serial` computes simulations of seismic cycles within the in-plane strain approximation with **OpenMP parallelism**.
  * `motorcycle-3d-ratestate` computes simulations of seismic cycles on finite parallel faults in an elastic space with **MPI parallelism**.
  * `motorcycle-3d-ratestate-bath` computes simulations of seismic cycles on finite parallel faults in an elastic space with thermal effect with **MPI parallelism**.

# SOURCE
The code is organized in the following directory tree structure

        README
        +---2d/
            +---antiplane/
                |   makefile
                +---src-serial/
                +---build/
                +---examples/
                   +---tutorials/
            +---planestrain/
                |   makefile
                +---src-serial/
                +---build/
                +---examples/
                    +---tutorials/
        +---3d/
            makefile
            +---src/
            +---build/
            +---examples/
                +---tutorials/
        +---docs/
        +---man/

The `src` and `src-serial` directories contain the Fortran code. Running the corresponding `makefile` in the parent directory places the binaries in the `build` directory. Example input files are in the `tutorials` directories. All other examples are for development, test, and research purposes and are not guaranteed to work well.

The code documentation is available as a manpage when the `man` directory is in the `MANPATH` environment variable. Load it with

    man motorcycle-ap-ratestate
    man motorcycle-ps-ratestate
    man motorcycle-3d-ratestate

A markdown documentation is in the `docs` directory. Code documentation is available with *doxygen* and the HTML version can be generated with

    doxygen .doxygen

# SYSTEM REQUIREMENTS
The code compilation requires

  * fortran compiler (gcc-9 and above)
  * netcdf and netcdff libraries
  * fftw3 library
  * openmpi library (for motorcycle-3d-ratestate)

# DOCUMENTATION
  * Markdown in `docs`
  * Man page in `man`
  * doxygen generated from `.doxygen` files
  * <https://motorcycle.readthedocs.io>

# COMMUNITY GUIDELINES

Contribute to the software by reporting issues, requesting new features, or suggesting improvements at <https://bitbucket.org/sbarbot/motorcycle/issues>. For support and third parties wishing to contribute to software development, contact Sylvain Barbot at <sbarbot@usc.edu>.

# VISUALIZATION

The simulations produce output files that can be visualized with the Generic Mapping Tools version 4.5 and above (version 6 available at <http://gmt.soest.hawaii.edu/doc/latest/gmt.html>). Simple ASCII files can be visualized efficiently with gnuplot (<http://www.gnuplot.info/>).

        ,
     .-/c-.,::
     (_)''==(_)

# License

*Motorycle* is published under the GNU GPL v3.

---

# Table of Contents

```{toctree}
---
maxdepth: 0
---
motorcycle-3d-ratestate.md
motorcycle-3d-ratestate-example.md
motorcycle-ap-ratestate.md
motorcycle-ap-ratestate-example.md
motorcycle-ps-ratestate.md
motorcycle-ps-ratestate-example.md
references.md
```
