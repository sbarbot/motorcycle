---
title: 'Motorcycle: A spectral boundary-integral method for seismic cycles on multiple faults'
tags:
  - fault dynamics
  - friction
  - geophysics
  - Fortran90
authors:
  - name: Sylvain Barbot
    orcid: 0000-0003-4257-7409
    affiliation: 1
affiliations:
 - name: University of Southern California
   index: 1
date: 19 July 2022
bibliography: paper.bib
# docker run --rm --volume $PWD:/data --user $(id -u):$(id -g) --env JOURNAL=joss openjournals/inara
---

# Summary

Numerical simulations of seismic cycles constitute a useful tool to test the implications of various constitutive friction laws, materials properties, and boundary conditions. A unique challenge of numerical models of fault dynamics is the resolution of a wide range of time and length scales, going from milliseconds during seismic ruptures to years during seismic quiescence with a rupture front spanning a few meters to fault slip distributed over multiple kilometers. A well-suited approach for this problem is the boundary integral method [@liu+rice07; @segall+bradley12; @barbot19b; @ozawa+ando21; @wang+barbot23], as the elastic medium is captured by appropriate Green's functions, and only the fault interface must be sampled numerically, resulting in orders of magnitude reduction in computational burden [@li+22], while still allowing realistic fault geometry [@li+liu16; @li+liu17; @sathiakumar+20]. Using the spectral boundary integral method [@lapusta+liu09] reduces the numerical complexity even further, allowing exploration of increasingly complex rheological models [@barbot+12; @miyake+noda19; @noda22; @gauriau+23]. However, the approach is often limited to a single fault [@romanet+ozawa22]. Here, we provide a suite of numerical modeling software to simulate seismic cycles on multiple parallel faults combining the efficiency of Fourier methods and the complexity of an interacting fault network [@barbot21].

The models include semi-infinite faults in conditions of two-dimensional anti-plane or in-plane strain, or along finite faults embedded in a three-dimensional full space. The fault dynamics is governed by a constitutive law with a slip-rate, state, and temperature dependence [@barbot19a; @barbot22; @barbot23]. The method is based on the quasi-dynamic approximation whereby the effect of seismic waves is approximated by radiation damping. The stress interactions are computed analytically in the Fourier domain [@barbot21] and converted with the `FFTW3` fast Fourier transform [@frigo+johnson05]. The calculations for a two-dimensional domain are parallelized with OpenMP. The spectrum of fault slip, including creep, slow-slip events, slow and fast earthquakes (\autoref{fig:01}), is afforded by adaptive time steps with the Runge-Kutta method [@press+96]. The simulations using finite faults are parallelized with `MPI` [@gabriel+04]. The stress kernels allow the mechanical interactions of an arbitrary number of parallel faults, allowing structurally complex settings with a network of faults and multiple step-overs.

![Example simulation of seismic cycles on two parallel faults. A) Model setup with the distribution of frictional and physical properties leading to unstable slip in a 5 km-wide asperity (red) surrounded by a velocity-strengthening region (blue). The thin surroundings of the fault surface (yellow) is subject to a kinematic boundary condition to enforce a long-term slip-rate of about 30 mm/yr, equivalent to 1 nm/s. The two faults are separated by 15 km. Each fault is sampled with 512x512 rectangle patches of 25 m. B) Sequences of fast ruptures followed by afterslip and slow-slip events late in the inter-seismic period corresponding to about 120,000 quasi-static time steps. The slices correspond to horizontal and vertical cross-sections through each fault. The dashed lines indicate the boundaries of the velocity-weakening region. C) Time series of peak velocity in the unstable asperities of faults 1 and 2. Velocities above 1 m/s are firmly in the seismic regime. Slow-slip events are more pronounced on fault 1. The simulation corresponds to the input file `3d/examples/tutorials/run2f.sh`.\label{fig:01}](figure-01.pdf)

# Statement of need

`Motorcycle` is a series of Fortran90 standalone numerical modeling tools for fault dynamics. The numerical simulations are optimized for performance and stability, based on automatic time-stepping and meshing. The input file allows complex rheological or structural settings and the automatic exploration of the parameter space. The simulation output is provided in ASCII tables and netcdf files [@brown+93; @rew+davis90] for automatic visualization with typical geophysical software such as the Generic Mapping Tools [@wessel+19].

`Motorcycle` is designed for scientists conducting research in fault dynamics. Applications include the nucleation of frictional instabilities (e.g., slow-slip events), the propagation of earthquake ruptures (e.g., crack-like versus pulse-like), and the mechanical coupling of multiple faults. Successful simulation benchmarks based on comparison with other software can be found in @jiang+22. Applications of the method include the simulation of synchronized earthquakes on distant faults [@barbot21], of complex slow-slip events generating tremors [@nie+barbot21], and of mainshock/aftershock sequences [@nie+barbot22].

# Acknowledgements

This study is supported in part by the National Science Foundation under award number EAR-1848192. We thank the editor Jed Brown for technical recommendations and the reviewers Prithvi Thakur and Matthew Herman for constructive comments that helped improve the software.

# References
