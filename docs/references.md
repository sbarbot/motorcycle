(sec:references)=

# References

Barbot S., "Modulation of fault strength during the seismic cycle by grain-size evolution around contact junctions", Tectonophysics, <https://dx.doi.org/j.tecto.2019.05.004>, 2019.

Barbot S., "A spectral boundary‐integral method for quasi‐dynamic ruptures of multiple parallel faults". Bull. Seism. Soc. Am., <https://dx.doi.org/10.1785/0120210004>, 2021.

# Benchmarks

Jiang J., Erickson B.A., Lambert V.R., Ampuero J.P., Ando R., Barbot S.D., Cattania C., Zilio L.D., Duan B., Dunham E.M. and Gabriel A.A., "Community‐driven code comparisons for three‐dimensional dynamic modeling of sequences of earthquakes and aseismic slip". J. Geophys. Res., <https://dx.doi.org/10.1029/2021JB023519>, 2022.

# Applications

Gauriau J., S. Barbot, and JF. Dolan, "Islands of chaos in a sea of periodic earthquakes". Earth and Planet. Sci. Lett., <https://dx.doi.org/10.1016/j.epsl.2023.118274>, 2023.

Nie, S. and Barbot, S., "Rupture styles linked to recurrence patterns in seismic cycles with a compliant fault zone". Earth Plan. Sci. Lett., <https://doi.org/10.1016/j.epsl.2022.117593>, 2022.

Nie, S. and Barbot, S., "Seismogenic and tremorgenic slow slip near the stability transition of frictional sliding". Earth Plan. Sci. Lett., <https://doi.org/10.1016/j.epsl.2021.117037>, 2021.

