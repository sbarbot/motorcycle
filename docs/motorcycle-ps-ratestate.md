---
Date: July 18, 2022
Title: motorcycle-ps-ratestate
---

## Motorcycle-ps-ratestate

`motorcycle-ps-ratestate` computes the evolution of slip on multiple rate- and state-dependent friction faults in condition of in-plane strain using the spectral boundary integral method with the radiation damping approximation.

# SYNOPSIS

motorcycle-ps-ratestate \[-h\] \[\--dry-run\] \[\--help\] \[\--export-netcdf\] \[\--epsilon\] \[\--friction-law ( 1 \| 2 \| 3)\] \[\--maximum-step\] \[\--maximum-iterations\] \[\--version\]

# OPTIONS

**-h** :   print a short message and abort calculation

**\--dry-run** :   write lightweight information files and abort calculation

**\--help** :   print a short message and abort calculation

**\--export-netcdf** :   export time series of instantaneous velocity in a GMT compatible
    netcdf file

**\--export-netcdf-rate \[20\]** :   set the rate of output in netcdf file

**\--export-netcdf-step \[1\]** :   set the spatial subsampling rate in netcdf file

**\--export-state** :   save the state vector to resume the simulation at the last
    computational time step

**\--export-stress** :   export the time series of shear stress in a GMT compatible netcdf
    file

**\--epsilon \[1e-6\]** :   set the relative accuracy of the 4/5th order Runge-Kutta integration
    method

**\--friction-law ( 1 \| 2 \| 3 )** :   select the type of friction law \[default: 1\] <br>
1: multiplicative form of rate-state friction (Barbot, 2019) <br>
2: additive form of rate-state friction (Ruina, 1983) <br>
3: arcsinh form of rate-state friction (Rice & Ben-zion, 1996)

**\--import-state dir** :   load the state vector from directory "dir" to resume the simulation at the last computational time step

**\--maximum-step \[Inf\]** :   set the maximum time step

**\--maximum-iterations \[1000000\]** :   set the maximum number of iterations

**\--source-export-rate \[50\]** :   set the maximum number of iterations

**\--verbose \[2\]** :   set the level of information in standard output

# ENVIRONMENT

The code is parallized with OpenMP. Calling the programs with

    OMP_NUM_THREADS=4 motorcycle-ps-ratestate

controls the number of threads. Otherwise, the maximum number of threads available is used.

# INPUT PARAMETERS

The lines starting with the "#" symbol are commented.

**output directory (wdir)** :   All output files are written to the specified directory, including observation patches, observation volumes, observation points and netcdf files.

**Lame parameter (lambda) and rigidity (mu)** :   The uniform Lame parameter (lambda) and rigidity (mu) in the full space. For the Earth, typical values are lambda=mu=30 GPa. All physical quantities are assumed in SI units (meter, Pascal, second).

**time interval** :   Refers to the duration of the calculation in SI units (s), for example 3.15e7 for one year.

**number of faults** :   The number of parallel faults involved. The center of the first fault is at position x2=x3=0.

**grid dimension (N2)** :   The dimension of rectangular faults, where N1 is in the strike direction and N2 is in the dip direction. N1 must be a factor of two to facilitate the in-place real-to-complex Fourier transform. The number of fault patches is N1\*N2 times the number of faults.

**sampling (dx2)** :   The sampling size along the dip direction.

**distance from fault 1** :   The program expects physical properties for each patch of each fault. When multiple parallel faults are present, all but the first fault requires the distance from the first fault. This parameter shall not be provided when only one fault is present.

**patch properties** :   The program expects physical properties for each patch of each fault. The properties are

    # n tau0 mu0 sig a b L Vo G/(2Vs) Vl Dirichlet

where tau0 is the initial stress, mu0 and sig are the static coefficient of friction and the effective normal stress, a and b are the dynamic friction coefficient of rate-and-state friction, L is the characteristic weakening distance, Vo is the reference velocity, G/(2Vs) is the radiation damping coefficient, Vl is the loading rate, for example 1e-9 m/s, and Dirichlet is a boolean ("T" or "F") to impose the velocity as a boundary condition. When tau0<0, the initial stress is set to the value that makes the fault slip at the velocity Vl. Prefix subsequent faults by their position x3 relative to fault 1. To avoid large input files, the code allows to reuse the physical properties of previous patches. Normal input for physical properties consists of a line number followed by the expected patch properties. To reuse the properties of the previous patch, use minus the line number. This strategy reduces input files by a factor of 10 to 20 in common cases.

**number of observation patches** :   The number of patch element that will be monitored during the calculation. For these patches, the time series of dynamic variables and their time derivatives will be exported in wdir/patch-01-00128.dat, where 01 and 00128 will be substituted with the fault index, and the patch index i2, respectively. These time series will include slip components, traction components, state variables, the log10 of the instantaneous velocity. The following columns of the file will contain the time derivatives of these variables. If the number is positive, this must be followed by

    # n fault i2 rate

where fault is the fault index and i2 is the patch index, and rate is the sampling rate. A sampling rate of 1 exports all time steps.

**number of observation profiles** :   The number of observation profiles where log10(velocity) is exported every "rate" steps. The profiles must be described with

    # n fault index direction rate

where n is a running index starting at 1, fault is the fault index, index is the column and row index, depending on direction, direction is 1 for horizontal and 2 for vertical, and rate is the export sampling rate.

# CALLING SEQUENCE

The main program is run with shared parallelism (OpenMP).

    OMP_NUM_THREADS=4 motorcycle-ps-ratestate wdir/in.param

# PHYSICAL UNITS

All physical quantities are assumed to be in SI units (meter, Pascal, second). A good practice is to use MPa instead of Pa for the effective normal stress and initial shear stress.

# AUTHOR

Sylvain Barbot (sbarbot@usc.edu), 2020

       ,
    .-/c-.,::
    (_)''==(_)

# COPYRIGHT

MOTORCYCLE is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

MOTORCYCLE is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with MOTORCYCLE. If not, see http://www.gnu.org/licenses/.
