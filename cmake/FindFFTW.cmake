# - Find FFTW
# Find the native FFTW includes and library
#
#  FFTW_INCLUDES        - where to find fftw3.h
#  FFTW_LIBRARIES       - List of libraries when using FFTW.
#  FFTW_OMP_LIBRARIES   - List of libraries when using FFTW.
#  FFTW_FOUND           - True if FFTW found.

IF (FFTW_INCLUDES)
  # Already in cache, be silent
  SET(FFTW_FIND_QUIETLY TRUE)
ENDIF()

FIND_PATH(FFTW_INCLUDES fftw3.h)

FIND_LIBRARY(FFTW_LIBRARIES NAMES fftw3)
set(FFTW_REQUIRED FFTW_LIBRARIES)
IF (USE_OPENMP)
  FIND_LIBRARY(FFTW_OMP_LIBRARIES NAMES fftw3_omp)
  list(PREPEND FFTW_REQUIRED FFTW_OMP_LIBRARIES)
ENDIF()

MESSAGE("-- FFTW library: " ${FFTW_LIBRARIES} " " ${FFTW_OMP_LIBRARIES})

# handle the QUIETLY and REQUIRED arguments and set FFTW_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(FFTW DEFAULT_MSG ${FFTW_REQUIRED} FFTW_INCLUDES)

MARK_AS_ADVANCED(FFTW_LIBRARIES FFTW_OMP_LIBRARIES FFTW_INCLUDES)
