LIB=$(patsubst %,$(DST)/%, \
    getopt_m.o exportnetcdf.o fft1d.o fft2d.o rk.o )

$(shell mkdir -p $(DST))

$(DST)/%.o: $(SRC)/%.f90
	$(COMPILE.f) $^ -o $(DST)/$*.o -J $(DST)

lib: $(LIB)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:

