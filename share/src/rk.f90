!-----------------------------------------------------------------------
! Copyright 2017-2023 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#define STDERR 0

MODULE rk

  IMPLICIT NONE

  PUBLIC
  REAL(8), ALLOCATABLE :: buffer(:,:)

  ! numerical accuracy
  REAL(8) :: epsilon = 1.d-6

  ! maximum time step
  REAL*8 :: maximumTimeStep = 1.7976931348623158d308

CONTAINS

  !------------------------------------------------------------------------------
  ! subroutine rungeKutta
  ! performs adaptive time step based on the 2/3rd- or 4/5th-order accurate time
  ! steps. See Numerical Recipes 2nd. edition, p. 712 for details.
  !------------------------------------------------------------------------------
  SUBROUTINE rungeKutta(n,t,y,dydt,yscal,yCandidate,yerr,ytmp,dtTry,dtDid,dtNext,odefun,method)

    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: dtTry,yscal(n)
    REAL*8, INTENT(INOUT) :: ytmp(n)
    REAL*8, INTENT(INOUT) :: t
    REAL*8, INTENT(INOUT) :: dydt(n),y(n)
    REAL*8, INTENT(INOUT) :: yCandidate(n),yerr(n)
    REAL*8, INTENT(INOUT) :: dtDid,dtNext
    EXTERNAL              :: odefun
    EXTERNAL              :: method

    REAL*8, PARAMETER    :: SAFETY=0.7d0,PGROW=-0.2d0,PSHRNK=-0.15d0,ERRCON=1.89d-4
    REAL*8               :: dt,errmax,tnew

    dt=dtTry

    DO WHILE (.TRUE.)
      
       CALL method(n,t,y,dydt,dt,yCandidate,yerr,ytmp,odefun)
       errmax=MAXVAL(ABS(yerr(1:n)/yscal(1:n)))

       errmax = errmax/epsilon
       IF (errmax .GT. 1.d0) THEN
          dt=MAX(dt*SAFETY*(errmax**PSHRNK),0.5d0*dt)
          tnew=t+dt
          IF (tnew .EQ. t) THEN
             WRITE(STDERR,'("# stepsize underflow in rungeKutta")')
             STOP 1
          END IF
       ELSE
          IF (errmax .GT. ERRCON) THEN
             dtNext=SAFETY*dt*(errmax**PGROW)
          ELSE
             dtNext=1.d1*dt
          END IF
          IF (dtNext .GT. maximumTimeStep) THEN
             dtNext=maximumTimeStep
          END IF

          dtDid=dt
          t=t+dt
          y=yCandidate
          EXIT 
       ENDIF
    END DO

  END SUBROUTINE rungeKutta

  !------------------------------------------------------------------------------
  !> timeStepTableau
  !
  ! Runge–Kutta methods are methods for the numerical solution of the ordinary differential equation
  !
  !    dy/dt = f(t,y) .
  !
  ! Explicit Runge–Kutta methods take the form
  !
  !    y_{n+1} = y_n + dt \sum_{i=1}^{s} b_i k_i
  ! 
  ! with
  !
  !        k_i = f(t_n + c_i h, y_n + dt \sum _{j=1}^{i-1} a_{ij} k_{j}) .
  !
  ! Different Runge-Kutta methods can be defined by its Butcher tableau, which puts the coefficients 
  ! of the method in a table as follows
  !
  !        c_1  |  a_{11} a_{12} ... a_{1s}
  !        c_2  |  a_{21} a_{22} ... a_{2s}
  !         .   |    .      .    ...   .
  !        c_s  |  a_{s1} a_{s2} ... a_{ss}
  !        -----+--------------------------
  !             |   b_1    b_2   ...  b_s
  !             |   d_1    d_2   ...  d_s
  !
  ! For adaptive and implicit methods, the Butcher tableau is extended to give values of d_i, 
  ! and the estimated error is then
  !
  !    e_{n+1} = dt \sum _{i=1}^{s} (b_i-d_i) k_i .
  !
  ! Algorithm:
  !
  !    k(1)=f(t_n, y_n)
  !    for i=2,s
  !       tmp = sum_{j=1}^{i-1} a_{ij} k_j
  !       k(i) = f(t_n + c_i dt, y_n + dt * tmp)
  !    end for
  !    y_{n+1} = y_n + dt sum_{i=1}^s b_i k_i
  !    e_{n+1} = dt sum_{i=1}^s (b_i-b_i*) k_i
  !
  ! Butcher Tableau:
  !
  !    ode23:
  !
  !    0.50d0  |       0.50d0
  !    0.75d0  |       0.75d0
  !    1.00d0  |      2d0/9d0  1d0/3d0  4d0/9d0              
  !    --------+----------------------------------------
  !            |      2d0/9d0  1d0/3d0  4d0/9d0
  !            |      7d0/24d0 1d0/4d0  1d0/3d0  0.125d0
  !
  !    ode45:
  ! 
  !    0.2d0   |            0.2d0
  !    0.3d0   |            3.0d0    9.d0/40.d0
  !    0.6d0   |            0.3d0        -0.9d0             1.2d0
  !    1.0d0   |     -11.d0/54.d0         2.5d0      -70.d0/27.d0        35.d0/27.d0
  !    0.875d0 | 1631.d0/55296.d0 175.d0/512.d0   575.d0/13824.d0 44275.d0/110592.d0    253.d0/4096.d0
  !    --------+-----------------------------------------------------------------------------------------------------
  !            |     37.d0/378.d0          0.d0     250.d0/621.d0      125.d0/594.d0              0.d0 512.d0/1771.d0
  !            | 2825.d0/27648.d0          0.d0 18575.d0/48384.d0  13525.d0/55296.d0   277.d0/14336.d0         0.25d0
  !
  !------------------------------------------------------------------------------
  SUBROUTINE timeStepTableau(n,t,yin,dydt,dt,yout,yerr,ytmp,m,tableau,odefun)
    IMPLICIT NONE
    INTEGER, INTENT(IN)   :: n,m
    REAL*8, INTENT(IN)    :: t,dt
    REAL*8, DIMENSION(n), INTENT(INOUT) :: yin,dydt,yout,yerr,ytmp
    REAL*8, DIMENSION(m,m), INTENT(IN) :: tableau
    EXTERNAL :: odefun

    INTEGER :: i,j

    REAL*8, DIMENSION(m-1) :: b
    REAL*8, DIMENSION(m-1) :: d
    REAL*8, DIMENSION(m-2) :: c
    REAL*8, DIMENSION(m-2,m-2) :: a

    d = tableau(m  ,2:m)
    c = tableau(1:m-2,1)
    a = tableau(1:m-2,2:m-1)
    b = tableau(m-1,2:m)

    ! intermediate solutions
    DO i=1,m-2
       ytmp=dydt*a(i,1)
       IF (1 .LT. i) THEN
          DO j=2,i
             IF (0d0.NE.a(i,j)) ytmp=ytmp+a(i,j)*buffer(:,j-1)
          END DO
       END IF
       CALL odefun(n,t+c(i)*dt,yin+dt*ytmp,buffer(:,i))
    END DO

    ! output
    yout=yin+dt*b(1)*dydt
    DO i=2,m-1
       yout=yout+dt*b(i)*buffer(:,i-1)
    END DO

    ! error estimate
    yerr=dt*(b(1)-d(1))*dydt
    DO i=2,m-1
       yerr=yerr+dt*(b(i)-d(i))*buffer(:,i-1)
    END DO

  END SUBROUTINE timeStepTableau

  !------------------------------------------------------------------------------
  !> subroutine timeStep23
  !  conducts 2/3rd-accurate forward time step based on forward model odefun.
  !------------------------------------------------------------------------------
  SUBROUTINE timeStep23(n,t,yin,dydt,dt,yout,yerr,ytmp,odefun)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: t,dt
    REAL*8, INTENT(INOUT) :: yin(n),dydt(n), &
                             yout(n),yerr(n),ytmp(n)
    EXTERNAL :: odefun

    INTEGER, PARAMETER :: m = 5

    REAL*8, DIMENSION(m,m), PARAMETER :: tableau = &
            TRANSPOSE(RESHAPE((/ 0.50d0,  0.50d0,   0.d0,   0.d0,   0.d0, &
                                 0.75d0,  0.75d0,   0.d0,   0.d0,   0.d0, &
                                 1.00d0, 2d0/9d0,1d0/3d0,4d0/9d0,   0.d0, &
                                 0.00d0, 2d0/9d0,1d0/3d0,4d0/9d0,   0.d0, &
                                 0.00d0,7d0/24d0,1d0/4d0,1d0/3d0,0.125d0 /), (/ m,m /)))

    CALL timeStepTableau(n,t,yin,dydt,dt,yout,yerr,ytmp,m,tableau,odefun)

  END SUBROUTINE timeStep23

  !------------------------------------------------------------------------------
  !> subroutine timeStep45
  !  conducts 4/5th-order accurate forward time step based on forward model odefun
  !------------------------------------------------------------------------------
  SUBROUTINE timeStep45(n,t,yin,dydt,dt,yout,yerr,ytmp,odefun)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: t,dt
    REAL*8, INTENT(INOUT) :: yin(n),dydt(n), &
                             yout(n),yerr(n),ytmp(n)
    EXTERNAL :: odefun

    INTEGER, PARAMETER :: m = 7

    REAL*8, DIMENSION(m,m), PARAMETER :: tableau = TRANSPOSE(RESHAPE((/ &
    0.2d0,           0.2d0,         0.d0,             0.d0,              0.d0,           0.d0,          0.d0, &
    0.3d0,      3.d0/40.d0,   9.d0/40.d0,             0.d0,              0.d0,           0.d0,          0.d0, &
    0.6d0,           0.3d0,       -0.9d0,            1.2d0,              0.d0,           0.d0,          0.d0, &
    1.0d0,    -11.d0/54.d0,        2.5d0,     -70.d0/27.d0,       35.d0/27.d0,           0.d0,          0.d0, &
  0.875d0,1631.d0/55296.d0,175.d0/512.d0,  575.d0/13824.d0,44275.d0/110592.d0, 253.d0/4096.d0,          0.d0, &
    0.0d0,    37.d0/378.d0,         0.d0,    250.d0/621.d0,     125.d0/594.d0,           0.d0,512.d0/1771.d0, &
     0.d0,2825.d0/27648.d0,         0.d0,18575.d0/48384.d0, 13525.d0/55296.d0,277.d0/14336.d0,        0.25d0 /), (/ m,m /)))

    CALL timeStepTableau(n,t,yin,dydt,dt,yout,yerr,ytmp,m,tableau,odefun)

  END SUBROUTINE timeStep45

END MODULE rk

