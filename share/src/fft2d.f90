!-----------------------------------------------------------------------
! Copyright 2007-2023 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

MODULE fft2d

  IMPLICIT NONE

  PUBLIC

  INCLUDE 'fftw3.f'

  INTEGER, PARAMETER :: FFT_FORWARD=-1,FFT_INVERSE=1

CONTAINS

  !----------------------------------------------------------------------
  !> subroutine FFTINIT builds wisdom for future DFT execution.
  !----------------------------------------------------------------------
  SUBROUTINE fftInit(sx1,sx2,data)
    INTEGER, INTENT(IN) :: sx1,sx2
    REAL*8, DIMENSION(sx1+2,sx2), INTENT(INOUT) :: data

    INTEGER*8 :: plan

    CALL dfftw_plan_dft_r2c_2d(plan,sx1,sx2, &
                               data(1,1),data(1,1),FFTW_MEASURE)

    CALL dfftw_plan_dft_c2r_2d(plan,sx1,sx2, &
                               data(1,1),data(1,1),FFTW_MEASURE)

  END SUBROUTINE fftInit

  !----------------------------------------------------------------------
  !> subroutine FFT2 performs normalized forward and
  !! inverse fourier transforms of real 2d data
  !!
  !! for real array the fourier transform returns a sx1/2+1 complex array
  !! and enough space must be reserved.
  !----------------------------------------------------------------------
  SUBROUTINE fft2(data,sx1,sx2,direction)
    INTEGER, INTENT(IN) :: sx1,sx2,direction
    REAL*8, DIMENSION(sx1+2,sx2), INTENT(INOUT) :: data

    INTEGER*8 :: plan

    IF (FFT_FORWARD == direction) THEN
      CALL dfftw_plan_dft_r2c_2d(plan,sx1,sx2, &
           data(1,1),data(1,1),FFTW_MEASURE)
    ELSE
      CALL dfftw_plan_dft_c2r_2d(plan,sx1,sx2, &
           data(1,1),data(1,1),FFTW_MEASURE)
    END IF

    CALL dfftw_execute_dft_r2c(plan,data(1,1),data(1,1))
    CALL dfftw_destroy_plan(plan)

    IF (FFT_INVERSE == direction) THEN
       data=data/(sx1*sx2)
    END IF

  END SUBROUTINE fft2

END MODULE fft2d
