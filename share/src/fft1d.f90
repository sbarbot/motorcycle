!-----------------------------------------------------------------------
! Copyright 2020 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

MODULE fft1d

  IMPLICIT NONE

  PUBLIC

  INCLUDE 'fftw3.f'

  INTEGER, PARAMETER :: FFT_FORWARD=-1,FFT_INVERSE=1

CONTAINS

  !----------------------------------------------------------------------
  !> subroutine FFT_INIT builds wisdom
  !----------------------------------------------------------------------
  SUBROUTINE fft_init(data,sx)
    INTEGER, INTENT(IN) :: sx
    REAL*8, DIMENSION(sx+2), INTENT(INOUT) :: data

    INTEGER*8 :: plan

    CALL dfftw_plan_dft_r2c_1d(plan,sx,data(1),data(1),FFTW_MEASURE)
    CALL dfftw_destroy_plan(plan)
    CALL dfftw_plan_dft_c2r_1d(plan,sx,data(1),data(1),FFTW_MEASURE)
    CALL dfftw_destroy_plan(plan)

  END SUBROUTINE fft_init

  !----------------------------------------------------------------------
  !> subroutine FFT1 performs normalized forward and
  !! inverse fourier transforms of real 1d data
  !!
  !! for real array the fourier transform returns a sx/2+1 complex array
  !! and enough space must be reserved.
  !----------------------------------------------------------------------
  SUBROUTINE fft1(data,sx,direction)
    INTEGER, INTENT(IN) :: sx,direction
    REAL*8, DIMENSION(sx+2), INTENT(INOUT) :: data

    INTEGER*8 :: plan

    IF (FFT_FORWARD == direction) THEN
      CALL dfftw_plan_dft_r2c_1d(plan,sx,data(1),data(1),FFTW_MEASURE)
    ELSE
      CALL dfftw_plan_dft_c2r_1d(plan,sx,data(1),data(1),FFTW_MEASURE)
    END IF

    CALL dfftw_execute_dft_r2c(plan,data,data)
    CALL dfftw_destroy_plan(plan)

    IF (FFT_INVERSE == direction) THEN
      data=data/sx
    END IF

  END SUBROUTINE fft1

END MODULE fft1d

