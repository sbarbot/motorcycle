!-----------------------------------------------------------------------
! Copyright 2017 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.h90"

MODULE types_3d_serial

  IMPLICIT NONE

  TYPE PATCH_ELEMENT_STRUCT
     SEQUENCE
     REAL*8 :: Vl,rake
     REAL*8 :: tau0,mu0,sig,a,b,L,Vo,damping
     LOGICAL :: dirichlet
  END TYPE PATCH_ELEMENT_STRUCT

  TYPE EVENT_STRUCT
     REAL*8 :: time
     INTEGER*4 :: i
  END TYPE EVENT_STRUCT
  
  TYPE FAULT_STRUCT
     ! offset relative to reference fault
     REAL*8 :: x1,x2,x3
     ! slip components
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: s1,s2
     ! stress components
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: s13,s23,s33
     ! padded slip components
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: ps1,ps2
  END TYPE FAULT_STRUCT

  TYPE OBSERVATION_STATE_STRUCT
     ! fault index
     INTEGER :: fault
     ! coordinates
     INTEGER :: i1,i2
     ! sampling rate
     INTEGER :: rate
     ! file number
     INTEGER :: id
  END TYPE OBSERVATION_STATE_STRUCT
  
#ifdef NETCDF
  TYPE PROFILE_STRUCT
     SEQUENCE
     INTEGER :: fault,index,direction,rate
     ! netcdf file
     INTEGER :: ncid,y_varid,z_varid,ncCount
  END TYPE PROFILE_STRUCT

  TYPE OBSERVATION_PLANE_STRUCT
     SEQUENCE
     ! fault-perpendicular distance
     REAL*8 :: x3
     ! export rate
     INTEGER :: rate
     ! netcdf file
     INTEGER :: ncid,y_varid,z_varid,ncCount
     ! displacement components
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: u1,u2,u3
  END TYPE OBSERVATION_PLANE_STRUCT
#endif

  TYPE :: SIMULATION_STRUCT

     ! elastic moduli
     REAL*8 :: lambda,mu,nu,alpha

     ! simulation time
     REAL*8 :: interval

     ! fault dimension
     INTEGER :: N1,N2

     ! sampling size
     REAL*8 :: dx1,dx2

     ! number of fault patches
     INTEGER :: nPatch

     ! patches
     TYPE(PATCH_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: patch

     ! number of parallel faults
     INTEGER :: nFault

     ! fault structures
     TYPE(FAULT_STRUCT), DIMENSION(:), ALLOCATABLE :: fault

     ! output directory
     CHARACTER(256) :: wdir

     ! filenames
     CHARACTER(256) :: timeFilename

     ! number of observation states
     INTEGER :: nObservationState

     ! observation state (patches and volumes)
     TYPE(OBSERVATION_STATE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationState

#ifdef NETCDF
     ! observation profiles
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationProfile

     ! observation planes
     TYPE(OBSERVATION_PLANE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationPlane
#endif

     ! number of observation profiles
     INTEGER :: nObservationProfiles

     ! number of observation profiles
     INTEGER :: nObservationPlanes

     ! number of observation points
     INTEGER :: nObservationPoint

     ! number of perturbation events
     INTEGER :: ne

     ! perturbation events
     TYPE(EVENT_STRUCT), DIMENSION(:), ALLOCATABLE :: event

     ! other options
     LOGICAL :: isdryrun=.FALSE.
     LOGICAL :: isExportNetcdf=.FALSE.
     LOGICAL :: isExportStress=.FALSE.
     LOGICAL :: isImportState=.FALSE.
     LOGICAL :: isExportState=.FALSE.
     LOGICAL :: ishelp=.FALSE.
     LOGICAL :: isversion=.FALSE.
     INTEGER :: sourceExportRate=100

  END TYPE SIMULATION_STRUCT

END MODULE types_3d_serial

