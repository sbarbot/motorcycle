!-----------------------------------------------------------------------
! Copyright 2020 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

MODULE greens_3d_serial

  USE types_3d_serial

  IMPLICIT NONE

  PUBLIC

CONTAINS

  !----------------------------------------------------------------------
  !> subroutine computeTraction
  !! computes the traction on a receiver fault due to slip on a source
  !! fault.
  !!
  !! INPUT:
  !! rcv         - receiver fault
  !! src         - source fault
  !! mu          - rigidity
  !! alpha       - elastic parameter alpha=1/2/(1-nu)=(l+mu)/(l+2*mu)
  !! isInit      - initialize rcv traction?
  !----------------------------------------------------------------------
  SUBROUTINE computeTraction(rcv,src,N1,N2,dx1,dx2,mu,alpha,isInit)
    TYPE(FAULT_STRUCT), INTENT(IN) :: src
    TYPE(FAULT_STRUCT), INTENT(INOUT) :: rcv
    INTEGER, INTENT(IN) :: N1,N2
    REAL*8, INTENT(IN) :: dx1,dx2
    REAL*8, INTENT(IN) :: mu,alpha
    LOGICAL, INTENT(IN) :: isInit

    ! pi
    REAL*8, PARAMETER :: PI = 3.1415926535897931159979634685441851615905_8

    ! I=SQRT(-1)
    COMPLEX*8, PARAMETER :: ci = CMPLX(0._8,1._8,8)

    ! counters
    INTEGER :: i1,i2

    ! radial wavenumber
    REAL*8 :: w

    ! wavenumbers
    REAL*8 :: w1,w2

    ! slip components
    COMPLEX(KIND=8) :: s1,s2

    ! traction components
    COMPLEX(KIND=8) :: s13,s23,s33

    ! fault-perpendicular distance
    REAL*8 :: x3

    x3=rcv%x3-src%x3

!$omp do private(i1,w1,w2,w,s1,s2,s13,s23,s33)
    DO i2=1,N2
       DO i1=1,N1/2+1
          IF (i1 < N1/2+1) THEN
             w1= 2*PI*(DBLE(i1-1))/(N1*dx1)
          ELSE
             w1=-2*PI*(DBLE(N1-i1+1))/(N1*dx1)
          END IF

          IF (i2 < N2/2+1) THEN
             w2= 2*PI*(DBLE(i2-1))/(N2*dx2)
          ELSE
             w2=-2*PI*(DBLE(N2-i2+1))/(N2*dx2)
          END IF

          ! radial wavenumber
          w=SQRT(w1**2+w2**2)

          ! slip components
          s1=CMPLX(src%s1(2*i1-1,i2),src%s1(2*i1,i2),8)
          s2=CMPLX(src%s2(2*i1-1,i2),src%s2(2*i1,i2),8)

          s13=-mu/2/w*( &
                  (w2**2+2*alpha*w1**2-2*alpha*w1**2*w*ABS(x3))*s1 &
                 +((2*alpha-1._8)-2*alpha*w*ABS(x3))*w1*w2*s2 &
               )*EXP(-w*ABS(x3))

          s23=-mu/2/w*( &
                  ((2*alpha-1._8)-2*alpha*w*ABS(x3))*w1*w2*s1 &
                 +(w1**2+2*alpha*w2**2-2*alpha*w2**2*w*ABS(x3))*s2 &
               )*EXP(-w*ABS(x3))

          s33= mu*alpha*w*x3*(ci*w1*s1+ci*w2*s2)*EXP(-w*ABS(x3))

          IF (isInit) THEN
             ! initialize
             rcv%s13(2*i1-1:2*i1,i2)=(/ REAL(s13),AIMAG(s13) /)
             rcv%s23(2*i1-1:2*i1,i2)=(/ REAL(s23),AIMAG(s23) /)
             rcv%s33(2*i1-1:2*i1,i2)=(/ REAL(s33),AIMAG(s33) /)
          ELSE
             ! update
             rcv%s13(2*i1-1:2*i1,i2)=rcv%s13(2*i1-1:2*i1,i2)+(/ REAL(s13),AIMAG(s13) /)
             rcv%s23(2*i1-1:2*i1,i2)=rcv%s23(2*i1-1:2*i1,i2)+(/ REAL(s23),AIMAG(s23) /)
             rcv%s33(2*i1-1:2*i1,i2)=rcv%s33(2*i1-1:2*i1,i2)+(/ REAL(s33),AIMAG(s33) /)
          END IF
       END DO
    END DO
!$omp end do

    ! zero mean stress
    rcv%s13(1:2,1)=0._8
    rcv%s23(1:2,1)=0._8
    rcv%s33(1:2,1)=0._8

  END SUBROUTINE computeTraction

  !----------------------------------------------------------------------
  !> subroutine computeDisplacement
  !! computes the displacement on a plane due to slip on a source
  !! fault.
  !!
  !! INPUT:
  !! src         - source fault
  !! mu          - rigidity
  !! alpha       - elastic parameter alpha=1/2/(1-nu)=(l+mu)/(l+2*mu)
  !! isInit      - initialize rcv traction?
  !----------------------------------------------------------------------
  SUBROUTINE computeDisplacement(src,plane,N1,N2,dx1,dx2,alpha,isInit)
    TYPE(FAULT_STRUCT), INTENT(IN) :: src
    TYPE(OBSERVATION_PLANE_STRUCT), INTENT(INOUT) :: plane
    INTEGER, INTENT(IN) :: N1,N2
    REAL*8, INTENT(IN) :: dx1,dx2
    REAL*8, INTENT(IN) :: alpha
    LOGICAL, INTENT(IN) :: isInit

    ! pi
    REAL*8, PARAMETER :: PI = 3.1415926535897931159979634685441851615905_8

    ! I=SQRT(-1)
    COMPLEX*8, PARAMETER :: ci = CMPLX(0._8,1._8,8)

    ! counters
    INTEGER :: i1,i2

    ! radial wavenumber
    REAL*8 :: w

    ! wavenumbers
    REAL*8 :: w1,w2

    ! slip components
    COMPLEX(KIND=8) :: s1,s2

    ! displacement components
    COMPLEX(KIND=8) :: u1,u2,u3

    ! fault-perpendicular distance
    REAL*8 :: x3

    x3=plane%x3-src%x3

!$omp do private(i1,w1,w2,w,s1,s2,u1,u2,u3)
    DO i2=1,N2
       DO i1=1,N1/2+1
          IF (i1 < N1/2+1) THEN
             w1= 2*PI*(DBLE(i1-1))/(N1*dx1)
          ELSE
             w1=-2*PI*(DBLE(N1-i1+1))/(N1*dx1)
          END IF

          IF (i2 < N2/2+1) THEN
             w2= 2*PI*(DBLE(i2-1))/(N2*dx2)
          ELSE
             w2=-2*PI*(DBLE(N2-i2+1))/(N2*dx2)
          END IF

          ! radial wavenumber
          w=SQRT(w1**2+w2**2)

          ! slip components
          s1=CMPLX(src%ps1(2*i1-1,i2),src%ps1(2*i1,i2),8)
          s2=CMPLX(src%ps2(2*i1-1,i2),src%ps2(2*i1,i2),8)

          ! displacement components
          u1=-0.5d0/w*((alpha*w1**2*x3-w)*s1+alpha*w1*w2*x3*s2)*EXP(-w*ABS(x3))
          u2=-0.5d0/w*(alpha*w1*w2*x3*s1+(alpha*w2**2*x3-w)*s2)*EXP(-w*ABS(x3))
          u3=-ci/2/w*((1d0-alpha)+alpha*w*x3)*(w1*s1+w2*s2)*EXP(-w*ABS(x3))

          IF (isInit) THEN
             ! initialize
             plane%u1(2*i1-1:2*i1,i2)=(/ REAL(u1),AIMAG(u1) /)
             plane%u2(2*i1-1:2*i1,i2)=(/ REAL(u2),AIMAG(u2) /)
             plane%u3(2*i1-1:2*i1,i2)=(/ REAL(u3),AIMAG(u3) /)
          ELSE
             ! update
             plane%u1(2*i1-1:2*i1,i2)=plane%u1(2*i1-1:2*i1,i2)+(/ REAL(u1),AIMAG(u1) /)
             plane%u2(2*i1-1:2*i1,i2)=plane%u2(2*i1-1:2*i1,i2)+(/ REAL(u2),AIMAG(u2) /)
             plane%u3(2*i1-1:2*i1,i2)=plane%u3(2*i1-1:2*i1,i2)+(/ REAL(u3),AIMAG(u3) /)
          END IF
       END DO
    END DO
!$omp end do

    ! zero mean stress
    plane%u1(1:2,1)=0._8
    plane%u2(1:2,1)=0._8
    plane%u3(1:2,1)=0._8

  END SUBROUTINE computeDisplacement

END MODULE greens_3d_serial

