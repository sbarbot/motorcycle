
OBJRS=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_3d_serial.o getdata.o \
      greens_3d_serial.o ratestate.o )

OBJTH=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_3d_serial.o getdata.o \
      greens_3d_serial.o ratestate_bath.o )

$(shell mkdir -p $(DST))
$(shell mkdir -p $(LIBDST))

$(DST)/%.o:$(SRC)/%.c
	$(COMPILE.c) $(CFLAGS) $^ -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f $(SRC)/macros.h90
	$(COMPILE.f) $(F77FLAGS) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f90 $(SRC)/macros.h90
	$(COMPILE.f) $(F90FLAGS) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o -J $(DST)

$(DST)/%.o: $(SRC)/%.cpp
	$(COMPILE.f) $(CXXFLAGS) $^ -o $(DST)/$*.o

$(LIBDST)/%.o: $(LIBSRC)/%.f90
	$(COMPILE.f) $^ -o $(LIBDST)/$*.o -J $(LIBDST)

LIB=$(patsubst %,$(LIBDST)/%, \
      getopt_m.o exportnetcdf.o fft2d.o rk.o )

all: lib motorcycle-3d-ratestate-serial

motorcycle-3d-ratestate-serial: $(filter-out $(SRC)/macros.h90,$(OBJRS)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

lib: $(LIB)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:

