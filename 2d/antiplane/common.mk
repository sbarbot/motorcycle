OBJRS=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_ap.o getdata.o greens_ap.o ratestate.o )

OBJCZ=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_ap.o getdata.o greens_ap.o ratestate_cz.o )

OBJTH=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_ap_bath.o getdata.o greens_ap_bath.o ratestate_bath.o )

OBJTB=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_ap_thermobaric.o getdata.o greens_ap_thermobaric.o rootbisection.o thermobaric.o )

LIB=$(patsubst %,$(LIBDST)/%, \
      getopt_m.o exportnetcdf.o fft1d.o rk.o )

$(shell mkdir -p $(DST))
$(shell mkdir -p $(LIBDST))

$(DST)/%.o:$(SRC)/%.c
	$(COMPILE.c) $^ -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f $(SRC)/macros.h90
	$(COMPILE.f) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f90
	$(COMPILE.f) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o -J $(DST)

$(LIBDST)/%.o: $(LIBSRC)/%.f90
	$(COMPILE.f) $^ -o $(LIBDST)/$*.o -J $(LIBDST)

all: lib motorcycle-ap-ratestate-serial motorcycle-ap-ratestate-cz-serial \
	motorcycle-ap-ratestate-bath-serial motorcycle-ap-thermobaric-serial

motorcycle-ap-ratestate-serial: $(filter-out $(SRC)/macros.h90,$(OBJRS)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

motorcycle-ap-ratestate-cz-serial: $(filter-out $(SRC)/macros.h90,$(OBJCZ)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

motorcycle-ap-ratestate-bath-serial: $(filter-out $(SRC)/macros.h90,$(OBJTH)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

motorcycle-ap-thermobaric-serial: $(filter-out $(SRC)/macros.h90,$(OBJTB)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

lib: $(LIB)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:

