!-----------------------------------------------------------------------
! Copyright 2020-2023 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

MODULE greens_ap

#include "macros.h90"

  USE types_ap

  IMPLICIT NONE

  PUBLIC

CONTAINS

  !----------------------------------------------------------------------
  !> subroutine initGreens
  !! computes the traction on a receiver fault due to slip on a source
  !! fault.
  !!
  !! INPUT:
  !! greens      - Greens functions (g11)
  !! rcv         - receiver fault
  !! src         - source fault
  !! N2          - array size COMPLEX(N2/2+1)
  !! dx2         - sampling size
  !! mu          - rigidity
  !----------------------------------------------------------------------
  SUBROUTINE initGreens(greens,rcv,src,N2,dx2,mu)
    TYPE(GREENS_TYPE), INTENT(INOUT) :: greens
    TYPE(FAULT_STRUCT), INTENT(INOUT) :: rcv
    TYPE(FAULT_STRUCT), INTENT(IN) :: src
    INTEGER, INTENT(IN) :: N2
    REAL*8, INTENT(IN) :: dx2
    REAL*8, INTENT(IN) :: mu

    ! pi
    REAL*8, PARAMETER :: PI = 3.1415926535897931159979634685441851615905_8

    ! I=SQRT(-1)
    COMPLEX(KIND=8), PARAMETER :: ci = CMPLX(0._8,1._8,8)

    ! counter
    INTEGER :: i2

    ! wavenumber
    REAL*8 :: w2

    ! traction components
    COMPLEX(KIND=8) :: s13

    ! fault-perpendicular distance
    REAL*8 :: x3

    x3=rcv%x3-src%x3

!$omp do private(w2,s13)
    DO i2=1,N2/2+1

       w2=2*PI*(DBLE(i2-1))/(N2*dx2)

       ! transfer function
       s13=-mu/2*w2*EXP(-w2*ABS(x3))

       ! initialize Greens function
       greens%g11(i2)=s13
    END DO
!$omp end do

  END SUBROUTINE initGreens

  !----------------------------------------------------------------------
  !> subroutine initGreensCompliantZone
  !! computes the traction on a receiver fault due to slip on a source
  !! fault.
  !!
  !! INPUT:
  !! greens      - Greens functions (g11)
  !! rcv         - receiver fault
  !! src         - source fault
  !! N2          - number of samples COMPLEX(N2/2+1)
  !! dx2         - sampling size
  !! mu          - rigidity
  !! tcz         - compliant zone thickness
  !! mucz        - rigidity of the compliant zone
  !----------------------------------------------------------------------
  SUBROUTINE initGreensCompliantZone(greens,rcv,src,N2,dx2,mu,tcz,mucz)
    TYPE(GREENS_TYPE), INTENT(INOUT) :: greens
    TYPE(FAULT_STRUCT), INTENT(INOUT) :: rcv
    TYPE(FAULT_STRUCT), INTENT(IN) :: src
    INTEGER, INTENT(IN) :: N2
    REAL*8, INTENT(IN) :: dx2
    REAL*8, INTENT(IN) :: mu,tcz,mucz

    ! pi
    REAL*8, PARAMETER :: PI = 3.1415926535897931159979634685441851615905_8

    ! I=SQRT(-1)
    COMPLEX(KIND=8), PARAMETER :: ci = CMPLX(0._8,1._8,8)

    ! counter
    INTEGER :: i2

    ! wavenumber
    REAL*8 :: w2

    ! traction components
    COMPLEX(KIND=8) :: s13

    ! fault-perpendicular distance
    REAL*8 :: x3

    ! dummy
    REAL*8 :: arg

    x3=rcv%x3-src%x3

!$omp do private(w2,s13,arg)
    DO i2=1,N2/2+1

       w2=2*PI*(DBLE(i2-1))/(N2*dx2)

       ! transfer function
       IF (mucz .GE. 0.995*mu) THEN
          s13=-mucz/2*w2
       ELSE
          arg=tcz*w2+ATANH(mucz/mu)
          IF (arg .GE. 200) THEN
             s13=-mucz/2*w2
          ELSE
             s13=-mucz/2*w2*COTH(arg)
          END IF
       END IF

       ! initialize Greens function
       greens%g11(i2)=s13

    END DO
!$omp end do

  END SUBROUTINE initGreensCompliantZone

  !----------------------------------------------------------------------
  !> subroutine computeTraction
  !! computes the traction on a receiver fault due to slip on a source
  !! fault.
  !!
  !! INPUT:
  !! greens      - receiver fault
  !! src         - source fault
  !! N2          - number of samples COMPLEX(N2/2+1)
  !! isInit      - initialize rcv traction?
  !----------------------------------------------------------------------
  SUBROUTINE computeTraction(greens,rcv,src,N2,isInit)
    TYPE(GREENS_TYPE), INTENT(IN) :: greens
    TYPE(FAULT_STRUCT), INTENT(INOUT) :: rcv
    TYPE(FAULT_STRUCT), INTENT(IN) :: src
    INTEGER, INTENT(IN) :: N2
    LOGICAL, INTENT(IN) :: isInit

    ! counter
    INTEGER :: i2

    ! slip components
    COMPLEX(KIND=8) :: s1

    ! traction components
    COMPLEX(KIND=8) :: s13

    IF (isInit) THEN

!$omp do private(s1,s13)
       DO i2=1,N2/2+1

          ! slip components
          s1=CMPLX(src%s1(2*i2-1),src%s1(2*i2),8)

          ! transfer function
          s13=greens%g11(i2)*s1

          rcv%s13(2*i2-1:2*i2)=(/ REAL(s13),AIMAG(s13) /)
       END DO
!$omp end do

    ELSE

!$omp do private(s1,s13)
       DO i2=1,N2/2+1

          ! slip components
          s1=CMPLX(src%s1(2*i2-1),src%s1(2*i2),8)

          ! transfer function
          s13=greens%g11(i2)*s1

          ! update
          rcv%s13(2*i2-1:2*i2)=rcv%s13(2*i2-1:2*i2)+(/ REAL(s13),AIMAG(s13) /)
       END DO
!$omp end do

  END IF

  END SUBROUTINE computeTraction

END MODULE greens_ap

