!-----------------------------------------------------------------------
! Copyright 2020-2025 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.h90"

MODULE types_ap_thermobaric

  IMPLICIT NONE

#ifdef THERMOBARIC
  TYPE FLOW_STRUCT
      SEQUENCE
      REAL*8 :: Vo,c0,n,Q,To,zeta
  END TYPE FLOW_STRUCT

  TYPE HEALING_STRUCT
      SEQUENCE
      REAL*8 :: f0,p,q,H,To
  END TYPE HEALING_STRUCT

  TYPE ROCK_STRUCT
      SEQUENCE
      REAL*8 :: mu0,d0,sigma0,alpha,beta,lambda
      INTEGER :: nFlow,nHealing
      TYPE(FLOW_STRUCT), DIMENSION(:), ALLOCATABLE :: flow
      TYPE(HEALING_STRUCT), DIMENSION(:), ALLOCATABLE :: healing
      CHARACTER(80) :: name
  END TYPE ROCK_STRUCT
#endif

  TYPE PATCH_ELEMENT_STRUCT
     SEQUENCE
     REAL*8 :: Vl
     REAL*8 :: tau0,sig,h
#ifdef BATH
     ! heat equation coefficients
     REAL*8 :: c,wRhoC,DW2,Tb
#endif
#ifdef THERMOBARIC
     INTEGER :: rockType
#endif
     LOGICAL :: dirichlet
  END TYPE PATCH_ELEMENT_STRUCT

  TYPE EVENT_STRUCT
     REAL*8 :: time
     INTEGER*4 :: i
  END TYPE EVENT_STRUCT
  
  TYPE FAULT_STRUCT
     ! offset relative to reference fault
     REAL*8 :: x3
     ! slip components
     REAL*8, DIMENSION(:), ALLOCATABLE :: s1
     ! stress components
     REAL*8, DIMENSION(:), ALLOCATABLE :: s13
  END TYPE FAULT_STRUCT

  TYPE OBSERVATION_STATE_STRUCT
     ! fault index
     INTEGER :: fault
     ! index
     INTEGER :: i2
     ! file number
     INTEGER :: id
     ! sampling rate
     INTEGER :: rate
  END TYPE OBSERVATION_STATE_STRUCT
  
#ifdef NETCDF
  TYPE PROFILE_STRUCT
     SEQUENCE
     INTEGER :: fault,rate
     ! netcdf file
     INTEGER :: ncid,y_varid,z_varid,ncCount
     ! filename
     CHARACTER*256 :: filename
  END TYPE PROFILE_STRUCT
#endif

  ! transfer function
  TYPE GREENS_TYPE
     COMPLEX(KIND=8), DIMENSION(:), ALLOCATABLE :: g11
  END TYPE GREENS_TYPE

  TYPE :: SIMULATION_STRUCT

     ! elastic moduli
     REAL*8 :: mu

#ifdef THERMOBARIC
     ! radiation damping term
     REAL*8 :: damping
#endif

#ifdef BATH
     ! universal gas constant
     REAL*8 :: R
#endif

#ifdef THERMOBARIC
     ! number of rock types
     INTEGER :: nRock

     ! list of rock types
     TYPE(ROCK_STRUCT), DIMENSION(:), ALLOCATABLE :: rock
#endif

     ! simulation time
     REAL*8 :: interval

     ! fault dimension
     INTEGER :: N2

     ! sampling size
     REAL*8 :: dx2

     ! number of fault patches
     INTEGER :: nPatch

     ! patches
     TYPE(PATCH_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: patch

     ! number of parallel faults
     INTEGER :: nFault

     ! Greens function
     TYPE(GREENS_TYPE), DIMENSION(:,:), ALLOCATABLE :: greens

     ! fault structures
     TYPE(FAULT_STRUCT), DIMENSION(:), ALLOCATABLE :: fault

     ! output directory
     CHARACTER(256) :: wdir

     ! filenames
     CHARACTER(256) :: timeFilename

     ! number of observation states
     INTEGER :: nObservationState

     ! observation state (patches and volumes)
     TYPE(OBSERVATION_STATE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationState

#ifdef NETCDF
     ! fault profiles
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: profile
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: profileStress
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: profileSlip
#ifdef BATH
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: profileTemperature
#endif
#endif

     ! number of perturbation events
     INTEGER :: ne

     ! perturbation events
     TYPE(EVENT_STRUCT), DIMENSION(:), ALLOCATABLE :: event

     ! other options
     LOGICAL :: isdryrun=.FALSE.
     LOGICAL :: isExportNetcdf=.FALSE.
     LOGICAL :: isExportStress=.FALSE.
     LOGICAL :: isImportState=.FALSE.
     LOGICAL :: isExportState=.FALSE.
     LOGICAL :: ishelp=.FALSE.
     LOGICAL :: isversion=.FALSE.

  END TYPE SIMULATION_STRUCT

END MODULE types_ap_thermobaric

