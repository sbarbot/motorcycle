!> program Motorcycle (cycles de terremotos) simulates evolution
!! of slip on multiple parallel faults with the spectral boundary
!! integral method with the radiation damping approximation.
!!
!! \mainpage
!!
!! The stress interactions are evaluated in closed-form in the
!! Fourier domain, following the analytical solution
!!
!!    s13h=-mu/2*w2*s1h*exp(-w*abs(x3))
!!
!! where w2=2 pi k2, s2h is the Fourier transform of fault slip
!! (strike-slip), and x3 is the fault-perpendicular distance.
!!
!! The time evolution is evaluated numerically using the 4/5th order
!! Runge-Kutta method with adaptive time steps. The state vector is 
!! as follows:
!!
!! \verbatim
!!
!!    / P1 1       \   +-----------------------+
!!    | .          |   |                       |
!!    | P1 dPatch  |   |                       |
!!    | .          |   |                       |
!!    | .          |   |    nPatch * dPatch    |
!!    | .          |   |                       |
!!    | Pn 1       |   |                       |
!!    | .          |   |                       |
!!    \ Pn dPatch  /   +-----------------------+
!!
!! \endverbatim
!!
!! where nPatch is the number of patches and dPatch is the degrees of 
!! freedom for patch. For each patch, we have the following items in
!! the state vector
!!
!! \verbatim
!!
!!   /  s1   \  1
!!   |  t1   |  .
!!   ! theta*|  .
!!   \  v*   /  dPatch
!!
!! \endverbatim
!!
!! where t1 is the local traction in the strike direction, s1 is the
!! total slip in the strike direction, v*=log10(v) is the logarithm of
!! the norm of the velocity, and theta*=log10(theta) is the logarithm
!! of the state variable in the rate and state friction framework.
!!
!! References:<br>
!!
!!   Barbot S., Modulation of fault strength during the seismic cycle
!!   by grain-size evolution around contact junctions, Tectonophysics,
!!   765, 129-145, doi:j.tecto.2019.05.004, 2019.
!!
!!   Barbot S.,A spectral boundary-integral method for quasi-dynamic 
!!   ruptures of multiple parallel faults, Bulletin of the Seismological
!!   Society of America, doi: 10.1785/0120210004, 2021.
!!
!! \author Sylvain Barbot, University of Southern California (2020-2022)
!----------------------------------------------------------------------
PROGRAM ratestate

#include "macros.h90"

#ifdef NETCDF
  USE exportnetcdf
#endif
  USE fft1d
  USE greens_ap
  USE rk
  USE types_ap

  IMPLICIT NONE

  REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8

  ! error flag
  INTEGER :: ierr

  CHARACTER(512) :: filename
  CHARACTER(512) :: format1,format2
  CHARACTER(512) :: importStateDir

  ! maximum velocity
  REAL*8, DIMENSION(:), ALLOCATABLE :: vMax

  ! moment rate
  REAL*8, DIMENSION(:), ALLOCATABLE :: momentRate

  ! scaling factor
  REAL*8, PARAMETER :: lg10=LOG(1.d1)

  ! state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: y
  ! rate of change of state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: dydt,yscal
  ! temporary variables
  REAL*8, DIMENSION(:), ALLOCATABLE :: ytmp,ytmp1,ytmp2,ytmp3

  ! time
  REAL*8 :: time,t0
  ! time step
  REAL*8 :: dt_try,dt_next,dt_done

  ! counters
  INTEGER :: i,j,k

  ! type of friction law (default)
  INTEGER :: frictionLawType=1

  ! maximum number of time steps (default)
  INTEGER :: maximumIterations=1000000

  ! sampling rate for Netcdf output (default)
  INTEGER :: exportNetcdfRate=20

  ! spatial sampling for Netcdf output (default)
  INTEGER :: exportNetcdfStep=1

  ! verbosity
  INTEGER :: verbose=2

  ! step to skip for slow faults
  INTEGER :: skipSlowStep=1

  ! model parameters
  TYPE(SIMULATION_STRUCT) :: in

!$  INTEGER, EXTERNAL :: omp_get_max_threads,omp_get_num_procs

  ! start time
  time=0.d0

  ! initial tentative time step
  dt_next=1.0d-3

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  IF (in%isdryrun) THEN
     PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isdryrun .OR. in%isversion .OR. in%ishelp) THEN
     STOP
  END IF

  ! fftw initialization
!$  CALL dfftw_init_threads(ierr)
!$  CALL dfftw_plan_with_nthreads(omp_get_max_threads())

  ! state vector
  ALLOCATE(y(in%nPatch*STATE_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vector"

  ! rate of state vector
  ALLOCATE(dydt (in%nPatch*STATE_VECTOR_DGF), &
           yscal(in%nPatch*STATE_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vectors"

  ALLOCATE(ytmp (in%nPatch*STATE_VECTOR_DGF), &
           ytmp1(in%nPatch*STATE_VECTOR_DGF), &
           ytmp2(in%nPatch*STATE_VECTOR_DGF), &
           ytmp3(in%nPatch*STATE_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the rungeKutta work space"

  ! allocate buffer from rk module
#ifdef ODE45
  ALLOCATE(buffer(in%nPatch*STATE_VECTOR_DGF,5),SOURCE=0.0d0,STAT=ierr)
#else
  ALLOCATE(buffer(in%nPatch*STATE_VECTOR_DGF,3),SOURCE=0.0d0,STAT=ierr)
#endif
  IF (ierr>0) STOP "could not allocate the buffer work space"

  ! allocate array of Greens functions
  ALLOCATE(in%greens(in%nFault,in%nFault), STAT=ierr)
  IF (ierr>0) STOP "could not allocate the array of Greens functions"

  ! initialize Greens functions
  DO k=1,in%nFault
     ! loop over receiver faults
     DO j=1,in%nFault
        ! loop over source faults
        ALLOCATE(in%greens(k,j)%g11(in%N2/2+1), STAT=ierr)
        IF (ierr>0) STOP "could not allocate Greens functions"

        ! initialize Greens function
        CALL initGreens(in%greens(k,j),in%fault(k),in%fault(j),in%N2,in%dx2,in%mu)
     END DO
  END DO

  ! report
  OPEN (UNIT=FPTIME,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
  IF (ierr>0) THEN
     WRITE_DEBUG_INFO(102)
     WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
     STOP 1
  END IF

  ALLOCATE(vMax(in%nFault),momentRate(in%nFault))

  ! initialize the y vector
  PRINT '("# initialize state vector.")'
  CALL initStateVector(in%nPatch*STATE_VECTOR_DGF,y,in)
  PRINT 2000

  ! allocate work space for Fourier transform
  DO j=1,in%nFault
     ALLOCATE(in%fault(j)%s1(in%N2+2),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the work space"

     ALLOCATE(in%fault(j)%s13(in%N2+2),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the work space"
  END DO

  ! builds wisdom for the FFT
  CALL fft_init(in%fault(1)%s1,in%N2)

#ifdef NETCDF
  ALLOCATE(in%profile(in%nFault))
  DO i=1,in%nFault
     in%profile(i)%rate=exportNetcdfRate
     WRITE (in%profile(i)%filename,'(a,"/fault-",I2.2,"-log10v.grd")') TRIM(in%wdir),i
  END DO

  ALLOCATE(in%profileStress(in%nFault))
  DO i=1,in%nFault
     in%profileStress(i)%rate=exportNetcdfRate
     WRITE (in%profileStress(i)%filename,'(a,"/fault-",I2.2,"-tau.grd")') TRIM(in%wdir),i
  END DO

  ALLOCATE(in%profileSlip(in%nFault))
  DO i=1,in%nFault
     in%profileSlip(i)%rate=exportNetcdfRate
     WRITE (in%profileSlip(i)%filename,'(a,"/fault-",I2.2,"-slip.grd")') TRIM(in%wdir),i
  END DO

  ! initialize netcdf output
  IF (in%isExportNetcdf) THEN
    DO i=1,in%nFault
       CALL initnc(in%profile(i))
       CALL initnc(in%profileStress(i))
       CALL initnc(in%profileSlip(i))
    END DO
  END IF
#endif

  ! initialize output
  WRITE (format1,'("(I9.9,ES20.13E2,ES19.12E2,",I3,"ES11.4E2)")') in%nFault
  WRITE (format2,'("(ES20.13E2,ES19.12E2,",I3,"ES11.4E2,",I3,"ES20.12E2)")') in%nFault,in%nFault
  WRITE(STDOUT,'("# export to ",a)') TRIM(in%wdir)
  PRINT 2000
  WRITE(STDOUT,'("#       n                time                 dt       vMax")')
  WRITE(STDOUT,format1) 0,time,dt_next,vMax
  WRITE(FPTIME,'("#               time                 dt               vMax         Moment-rate")')

  ! initialize observation patch
  DO j=1,in%nObservationState
     in%observationState(j)%id=100+j
     WRITE (filename,'(a,"/patch-",I2.2,"-",I5.5,".dat")') TRIM(in%wdir), &
             in%observationState(j)%fault, &
             in%observationState(j)%i2
     OPEN (UNIT=in%observationState(j)%id, &
           FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
        STOP 1
     END IF
  END DO

  ! main loop
#ifdef ODE23
  CALL odefun(in%nPatch*STATE_VECTOR_DGF,time,y,dydt)
#endif
  DO i=1,maximumIterations

#ifdef ODE45
     CALL odefun(in%nPatch*STATE_VECTOR_DGF,time,y,dydt)
#endif

     CALL export()

#ifdef NETCDF
     IF (in%isexportnetcdf) THEN
        DO j=1,in%nFault
           IF (0 .EQ. MOD(i,in%profile(j)%rate)) THEN
              CALL exportnc(in%profile(j),j)
           END IF
           IF (0 .EQ. MOD(i,in%profileStress(j)%rate)) THEN
              CALL exportncStress(in%profileStress(j),j)
           END IF
           IF (0 .EQ. MOD(i,in%profileSlip(j)%rate)) THEN
              CALL exportncSlip(in%profileSlip(j),j)
           END IF
        END DO
     END IF
#endif

     dt_try=dt_next
     yscal(:)=ABS(y(:))+ABS(dt_try*dydt(:))+TINY

     t0=time
#ifdef ODE45
     CALL rungeKutta(in%nPatch*STATE_VECTOR_DGF,t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep45)
#else
     CALL rungeKutta(in%nPatch*STATE_VECTOR_DGF,t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep23)
#endif

     time=time+dt_done

     IF (in%isExportState) THEN
        IF (0 .EQ. MOD(i,5000)) THEN
           PRINT '("# save state")'
           CALL exportState()
        END IF
     END IF

     ! end calculation
     IF (in%interval .LE. time) THEN
        EXIT
     END IF
   
  END DO

  ! save state
  IF (in%isExportState) THEN
     CALL exportState()
  END IF

  PRINT '(I9.9," time steps.")', i

  CLOSE(FPTIME)

  ! close observation state files
  DO j=1,in%nObservationState
     CLOSE(in%observationState(j)%id)
  END DO

  DEALLOCATE(vMax,momentRate)
  DEALLOCATE(y,dydt,yscal)
  DEALLOCATE(ytmp,ytmp1,ytmp2,ytmp3)
  DEALLOCATE(buffer)

  DO j=1,in%nFault
     DEALLOCATE(in%fault(j)%s1)
     DEALLOCATE(in%fault(j)%s13)
     DO k=1,in%nFault
        DEALLOCATE(in%greens(k,j)%g11)
     END DO
  END DO
  DEALLOCATE(in%fault)
  DEALLOCATE(in%greens)

#ifdef NETCDF
  IF (in%isexportnetcdf) THEN
     DO j=1,in%nFault
        CALL closeNetcdfUnlimited(in%profile(j)%ncid, &
                                  in%profile(j)%y_varid, &
                                  in%profile(j)%z_varid, &
                                  in%profile(j)%ncCount)
        CALL closeNetcdfUnlimited(in%profileStress(j)%ncid, &
                                  in%profileStress(j)%y_varid, &
                                  in%profileStress(j)%z_varid, &
                                  in%profileStress(j)%ncCount)
        CALL closeNetcdfUnlimited(in%profileSlip(j)%ncid, &
                                  in%profileSlip(j)%y_varid, &
                                  in%profileSlip(j)%z_varid, &
                                  in%profileSlip(j)%ncCount)
     END DO
  END IF

  DEALLOCATE(in%profile)
  DEALLOCATE(in%profileStress)
  DEALLOCATE(in%profileSlip)
#endif

!$  CALL dfftw_cleanup_threads()

2000 FORMAT ("# -----------------------------------------------------------------------------------")
     
CONTAINS

  !-----------------------------------------------------------------------
  !> subroutine exportState
  ! export state vector to disk to allow restart
  !----------------------------------------------------------------------
  SUBROUTINE exportState()
    WRITE (filename,'(a,"/state.ode")') TRIM(in%wdir)
    OPEN(FPSTATE,FILE=filename,FORM="unformatted")
    WRITE(FPSTATE) time
    WRITE(FPSTATE) y
    CLOSE(FPSTATE)
  END SUBROUTINE exportState

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine initnc
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE initnc(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*8, DIMENSION(:), ALLOCATABLE :: x

    INTEGER :: i,ierr

    ! initialize the number of exports
    profile%ncCount=0

    ALLOCATE(x(INT(in%N2/exportNetcdfStep)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate"

    ! loop over all patch elements
    DO i=0,in%N2-1,exportNetcdfStep
       x(i/exportNetcdfStep+1)=REAL((i-in%N2/2)*in%dx2,8)
    END DO

    ! netcdf file compatible with GMT
    CALL openNetcdfUnlimited( &
             profile%filename, &
             INT(in%N2/exportNetcdfStep), &
             x, &
             profile%ncid, &
             profile%y_varid, &
             profile%z_varid)

    DEALLOCATE(x)

  END SUBROUTINE initnc
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportncStress
  ! export time series of log10(v) along profiles
  !----------------------------------------------------------------------
  SUBROUTINE exportncStress(profile,j)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile
    INTEGER, INTENT(IN) :: j

    REAL*4, DIMENSION(:), ALLOCATABLE :: z

    INTEGER :: i2,l

    ! update the export count
    profile%ncCount=profile%ncCount+1

    ALLOCATE(z(INT(in%N2/exportNetcdfStep)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf z values"

    DO i2=1,in%N2,exportNetcdfStep
       ! state vector index
       l=((j-1)*in%N2+i2-1)*STATE_VECTOR_DGF+1

       ! norm of traction rate vector
       z((i2-1)/exportNetcdfStep+1)=REAL(y(l+STATE_VECTOR_TRACTION_STRIKE),4)
    END DO

    CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount, &
            INT(in%N2/exportNetcdfStep),z)

    ! flush every so often
    IF (0 .EQ. MOD(profile%ncCount,50)) THEN
       CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
    END IF

    DEALLOCATE(z)

  END SUBROUTINE exportncStress
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportncSlip
  ! export time series of slip along profiles
  !----------------------------------------------------------------------
  SUBROUTINE exportncSlip(profile,j)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile
    INTEGER, INTENT(IN) :: j

    REAL*4, DIMENSION(:), ALLOCATABLE :: z

    INTEGER :: i2,l

    ! update the export count
    profile%ncCount=profile%ncCount+1

    ALLOCATE(z(INT(in%N2/exportNetcdfStep)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf z values"

    DO i2=1,in%N2,exportNetcdfStep
       ! state vector index
       l=((j-1)*in%N2+i2-1)*STATE_VECTOR_DGF+1

       ! norm of slip
       z((i2-1)/exportNetcdfStep+1)=REAL(y(l+STATE_VECTOR_SLIP_STRIKE),4)
    END DO

    CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount, &
            INT(in%N2/exportNetcdfStep),z)

    ! flush every so often
    IF (0 .EQ. MOD(profile%ncCount,50)) THEN
       CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
    END IF

    DEALLOCATE(z)

  END SUBROUTINE exportncSlip
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportnc
  ! export time series of log10(v) along profiles
  !----------------------------------------------------------------------
  SUBROUTINE exportnc(profile,j)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile
    INTEGER, INTENT(IN) :: j

    REAL*4, DIMENSION(:), ALLOCATABLE :: z

    INTEGER :: i2,l

    ! update the export count
    profile%ncCount=profile%ncCount+1

    ALLOCATE(z(INT(in%N2/exportNetcdfStep)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf z values"

    DO i2=1,in%N2,exportNetcdfStep
       ! state vector index
       l=((j-1)*in%N2+i2-1)*STATE_VECTOR_DGF+1

       ! norm of slip rate vector
       z((i2-1)/exportNetcdfStep+1)=REAL(y(l+STATE_VECTOR_VELOCITY),4)
    END DO

    CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount, &
            INT(in%N2/exportNetcdfStep),z)

    ! flush every so often
    IF (0 .EQ. MOD(profile%ncCount,50)) THEN
       CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
    END IF

    DEALLOCATE(z)

  END SUBROUTINE exportnc

#endif

  !-----------------------------------------------------------------------
  !> subroutine export
  ! write the state variables of patch elements, and other information.
  !----------------------------------------------------------------------
  SUBROUTINE export()

    ! counters
    INTEGER :: j,k,l

    ! format string
    CHARACTER(1024) :: formatString

    ! export observation state
    DO j=1,in%nObservationState

       ! check observation state sampling rate
       IF (0 .EQ. MOD(i-1,in%observationState(j)%rate)) THEN
          formatString="(ES19.12E2"
          DO k=1,STATE_VECTOR_DGF
             formatString=TRIM(formatString)//",X,ES20.12E3,X,ES20.12E3"
          END DO
          formatString=TRIM(formatString)//")"

          l=((in%observationState(j)%fault-1)*in%N2+(in%observationState(j)%i2-1))*STATE_VECTOR_DGF+1
          WRITE (in%observationState(j)%id,TRIM(formatString)) time, &
                    y(l:l+STATE_VECTOR_DGF-1), &
                 dydt(l:l+STATE_VECTOR_DGF-1)
       END IF
    END DO

    WRITE(FPTIME,format2) time,dt_done,vMax,momentRate
    IF (0 .EQ. MOD(i,50)) THEN
       WRITE(STDOUT,format1) i,time,dt_done,vMax
       CALL FLUSH(STDOUT)
       CALL FLUSH(FPTIME)
    END IF

  END SUBROUTINE export

  !-----------------------------------------------------------------------
  !> subroutine initStateVector
  ! initialize the state vector
  !
  ! INPUT:
  ! @param n - number of state elements own by current thread
  ! @param y - the state vector (segment owned by currect thread)
  !----------------------------------------------------------------------
  SUBROUTINE initStateVector(n,y,in)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(OUT)    :: y(n)
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    INTEGER :: i2,j,k,l
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! velocity perturbation
    REAL*8 :: modifier = 0.99d0

    ! initial stress
    REAL*8 :: tau

    ! initial state variable (s)
    REAL*8 :: Tinit

    ! initial velocity
    REAL*8 :: Vinit

    ! zero out state vector
    y=0._8

    ! maximum velocity
    vMax=0._8

    ! moment-rate
    momentRate=0._8

    ! restart
    IF (in%isImportState) THEN
       WRITE (filename,'(a,"/state.ode")') TRIM(importStateDir)
       WRITE (STDOUT,'("# load state vector from ",a)') TRIM(filename)
       OPEN(FPSTATE,FILE=filename,FORM="unformatted")
       READ(FPSTATE) time
       READ(FPSTATE) y
       CLOSE(FPSTATE)

       ! loop over faults
       DO j=1,in%nFault
          ! loop over fault elements
          DO i2=1,in%N2
             ! patch index
             k=(j-1)*in%N2+i2

             ! state vector index
             l=(k-1)*STATE_VECTOR_DGF+1

             Vinit=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

             ! maximum velocity
             vMax(j)=MAX(Vinit,vMax(j))
   
             ! moment-rate
             momentRate(j)=momentRate(j)+Vinit*in%mu*in%dx2
          END DO
       END DO

       RETURN
    END IF

    ! loop over faults
    DO j=1,in%nFault

       ! loop over fault elements
!$OMP PARALLEL DO PRIVATE(k,l,patch,Vinit,tau) REDUCTION(+:momentRate) REDUCTION(MAX:vMax)
       DO i2=1,in%N2

          ! patch index
          k=(j-1)*in%N2+i2

          ! state vector index
          l=(k-1)*STATE_VECTOR_DGF+1

          patch=in%patch(k)

          ! strike slip
          y(l+STATE_VECTOR_SLIP_STRIKE) = 0._8

          ! traction
          IF (0 .GT. patch%tau0) THEN
   
             ! initial velocity
             Vinit=modifier*patch%Vl
   
             SELECT CASE(frictionLawType)
             CASE(1)
                ! multiplicative form of rate-state friction (Barbot, 2019)
                tau = patch%mu0*patch%sig*exp((patch%a-patch%b)/patch%mu0*log(patch%Vl/patch%Vo))
             CASE(2)
                ! additive form of rate-state friction (Ruina, 1983)
                tau = patch%sig*(patch%mu0+(patch%a-patch%b)*log(patch%Vl/patch%Vo))
             CASE(3)
                ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
                tau = patch%a*patch%sig*ASINH(patch%Vl/2/patch%Vo*exp((patch%mu0+patch%b*log(patch%Vo/patch%Vl))/patch%a))
             CASE DEFAULT
                WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
                WRITE_DEBUG_INFO(100)
                STOP 3
             END SELECT
          ELSE

             tau = patch%tau0

             ! set initial velocity to local loading rate
             Vinit = patch%Vl
          END IF

          ! set state variable log10(theta) compatible with velocity and traction
          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2019)
             Tinit=(LOG(patch%L/patch%Vo)+(patch%a*LOG(patch%Vo/patch%Vl)+patch%mu0*LOG(tau/patch%mu0/patch%sig))/patch%b)/lg10
          CASE(2)
             ! additive form of rate-state friction (Ruina, 1983)
             Tinit=(LOG(patch%L/patch%Vo)+(patch%a*LOG(patch%Vo/patch%Vl)+tau/patch%sig-patch%mu0)/patch%b)/lg10
          CASE(3)
             ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
             Tinit=(LOG(patch%L/patch%Vo) &
                    +patch%a/patch%b*LOG(2*patch%Vo/patch%Vl*SINH(tau/patch%a/patch%sig)) &
                    -patch%mu0/patch%b)/lg10
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT

          IF (patch%dirichlet) THEN
             Vinit=patch%Vl
          END IF
      
          ! traction in strike direction
          y(l+STATE_VECTOR_TRACTION_STRIKE) = tau
      
          ! state variable log10(theta)
          y(l+STATE_VECTOR_STATE_1) = Tinit
      
          ! maximum velocity
          vMax(j)=MAX(Vinit,vMax(j))
   
          ! moment-rate
          momentRate(j)=momentRate(j)+(Vinit-patch%Vl)*in%mu*in%dx2

          ! slip velocity log10(V)
          y(l+STATE_VECTOR_VELOCITY) = log(Vinit)/lg10

       END DO
!$OMP END PARALLEL DO
    END DO

  END SUBROUTINE initStateVector

  !-----------------------------------------------------------------------
  !> subroutine odefun
  ! evalutes the derivative of the state vector
  !
  ! @param n - number of state elements own by current thread
  ! @param m - degrees of freedom
  !
  ! DESCRIPTION:
  !   1- extract slip velocity and strain rate from state vector
  !   2- calculate the rate of traction and rate of stress
  !   3- calculate the rate of remaining state variables
  !----------------------------------------------------------------------
  SUBROUTINE odefun(n,time,y,dydt)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: time
    REAL*8, INTENT(IN)    :: y(n)
    REAL*8, INTENT(INOUT) :: dydt(n)

    INTEGER :: i2,j,k,l
    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    REAL*8 :: correction

    ! traction components in the strike direction
    REAL*8 :: ts

    ! scalar rate of shear traction
    REAL*8 :: dtau

    ! velocity scalar
    REAL*8 :: velocity

    ! friction
    REAL*8 :: friction

    ! normal stress
    REAL*8 :: sigma

    ! modifier for the arcsinh form of rate-state friction
    REAL*8 :: reg

    ! maximum velocity
    vMax=0._8

    ! initialize moment-rate
    momentRate=0._8

    !--------------------------------------------------------------------
    ! step 1/3 - extract slip velocity and strain rate from state vector
    !--------------------------------------------------------------------

    ! loop over faults
    DO j=1,in%nFault
       ! loop of fault elements
!$OMP PARALLEL DO PRIVATE(k,l,patch,ts,velocity) REDUCTION(MAX:vMax)
       DO i2=1,in%N2
          
          ! patch index
          k=(j-1)*in%N2+i2
          ! state vector index
          l=(k-1)*STATE_VECTOR_DGF+1

          patch=in%patch(k)

          ! traction
          ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
   
          ! slip velocity
          velocity=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

          ! maximum velocity
          vMax(j)=MAX(velocity,vMax(j))

          ! update state vector (rate of slip components)
          dydt(l+STATE_VECTOR_SLIP_STRIKE)=velocity
 
          ! slip velocity
          in%fault(j)%s1(i2)=velocity-patch%Vl

       END DO
!$OMP END PARALLEL DO
    END DO

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

    ! forward Fourier transform source
    DO j=1,in%nFault
       IF ((vMax(j) .GE. 1d-6*MAXVAL(vMax)) .OR. (0 .EQ. MOD(i,skipSlowStep))) THEN
          CALL fft1(in%fault(j)%s1,in%N2,FFT_FORWARD)
       END IF
    END DO

    ! loop over receiver faults
    DO k=1,in%nFault
       ! loop over source faults
       DO j=1,in%nFault

          ! traction component on fault k
          CALL computeTraction(in%greens(k,j),in%fault(k),in%fault(j),in%N2,(j .EQ. 1))

       END DO
    END DO

    ! inverse Fourier transform receivers
    DO j=1,in%nFault
       CALL fft1(in%fault(j)%s13,in%N2,FFT_INVERSE)
    END DO

    !-----------------------------------------------------------------
    ! step 3/3 - calculate the rate of remaining state variables
    !-----------------------------------------------------------------

    ! cumulative patch index
    k=1
    ! element index in state vector
    l=1
    ! loop over faults
    DO j=1,in%nFault
       ! loop of fault elements
!$OMP PARALLEL DO PRIVATE(k,l,patch,ts,velocity,dtau,sigma,friction,reg,correction) REDUCTION(+:momentRate) 
       DO i2=1,in%N2

          ! cumulative patch index
          k=(j-1)*in%N2+i2
          ! state vector index
          l=(k-1)*STATE_VECTOR_DGF+1

          patch=in%patch(k)

          ! constant velocity boundary condition
          IF (patch%dirichlet) THEN
             dydt(l+STATE_VECTOR_TRACTION_STRIKE)=0._8
             dydt(l+STATE_VECTOR_STATE_1)=0._8
             dydt(l+STATE_VECTOR_VELOCITY)=0._8
             CYCLE
          END IF

          ! slip velocity
          velocity=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)
      
          ! moment-rate
          momentRate(j)=momentRate(j)+(velocity-patch%Vl)*in%mu*in%dx2
      
          ! rate of state
          dydt(l+STATE_VECTOR_STATE_1)=(EXP(-y(l+STATE_VECTOR_STATE_1)*lg10)-velocity/patch%L)/lg10
      
          ! scalar rate of shear traction
          dtau=in%fault(j)%s13(i2)
                
          ! normal stress
          sigma=patch%sig
      
          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2019)
             friction=patch%mu0*exp(patch%a/patch%mu0*LOG(velocity/patch%Vo) &
                                   +patch%b/patch%mu0*(y(l+STATE_VECTOR_STATE_1)*lg10+log(patch%Vo/patch%L)))
      
             ! acceleration (1/V dV/dt) / log(10)
             dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*sigma*friction/patch%mu0*dydt(l+STATE_VECTOR_STATE_1)*lg10) / &
                  (patch%a*sigma*friction/patch%mu0+patch%damping*velocity) / lg10
          CASE(2)
             ! additive form of rate-state friction (Ruina, 1983)

             ! acceleration
             dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10) / &
                  (patch%a*sigma+patch%damping*velocity) / lg10
          CASE(3)
             ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
             reg=2.0d0*patch%Vo/velocity*exp(-(patch%mu0+patch%b*(y(STATE_VECTOR_STATE_1)*lg10-log(patch%L/patch%Vo)))/patch%a)
             reg=1.0d0/SQRT(1._8+reg**2)

             ! acceleration
             dydt(l+STATE_VECTOR_VELOCITY)= &
                     (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10*reg) / &
                     (patch%a*sigma*reg+patch%damping*velocity) / lg10
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
      
          ! correction
          correction=patch%damping*velocity*dydt(l+STATE_VECTOR_VELOCITY)*lg10
      
          ! traction rate
          dydt(l+STATE_VECTOR_TRACTION_STRIKE)=in%fault(j)%s13(i2)-correction
       END DO
!$OMP END PARALLEL DO
    END DO

  END SUBROUTINE odefun

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@ntu.edu.sg)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE getopt_m

    TYPE(SIMULATION_STRUCT), INTENT(OUT) :: in

    CHARACTER :: ch
    CHARACTER(512) :: dataline
    CHARACTER(256) :: filename
    INTEGER :: iunit,noptions
    LOGICAL :: new
    TYPE(PATCH_ELEMENT_STRUCT) :: patchTemp
    TYPE(PATCH_ELEMENT_STRUCT) :: patchMin,patchMax
    TYPE(OPTION_S) :: opts(15)

    INTEGER :: i2,k,ierr

    ! define long options, such as --dry-run
    ! parse the command line for options
    opts( 1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts( 2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts( 3)=OPTION_S("epsilon",.TRUE.,'e')
    opts( 4)=OPTION_S("export-netcdf",.FALSE.,'n')
    opts( 5)=OPTION_S("export-stress",.FALSE.,'s')
    opts( 6)=OPTION_S("export-netcdf-rate",.TRUE.,CHAR(23))
    opts( 7)=OPTION_S("export-netcdf-step",.TRUE.,CHAR(24))
    opts( 8)=OPTION_S("import-state",.TRUE.,'t')
    opts( 9)=OPTION_S("export-state",.FALSE.,'x')
    opts(10)=OPTION_S("friction-law",.TRUE.,'f')
    opts(11)=OPTION_S("maximum-step",.TRUE.,'m')
    opts(12)=OPTION_S("maximum-iterations",.TRUE.,'i')
    opts(13)=OPTION_S("skip-slow-step",.TRUE.,CHAR(25))
    opts(14)=OPTION_S("help",.FALSE.,'h')
    opts(15)=OPTION_S("verbose",.TRUE.,'v')

    noptions=0
    DO
       ch=getopt("he:f:i:m:nt:v:",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isversion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isdryrun=.TRUE.
       CASE('e')
          ! numerical accuracy (variable epsilon sits in the ode45 module)
          READ(optarg,*) epsilon
          noptions=noptions+1
       CASE('f')
          ! type of friction law
          READ(optarg,*) frictionLawType
          noptions=noptions+1
       CASE('i')
          ! maximum number of iterations
          READ(optarg,*) maximumIterations
          noptions=noptions+1
       CASE('m')
          ! maximum time step (variable maximumTimeStep sits in the ode45 module)
          READ(optarg,*) maximumTimeStep
          noptions=noptions+1
       CASE('n')
          ! export in netcdf format
          in%isExportNetcdf=.TRUE.
       CASE('s')
          ! export in netcdf format
          in%isExportStress=.TRUE.
       CASE(CHAR(23))
          ! NETCDF export rate
          READ(optarg,*) exportNetcdfRate
          noptions=noptions+1
       CASE(CHAR(24))
          ! NETCDF export step
          READ(optarg,*) exportNetcdfStep
          noptions=noptions+1
       CASE(CHAR(25))
          ! steps to skip for slow faults
          READ(optarg,*) skipSlowStep
          noptions=noptions+1
       CASE('t')
          ! export in netcdf format
          in%isImportState=.TRUE.
          READ(optarg,'(a)') importStateDir
          noptions=noptions+1
       CASE('x')
          ! export in netcdf format
          in%isExportState=.TRUE.
       CASE('h')
          ! option help
          in%ishelp=.TRUE.
       CASE('v')
          ! verbosity
          READ(optarg,*) verbose
          noptions=noptions+1
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%ishelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO

    IF (in%isversion) THEN
       CALL printversion()
       ! abort parameter input
       STOP
    END IF

    IF (in%ishelp) THEN
       CALL printhelp()
       ! abort parameter input
       STOP
    END IF

    in%nPatch=0

    PRINT 2000
    PRINT '("# MOTORCYCLE")'
    PRINT '("# quasi-dynamic earthquake cycle simulation on parallel strike-slip")'
    PRINT '("# faults with the spectral boundary integral method.")'
    SELECT CASE(frictionLawType)
    CASE(1)
       PRINT '("# friction law: multiplicative form of rate-state friction (Barbot, 2019)")'
    CASE(2)
       PRINT '("# friction law: additive form of rate-state friction (Ruina, 1983)")'
    CASE(3)
       PRINT '("# friction law: arcsinh form of rate-state friction (Rice & BenZion, 1996)")'
    CASE DEFAULT
       WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
       WRITE_DEBUG_INFO(100)
       STOP 3
    END SELECT
    PRINT '("# numerical accuracy:     ",ES11.4)', epsilon
    PRINT '("# maximum iterations:     ",I11)', maximumIterations
    PRINT '("# maximum time step:     ",ES12.4)', maximumTimeStep
    IF (in%isExportNetcdf) THEN
       PRINT '("# export velocity to netcdf:      yes")'
    ELSE
       PRINT '("# export velocity to netcdf:       no")'
    END IF
!$  PRINT '("# number of threads:          ",I3.3,"/",I3.3)', &
!$                  omp_get_max_threads(),omp_get_num_procs()
       PRINT 2000

    IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
       ! read from input file
       iunit=25
       CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
       OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
    ELSE
       ! get input parameters from standard input
       iunit=5
    END IF

    PRINT '("# output directory")'
    CALL getdata(iunit,dataline)
    READ (dataline,'(a)') in%wdir
    PRINT '(2X,a)', TRIM(in%wdir)

    in%timeFilename=TRIM(in%wdir)//"/time.dat"

    ! test write permissions on output directory
    OPEN (UNIT=FPTIME,FILE=in%timeFilename,POSITION="APPEND",&
            IOSTAT=ierr,FORM="FORMATTED")
    IF (ierr>0) THEN
       WRITE_DEBUG_INFO(102)
       WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
       STOP 1
    END IF
    CLOSE(FPTIME)

    PRINT '("# rigidity")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%mu
    PRINT '(ES9.2E1)', in%mu

    IF (0 .GT. in%mu) THEN
       WRITE_DEBUG_INFO(-1)
       WRITE (STDERR,'(a)') TRIM(dataline)
       WRITE (STDERR,'("input error: shear modulus must be positive")')
       STOP 2
    END IF

    PRINT '("# time interval")'
       CALL getdata(iunit,dataline)
    READ  (dataline,*) in%interval
    PRINT '(ES20.12E2)', in%interval

    IF (in%interval .LE. 0._8) THEN
       WRITE (STDERR,'("**** error **** ")')
       WRITE (STDERR,'(a)') TRIM(dataline)
       WRITE (STDERR,'("simulation time must be positive. exiting.")')
       STOP 1
    END IF

    PRINT '("# number of faults")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%nFault
    PRINT '(I2)', in%nFault

    PRINT '("# grid dimension (N2)")'
    CALL getdata(iunit,dataline)
    READ (dataline,*) in%N2
    PRINT '(2I5)', in%N2

    ! number of overall fault patches
    in%nPatch=in%nFault*in%N2

    PRINT '("# sampling (dx2)")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%dx2
    PRINT '(2ES9.2E1)', in%dx2


    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    !        A L L O C A T E   M E M O R Y
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -

    ! cumulative patch index
    ALLOCATE(in%patch(in%nPatch),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the patch list"

    ALLOCATE(in%fault(in%nFault),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the fault list"

    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    !        F R I C T I O N   P R O P E R T I E S
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -

    ! cumulative patch index
    k=1
    DO j=1,in%nFault
       PRINT 2000
       IF (1 .EQ. j) THEN
          in%fault(j)%x3=0
       ELSE
          PRINT '("# relative fault position (m)")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%fault(j)%x3
          PRINT '(3ES9.2E1)', in%fault(j)%x3
       END IF

       PRINT '("#    n     tau0     mu0     sig       a        b       L      Vo G/(2Vs)       Vl  BC")'
       PRINT 2000

       ! parameter range
       patchMin%tau0=HUGE(1.e0)
       patchMin%mu0=HUGE(1.e0)
       patchMin%sig=HUGE(1.e0)
       patchMin%a=HUGE(1.e0)
       patchMin%b=HUGE(1.e0)
       patchMin%L=HUGE(1.e0)
       patchMin%Vo=HUGE(1.e0)
       patchMin%damping=HUGE(1.e0)
       patchMin%Vl=HUGE(1.e0)
       patchMin%dirichlet=.TRUE.

       patchMax%tau0=-HUGE(1.e0)
       patchMax%mu0=-HUGE(1.e0)
       patchMax%sig=-HUGE(1.e0)
       patchMax%a=-HUGE(1.e0)
       patchMax%b=-HUGE(1.e0)
       patchMax%L=-HUGE(1.e0)
       patchMax%Vo=-HUGE(1.e0)
       patchMax%damping=-HUGE(1.e0)
       patchMax%Vl=-HUGE(1.e0)
       patchMax%dirichlet=.FALSE.

       patchTemp%tau0=HUGE(1.e0)
       patchTemp%mu0=HUGE(1.e0)
       patchTemp%sig=HUGE(1.e0)
       patchTemp%a=HUGE(1.e0)
       patchTemp%b=HUGE(1.e0)
       patchTemp%L=HUGE(1.e0)
       patchTemp%Vo=HUGE(1.e0)
       patchTemp%damping=HUGE(1.e0)
       patchTemp%Vl=HUGE(1.e0)
       patchTemp%dirichlet=.TRUE.

       new=.FALSE.
       DO i2=1,in%N2

          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) i
          IF (0 .GT. i) THEN
             ! compressed input, take previous value
             in%patch(k)%tau0=patchTemp%tau0
             in%patch(k)%mu0=patchTemp%mu0
             in%patch(k)%sig=patchTemp%sig
             in%patch(k)%a=patchTemp%a
             in%patch(k)%b=patchTemp%b
             in%patch(k)%L=patchTemp%L
             in%patch(k)%Vo=patchTemp%Vo
             in%patch(k)%damping=patchTemp%damping
             in%patch(k)%Vl=patchTemp%Vl
             in%patch(k)%dirichlet=patchTemp%dirichlet
             i=-i
             new=.FALSE.
          ELSE
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%patch(k)%tau0, &
                   in%patch(k)%mu0, &
                   in%patch(k)%sig, &
                   in%patch(k)%a, &
                   in%patch(k)%b, &
                   in%patch(k)%L, &
                   in%patch(k)%Vo, &
                   in%patch(k)%damping, &
                   in%patch(k)%Vl, &
                   in%patch(k)%dirichlet
             new=.TRUE.
          END IF

          IF ((2 .LE. verbose) .OR. (new) .OR. (4 .GE. i2) .OR. (in%N2-3 .LE. i2)) THEN
             PRINT '(I6,ES9.2E1,3ES8.2E1,ES9.2E1,3ES8.2E1,ES9.2E2,L4)',i, &
                  in%patch(k)%tau0, &
                  in%patch(k)%mu0, &
                  in%patch(k)%sig, &
                  in%patch(k)%a, &
                  in%patch(k)%b, &
                  in%patch(k)%L, &
                  in%patch(k)%Vo, &
                  in%patch(k)%damping, &
                  in%patch(k)%Vl, &
                  in%patch(k)%dirichlet
          END IF

          IF (i .NE. k-(j-1)*in%N2) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'("invalid friction property definition for patch")')
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: unexpected index")')
             STOP 1
          END IF

          ! parameter range
          patchMin%tau0     =MIN(patchMin%tau0,   in%patch(k)%tau0)
          patchMin%mu0      =MIN(patchMin%mu0,    in%patch(k)%mu0)
          patchMin%sig      =MIN(patchMin%sig,    in%patch(k)%sig)
          patchMin%a        =MIN(patchMin%a,      in%patch(k)%a)
          patchMin%b        =MIN(patchMin%b,      in%patch(k)%b)
          patchMin%L        =MIN(patchMin%L,      in%patch(k)%L)
          patchMin%Vo       =MIN(patchMin%Vo,     in%patch(k)%Vo)
          patchMin%damping  =MIN(patchMin%damping,in%patch(k)%damping)
          patchMin%Vl       =MIN(patchMin%Vl,     in%patch(k)%Vl)
          patchMin%dirichlet=patchMin%dirichlet .AND. in%patch(k)%dirichlet

          patchMax%tau0     =MAX(patchMax%tau0,   in%patch(k)%tau0)
          patchMax%mu0      =MAX(patchMax%mu0,    in%patch(k)%mu0)
          patchMax%sig      =MAX(patchMax%sig,    in%patch(k)%sig)
          patchMax%a        =MAX(patchMax%a,      in%patch(k)%a)
          patchMax%b        =MAX(patchMax%b,      in%patch(k)%b)
          patchMax%L        =MAX(patchMax%L,      in%patch(k)%L)
          patchMax%Vo       =MAX(patchMax%Vo,     in%patch(k)%Vo)
          patchMax%damping  =MAX(patchMax%damping,in%patch(k)%damping)
          patchMax%Vl       =MAX(patchMax%Vl,     in%patch(k)%Vl)
          patchMax%dirichlet=patchMax%dirichlet .OR. in%patch(k)%dirichlet

          ! save previous value for compression
          patchTemp%tau0=     in%patch(k)%tau0
          patchTemp%mu0=      in%patch(k)%mu0
          patchTemp%sig=      in%patch(k)%sig
          patchTemp%a=        in%patch(k)%a
          patchTemp%b=        in%patch(k)%b
          patchTemp%L=        in%patch(k)%L
          patchTemp%Vo=       in%patch(k)%Vo
          patchTemp%damping=  in%patch(k)%damping
          patchTemp%Vl=       in%patch(k)%Vl
          patchTemp%dirichlet=in%patch(k)%dirichlet

          k=k+1
       END DO

       PRINT 2000
       PRINT '("   min",ES9.2E1,3ES8.2E1,ES9.2E1,3ES8.2E1,ES9.2E2,L4)', &
            patchMin%tau0, &
            patchMin%mu0, &
            patchMin%sig, &
            patchMin%a, &
            patchMin%b, &
            patchMin%L, &
            patchMin%Vo, &
            patchMin%damping, &
            patchMin%Vl, &
            patchMin%dirichlet

       PRINT '("   max",ES9.2E1,3ES8.2E1,ES9.2E1,3ES8.2E1,ES9.2E2,L4)', &
            patchMax%tau0, &
            patchMax%mu0, &
            patchMax%sig, &
            patchMax%a, &
            patchMax%b, &
            patchMax%L, &
            patchMax%Vo, &
            patchMax%damping, &
            patchMax%Vl, &
            patchMax%dirichlet

    END DO
       
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    !       O B S E R V A T I O N   P A T C H E S
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    PRINT 2000
    PRINT '("# number of observation patches")'
       CALL getdata(iunit,dataline)
    READ  (dataline,*) in%nObservationState
    PRINT '(I5)', in%nObservationState
    IF (0 .LT. in%nObservationState) THEN
       ALLOCATE(in%observationState(in%nObservationState),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the observation patches"
       PRINT 2000
       PRINT '("#   n fault     i2 rate")'
       PRINT 2000
       DO k=1,in%nObservationState
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) i, &
                  in%observationState(k)%fault, &
                  in%observationState(k)%i2, &
                  in%observationState(k)%rate

          PRINT '(I5,X,I5,X,I6,X,I4)', i, &
                  in%observationState(k)%fault, &
                  in%observationState(k)%i2, &
                  in%observationState(k)%rate

          IF (in%nFault .LT. in%observationState(k)%fault) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: wrong fault index")')
             STOP 1
          END IF

          IF (in%N2 .LT. in%observationState(k)%i2) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: invalid coordinate i2")')
             STOP 1
          END IF

          IF (0 .GE. in%observationState(k)%rate) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: invalid subsampling rate")')
             STOP 1
          END IF

          IF (i .NE. k) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: unexpected index")')
             STOP 1
          END IF
       END DO
    END IF

    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    !                  E V E N T S
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    PRINT '("# number of events")'
    CALL getdata(iunit,dataline)
    READ (dataline,*) in%ne
    PRINT '(I5)', in%ne
    IF (in%ne .GT. 0) ALLOCATE(in%event(in%ne),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the event list"
    
    DO i=1,in%ne
       IF (1 .NE. i) THEN
          PRINT '("# time of next event")'
          CALL getdata(iunit,dataline)
          READ (dataline,*) in%event(i)%time
          in%event(i)%i=i-1
          PRINT '(ES9.2E1)', in%event(i)%time

          IF (in%event(i)%time .LE. in%event(i-1)%time) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'(a,a)') "input file error. ", &
                  "timing of perturbations must increase, quiting."
             STOP 1
          END IF
       ELSE
          in%event(1)%time=0._8
          in%event(1)%i=0
       END IF

    END DO

    ! test the presence of dislocations for coseismic calculation
    IF ((in%nPatch .EQ. 0) .AND. &
        (in%interval .LE. 0._8)) THEN

       WRITE_DEBUG_INFO(300)
       WRITE (STDERR,'("nothing to do. exiting.")')
       STOP 1
    END IF

    PRINT 2000
    ! flush standard output
    CALL FLUSH(6)      

2000 FORMAT ("# -----------------------------------------------------------------------------------")
   
  END SUBROUTINE init
   
  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message.
  !-----------------------------------------------
  SUBROUTINE printhelp()

    PRINT '("usage:")'
    PRINT '("")'
    PRINT '("OMP_NUM_THREADS=2 motorcycle-ap-thermobaric-serial [-h] [--dry-run] [--help] [--epsilon 1e-6]")'
    PRINT '("                                         [--export-netcdf] [--export-stress]")'
    PRINT '("                                         [--export-state] [--import state wdir] [filename]")'
    PRINT '("")'
    PRINT '("options:")'
    PRINT '("   -h                      prints this message and aborts calculation")'
    PRINT '("   --dry-run               abort calculation, only output geometry")'
    PRINT '("   --help                  prints this message and aborts calculation")'
    PRINT '("   --version               print version number and exit")'
    PRINT '("   --epsilon               set the numerical accuracy [1E-6]")'
    PRINT '("   --export-netcdf         export information to .grd netcdf files")'
    PRINT '("   --export-stress         export stress components")'
    PRINT '("   --export-state          export the state vector periodically to facilitate restart")'
    PRINT '("   --import-state wdir     import the state vector from a previous simulation")'
    PRINT '("   --friction-law          type of friction law [1]")'
    PRINT '("       1: multiplicative   form of rate-state friction (Barbot, 2019)")'
    PRINT '("       2: additive         form of rate-state friction (Ruina, 1983)")'
    PRINT '("       3: arcsinh          form of rate-state friction (Rice & Benzion, 1996)")'
    PRINT '("   --maximum-iterations    set the maximum time step [1000000]")'
    PRINT '("   --maximum-step          set the maximum time step [Inf]")'
    PRINT '("")'
    PRINT '("description:")'
    PRINT '("   simulates elasto-dynamics on parallel strike-slip faults")'
    PRINT '("   following the radiation-damping approximation")'
    PRINT '("   using the spectral boundary integral method.")'
    PRINT '("")'
    PRINT '("see also: ""man motorcycle""")'
    PRINT '("")'
    PRINT '("                ,      ")'
    PRINT '("             .-/c-.,:: ")'
    PRINT '("             (_)''==(_) ")'
    PRINT '("")'
    CALL FLUSH(6)

  END SUBROUTINE printhelp

  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version.
  !-----------------------------------------------
  SUBROUTINE printversion()

    PRINT '("motorcycle-ap-thermobaric-serial version 1.0.0, compiled on ",a)', __DATE__
    PRINT '("")'
    CALL FLUSH(6)

  END SUBROUTINE printversion

END PROGRAM ratestate

