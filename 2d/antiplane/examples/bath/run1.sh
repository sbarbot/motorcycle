#!/bin/bash -e

# thermally weakening patch embedded in a strengthening
# generating slow-slip events.

selfdir=$(dirname $0)

WDIR=output1

N2=1024
DX=20

if [ ! -e "$WDIR" ]; then
	echo "# adding directory $WDIR"
	mkdir -p "$WDIR"
fi

OMP_NUM_THREADS=2 motorcycle-ap-ratestate-bath-serial \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--export-netcdf-rate 20 \
	--maximum-iterations 1000000 <<EOF
# output directory
$WDIR
# elastic moduli (MPa), universal gas constant (J/K/mol)
30e3 8.314462618153
# time interval (s)
1e10
# number of faults
1
# grid dimension (N2)
$N2
# sampling (dx2)
$DX
#   n  tau0   mu0   sig   a   b   L   Vo   G/(2Vs)   Vl Dirichlet
$(echo "" | awk -v n2=$N2 -v dx=$DX '
	{
	for (i2=0;i2<n2;i2++){
		x2=10e3+(i2-n2/2)*dx; 
		tau0=-1;
		mu0=0.6;
		sig=50;
		a=1.0e-2;
		b=a-4e-3;
		L=5e-3;
		Vo=1e-6;
		damping=5;
		Vl=1e-9;
		dirichlet=((i2<10) || (i2>=(n2-10)))?"T":"F";
		printf "%5d %e %e %e %e %e %e %e %e %e %s\n", 
	        	i2+1, tau0, mu0, sig, a, b, L, Vo, damping, Vl, dirichlet;
	}
}')
#   n   Q  H  D  W  w  rhoc Tb
$(echo "" | awk -v fw=1.5e3 -v n2=$N2 -v dx=$DX '{ 
	for (i2=0;i2<n2;i2++){
		x2=10e3+(i2-n2/2)*dx; 
		Q=90e3;
		H=60e3;
		D=1e-6;
		W=2;
		w=(x2>17e3 && x2<(17e3+fw))?5e-4:1e-0;
		rhoc=2.6;
		Tb=600+273.15;
		printf "%06d %e %e %e %e %e %e %e\n", i2+1,Q,H,D,W,w,rhoc,Tb;
	}
}')
# number of observation patches
1
# n fault i2 rate
  1     1 $(echo "" | awk -v n2=$N2 '{print n2/2+1,1}')
# number of events
0
EOF

