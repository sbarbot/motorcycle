#!/bin/bash -e

# test for a single fault

selfdir=$(dirname $0)

WDIR=$selfdir/output1c

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

N2=16384
DX=2.5

OMP_NUM_THREADS=2 motorcycle-ap-ratestate-compliant-serial \
	--verbose 1 \
	--epsilon 1e-6 \
	--export-state \
	--export-netcdf \
	--export-netcdf-rate 20 \
	--export-netcdf-step 4 \
	--maximum-step 3.15e7 \
	--maximum-iterations 1000000 \
	--friction-law 1 <<EOF
# output directory
$WDIR
# rigidity, compliant zone thickness, compliant zone rigidity
30e3 1e2 10e3
# time interval
1e10
# number of faults
1
# grid dimension (N2)
$N2
# sampling (dx2)
$DX
#   n  tau0   mu0   sig   a   b   L   Vo   G/(2Vs)   Vl Dirichlet
`echo "" | awk -v n2=$N2 -v dx=$DX '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	BEGIN{
	c=1;
	tau0_p=-1;mu0_p =-1;sig_p=-1;
	a_p=-1;b_p=-1;L_p=-1;
	Vo_p=-1;damping_p=-1;
	Vl_p=-1;dirichlet_p="T";
	}{
	for (i2=0;i2<n2;i2++){
		x2=(i2-n2/2)*dx; 
		tau0=-1;
		L=1e-2; 
		a=1e-2; 
		b=(abs(x2)<2.5e3)?a+4.0e-3:a-4e-3;
		mu0=0.6; 
		sig=1e2; 
		Vo=1e-6; 
		Vl=1e-9;
		damping=5;
		if (10>=i2 || (n2-10)<=i2){
			dirichlet="T"
		} else {
			dirichlet="F";
		}
		if (1==boxcar((x2-0.5e3)/1e3)){
			tau0=(mu0+(a-b)*log(1e-10/Vo))*sig;
		}
		if ((tau0_p == tau0) && (mu0_p==mu0) && (sig_p==sig) && (a_p==a) && (b_p==b) && (L_p==L) && 
		    (Vo_p==Vo) && (damping_p==damping) && (Vl_p==Vl) && (dirichlet_p==dirichlet)){
			printf "%5d\n",-c;
		} else {
			printf "%5d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d %10.2e %s\n", 
	        			c,  tau0,   mu0,   sig,     a,     b,     L,    Vo, damping, Vl, dirichlet;
		}
		c++;
		tau0_p=tau0;mu0_p=mu0;sig_p=sig;
		a_p=a;b_p=b;L_p=L;
		Vo_p=Vo;damping_p=damping;
		Vl_p=Vl;dirichlet_p=dirichlet;
	}
}'`
# number of observation patches
1
# n fault i2 rate
  1     1 `echo "" | awk -v n2=$N2 '{print n2/2+1,1}'`
# number of events
0
EOF

