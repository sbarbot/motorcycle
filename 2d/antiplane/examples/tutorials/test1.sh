#!/bin/bash -e

# seismic cycle on a single fault
# h* = 750 m; W = 5e3; Ru = 6.66

selfdir=$(dirname $0)

WDIR=$selfdir/test-results/output1

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir -p $WDIR
fi

N2=1024
DX=40

OMP_NUM_THREADS=2 motorcycle-ap-ratestate-serial \
	--verbose 1 \
	--epsilon 1e-6 \
	--export-state \
	--export-stress \
	--export-netcdf \
	--export-netcdf-rate 20 \
	--export-netcdf-step 4 \
	--maximum-step 3.15e7 \
	--maximum-iterations 20000 \
	--friction-law 1 <<EOF
# output directory
$WDIR
# rigidity
30e3
# time interval
1e10
# number of faults
1
# grid dimension (N2)
$N2
# sampling (dx2)
$DX
#   n  tau0   mu0   sig   a   b   L   Vo   G/(2Vs)   Vl Dirichlet
$(echo "" | awk -v n2="$N2" -v dx="$DX" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	BEGIN{
	c=1;
	tau0_p=-1;mu0_p =-1;sig_p=-1;
	a_p=-1;b_p=-1;L_p=-1;
	Vo_p=-1;damping_p=-1;
	Vl_p=-1;dirichlet_p="T";
	}{
	for (i2=0;i2<n2;i2++){
		x2=(i2-n2/2)*dx; 
		tau0=-1;
		L=1e-2; 
		a=1e-2; 
		b=(abs(x2)<2.5e3)?a+4.0e-3:a-4e-3;
		mu0=0.6; 
		sig=1e2; 
		Vo=1e-6; 
		Vl=1e-9;
		damping=5;
		if (10>=i2 || (n2-10)<=i2){
			dirichlet="T"
		} else {
			dirichlet="F";
		}
		if (1==boxcar((x2-0.5e3)/1e3)){
			tau0=mu0*sig*(1.1e-9/Vo)^(a/mu0)*(L/Vl)^(b/mu0);
		}
		if ((tau0_p == tau0) && (mu0_p==mu0) && (sig_p==sig) && (a_p==a) && (b_p==b) && (L_p==L) && 
		    (Vo_p==Vo) && (damping_p==damping) && (Vl_p==Vl) && (dirichlet_p==dirichlet)){
			printf "%5d\n",-c;
		} else {
			printf "%5d %12.4e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d %10.2e %s\n", 
	        			c,  tau0,   mu0,   sig,     a,     b,     L,    Vo, damping, Vl, dirichlet;
		}
		c++;
		tau0_p=tau0;mu0_p=mu0;sig_p=sig;
		a_p=a;b_p=b;L_p=L;
		Vo_p=Vo;damping_p=damping;
		Vl_p=Vl;dirichlet_p=dirichlet;
	}
}')
# number of observation patches
1
# n fault i2 rate
  1     1 `echo "" | awk -v n2="$N2" '{print n2/2+1,1}'`
# number of events
0
EOF

ls $WDIR/fault-01-log10v.grd $WDIR/fault-01-slip.grd $WDIR/fault-01-tau.grd $WDIR/patch-01-00513.dat $WDIR/state.ode $WDIR/time.dat

gnuplot <<EOF
set term dumb
set logscale y
set ylabel 'Velocity (m/s)'
set xlabel 'Time (year)'
plot '$WDIR/time.dat' using (\$1/3.15e7):3 with line title ''
EOF

echo "# test successful" | tee $WDIR/report.xml

