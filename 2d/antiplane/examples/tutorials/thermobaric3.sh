#!/bin/bash -e

# simulation for a single fault with two healing mechanisms
# and two flow mechanisms (brittle and ductile.)

export OMP_NUM_THREADS=2

WDIR=thermobaric3
IN="$WDIR/in.param"

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

N2=1024
DX=20

cat <<EOF > $IN
# output directory
$WDIR
# rigidity, radiation damping, universal gas constant
30e3 5 8.31446261815324
# time interval
1e10
# number of faults
1
# grid dimension (N2)
$N2
# sampling (dx2)
$DX
# - - - - - - - - - - - - - - - - - - - - - - -
# R O C K   R H E O L O G Y
# - - - - - - - - - - - - - - - - - - - - - - -
# rock rheology
# number of rocks
1
# index name
1 test3
# mu0   d0 sigma0 alpha beta
 0.60 1e-6    100  0.05  0.3
# number of flow laws
3
# n   Vo  c0  n     Q     To zeta
  1 1e-6   0 70  10e3 293.15    0
  2 0e-6   0 30 110e3 450.00    0
  3 1e-6 100 20 110e3 850.00    0
# number of healing terms
2
# n f0     p   q    H     To
  1  1 10.65  10 40e3 273.15
  2  1 3.053   2 90e3 443.15
# reciprocal of characteristic strain for weakening (lambda)
10
# - - - - - - - - - - - - - - - - - - - - - - -
# L I T H O L O G Y
# - - - - - - - - - - - - - - - - - - - - - - -
#   n  rock
$(echo "" | awk -v n2="$N2" -v dx="$DX" '
	BEGIN{
	c=1;
	type_p=-1;
	}{
	for (i2=0;i2<n2;i2++){
		x2=i2*dx; 
		type=1;
		if (type_p == type){
			printf "%5d\n",-c;
		} else {
			printf "%5d %5d\n",c,type;
		}
		c++;
		type_p=type;
	}
}')
# - - - - - - - - - - - - - - - - - - - - - - -
# T H E R M A L   P R O P E R T I E S
# - - - - - - - - - - - - - - - - - - - - - - -
#   n           D           W           w        rhoc          Tb
$(echo "" | awk -v n2="$N2" -v dx="$DX" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	BEGIN{
	c=1;
	D_p=-1;
	W_p =-1;
	w_p=-1;
	rhoc_p=-1;
	Tb_p=-1;
	}{
	for (i2=0;i2<n2;i2++){
		x2=i2*dx; 
		# thermal diffusivity (m^2/s)
		D=1e-6;
		# thickness of damage zone (m)
		W=2;
		# thickness of shear zone (m)
		w=1e-0;
		# density * heat capacity
		rhoc=2.6;
		# bath temperature
		Tb=273.15+x2*20e-3;
		if ((D_p == D) && (W_p==W) && (w_p==w) && (rhoc_p==rhoc) && (Tb_p==Tb)){
			printf "%5d\n",-c;
		} else {
			printf "%5d %12.6e %12.6e %12.6e %12.6e %12.6e\n", 
	       			  c,     D,     W,     w,  rhoc,    Tb;
		}
		c++;
		D_p=D;
		W_p=W;
		w_p=w;
		rhoc_p=rhoc;
		Tb_p=Tb;
	}
}')
# - - - - - - - - - - - - - - - - - - - - - - -
# F A U L T   P R O P E R T I E S
# - - - - - - - - - - - - - - - - - - - - - - -
#   n      tau0      sig      w       Vl    Dirichlet
$(echo "" | awk -v n2="$N2" -v dx="$DX" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	BEGIN{
	c=1;
	tau0_p=-1;
	sig_p =-1;
	w_p=-1;
	Vl_p=-1;
	dirichlet_p="T";
	}{
	for (i2=0;i2<n2;i2++){
		x2=(i2-n2/2)*dx; 
		tau0=-1;
		sig=1e2; 
		w=1e-2;
		Vl=1e-9;
		if (50>=i2 || (n2-50)<=i2){
			# apply Dirichlet boundary condition (constant velocity Vl)
			dirichlet="T"
		} else {
			# resolve rate and state dependence of friction
			dirichlet="F";
		}
		if (1==boxcar((x2-0.0e3)/2e3)){
		        Vo=1e-6;
			tau0=60.0
		}
		if ((tau0_p == tau0) && (sig_p==sig) && (w_p==w) && (Vl_p==Vl) && (dirichlet_p==dirichlet)){
			printf "%5d\n",-c;
		} else {
			printf "%5d %12.6e %12.6e %12.6e %12.6e %s\n", 
	        		  c,  tau0,   sig,     w,    Vl,dirichlet;
		}
		c++;
		tau0_p=tau0;
		sig_p=sig;
		w_p=w;
		Vl_p=Vl;
		dirichlet_p=dirichlet;
	}
}')
# number of observation patches
1
# n fault i2 rate
  1     1 $(echo "" | awk -v n2=$N2 '{print n2/2+1,1}')
# number of events
0
EOF

OMP_NUM_THREADS=1 motorcycle-ap-thermobaric-serial \
	--verbose 1 \
	--epsilon 1e-6 \
	--export-state \
	--export-netcdf \
	--maximum-step 3.15e7 \
	--maximum-iterations 100000 \
	--evolution-law 1 $* "$IN"

