#!/bin/bash -e

# step-over (decoupled)
# fault 1: Ru=17.33, Rb=0.285
#
# gnuplot
# plot 'output2/time.dat' u ($1/3.15e7):(log10($3)) w l, '' u ($1/3.15e7):(log10($4)) w l, 'output2/patch-01-01025.dat' u ($1/3.15e7):6 w l, 'output2/patch-02-01025.dat' u ($1/3.15e7):6 w l

selfdir=$(dirname $0)

WDIR=$selfdir/output2a

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

N2=2048
DX=10

OMP_NUM_THREADS=2 motorcycle-ps-ratestate-serial \
	--verbose 1 --epsilon 1e-6 \
	--export-state --friction-law 1 \
	--export-netcdf --export-netcdf-rate 20 --export-netcdf-step 1 \
	--maximum-step 3.15e7 --maximum-iterations 1000000 <<EOF
# output directory
$WDIR
# Lame parameters (lambda, mu)
30e3 30e3
# time interval
1e10
# number of faults
1
# grid dimension (N2)
$N2
# sampling (dx2)
$DX
#   n  tau0   mu0   sig   a   b   L   Vo   G/(2Vs)   Vl Dirichlet
`echo "" | awk -v n2=$N2 -v dx=$DX '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	BEGIN{
		c=1;
		tau0_p=-1;mu0_p =-1;sig_p=-1;
		a_p=-1;b_p=-1;L_p=-1;
		Vo_p=-1;damping_p=-1;
		Vl_p=-1;dirichlet_p="T";
	}{
	for (i2=0;i2<n2;i2++){
		x2=(i2-n2/2)*dx; 
		tau0=-1;
		L=5e-3;
		a=1e-2; 
		b=(-5e3<=x2 && x2<1.5e3)?a+4.0e-3:a-4e-3;
		mu0=0.6; 
		sig=1e2; 
		Vo=1e-6; 
		damping=5;
		if (x2<2.5e3){
			Vl=1e-9;
			dirichlet="F";
		} else {
			Vl=1e-12;
			dirichlet="T";
		}
		if ((i2<10) || (i2>=(n2-10))){
			dirichlet="T";
		}
		if (1==boxcar((x2-0.5e3)/1e3)){
			tau0=(mu0+(a-b)*log(1e-10/Vo))*sig;
		}
		if ((tau0_p == tau0) && (mu0_p==mu0) && (sig_p==sig) && (a_p==a) && (b_p==b) && (L_p==L) && 
		    (Vo_p==Vo) && (damping_p==damping) && (Vl_p==Vl) && (dirichlet_p==dirichlet)){
			printf "%5d\n",-c;
		} else {
			printf "%5d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d %10.2e %s\n", 
	        			c,  tau0,   mu0,   sig,     a,     b,     L,    Vo, damping, Vl, dirichlet;
		}
		c++;
		tau0_p=tau0;mu0_p=mu0;sig_p=sig;
		a_p=a;b_p=b;L_p=L;
		Vo_p=Vo;damping_p=damping;
		Vl_p=Vl;dirichlet_p=dirichlet;
	}
}'`
# number of observation patches
1
# n fault i2 rate
  1     1 `echo "" | awk -v n2=$N2 '{print n2/2+1,1}'`
# number of events
0
EOF

