!-----------------------------------------------------------------------
! Copyright 2020-2023 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.h90"

MODULE types_ps

  IMPLICIT NONE

  TYPE PATCH_ELEMENT_STRUCT
     SEQUENCE
     REAL*8 :: Vl
     REAL*8 :: tau0,mu0,sig,a,b,L,Vo,damping
     REAL*8 :: c,d
     LOGICAL :: dirichlet
  END TYPE PATCH_ELEMENT_STRUCT

  TYPE EVENT_STRUCT
     REAL*8 :: time
     INTEGER*4 :: i
  END TYPE EVENT_STRUCT
  
  TYPE FAULT_STRUCT
     ! offset relative to reference fault
     REAL*8 :: x1,x2,x3
     ! slip components
     REAL*8, DIMENSION(:), ALLOCATABLE :: s2
     ! stress components
     REAL*8, DIMENSION(:), ALLOCATABLE :: s23,s33
  END TYPE FAULT_STRUCT

  TYPE OBSERVATION_STATE_STRUCT
     ! fault index
     INTEGER :: fault
     ! coordinates
     INTEGER :: i2
     ! sampling rate
     INTEGER :: rate
     ! file number
     INTEGER :: id
  END TYPE OBSERVATION_STATE_STRUCT
  
#ifdef NETCDF
  TYPE PROFILE_STRUCT
     SEQUENCE
     INTEGER :: fault,index,direction,rate
     ! netcdf file
     INTEGER :: ncid,y_varid,z_varid,ncCount
     ! filename
     CHARACTER*256 :: filename
  END TYPE PROFILE_STRUCT
#endif

  ! transfer function
  TYPE GREENS_TYPE
     COMPLEX(KIND=8), DIMENSION(:), ALLOCATABLE :: g23
     COMPLEX(KIND=8), DIMENSION(:), ALLOCATABLE :: g33
  END TYPE GREENS_TYPE

  TYPE :: SIMULATION_STRUCT

     ! elastic moduli
     REAL*8 :: lambda,mu,nu,alpha

     ! simulation time
     REAL*8 :: interval

     ! fault dimension
     INTEGER :: N2

     ! sampling size
     REAL*8 :: dx2

     ! number of fault patches
     INTEGER :: nPatch

     ! patches
     TYPE(PATCH_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: patch

     ! number of parallel faults
     INTEGER :: nFault

     ! Greens function
     TYPE(GREENS_TYPE), DIMENSION(:,:), ALLOCATABLE :: greens

     ! fault structures
     TYPE(FAULT_STRUCT), DIMENSION(:), ALLOCATABLE :: fault

     ! output directory
     CHARACTER(256) :: wdir

     ! filenames
     CHARACTER(256) :: timeFilename

     ! number of observation states
     INTEGER :: nObservationState

     ! observation state (patches and volumes)
     TYPE(OBSERVATION_STATE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationState

#ifdef NETCDF
     ! observation profiles
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: profile
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: profileStress,profileNormalStress
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: profileSlip
#endif

     ! number of observation points
     INTEGER :: nObservationPoint

     ! number of perturbation events
     INTEGER :: ne

     ! perturbation events
     TYPE(EVENT_STRUCT), DIMENSION(:), ALLOCATABLE :: event

     ! other options
     LOGICAL :: isdryrun=.FALSE.
     LOGICAL :: isExportNetcdf=.FALSE.
     LOGICAL :: isExportStress=.FALSE.
     LOGICAL :: isImportState=.FALSE.
     LOGICAL :: isExportState=.FALSE.
     LOGICAL :: ishelp=.FALSE.
     LOGICAL :: isversion=.FALSE.

  END TYPE SIMULATION_STRUCT

END MODULE types_ps

