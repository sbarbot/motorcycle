!-----------------------------------------------------------------------
! Copyright 2020-2023 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

MODULE greens_ps

  USE types_ps

  IMPLICIT NONE

  PUBLIC

CONTAINS

  !----------------------------------------------------------------------
  !> subroutine initGreens
  !! computes the traction on a receiver fault due to slip on a source
  !! fault.
  !!
  !! INPUT:
  !! rcv         - receiver fault
  !! src         - source fault
  !! mu          - rigidity
  !! alpha       - elastic parameter alpha=1/2/(1-nu)=(l+mu)/(l+2*mu)
  !----------------------------------------------------------------------
  SUBROUTINE initGreens(greens,rcv,src,N2,dx2,mu,alpha)
    TYPE(GREENS_TYPE), INTENT(INOUT) :: greens
    TYPE(FAULT_STRUCT), INTENT(IN) :: rcv
    TYPE(FAULT_STRUCT), INTENT(IN) :: src
    INTEGER, INTENT(IN) :: N2
    REAL*8, INTENT(IN) :: dx2
    REAL*8, INTENT(IN) :: mu,alpha

    ! pi
    REAL*8, PARAMETER :: PI = 3.1415926535897931159979634685441851615905_8

    ! I=SQRT(-1)
    COMPLEX(KIND=8), PARAMETER :: ci = CMPLX(0._8,1._8,8)

    ! counters
    INTEGER :: i2

    ! wavenumbers
    REAL*8 :: w2

    ! traction components
    COMPLEX(KIND=8) :: s23,s33

    ! fault-perpendicular distance
    REAL*8 :: x3

    x3=rcv%x3-src%x3

!$omp do private(w2,s23,s33)
    DO i2=1,N2/2+1

       w2=2*PI*(DBLE(i2-1))/(N2*dx2)

       s23=-mu*alpha*w2*(1d0-w2*ABS(x3))*EXP(-w2*ABS(x3))
       s33= mu*alpha*w2**2*x3*ci*EXP(-w2*ABS(x3))

       ! initialize Greens function
       greens%g23(i2)=s23
       greens%g33(i2)=s33

    END DO
!$omp end do

  END SUBROUTINE initGreens

  !----------------------------------------------------------------------
  !> subroutine computeTraction
  !! computes the traction on a receiver fault due to slip on a source
  !! fault.
  !!
  !! INPUT:
  !! rcv         - receiver fault
  !! src         - source fault
  !! mu          - rigidity
  !! alpha       - elastic parameter alpha=1/2/(1-nu)=(l+mu)/(l+2*mu)
  !! isInit      - initialize rcv traction?
  !----------------------------------------------------------------------
  SUBROUTINE computeTraction(greens,rcv,src,N2,isInit)
    TYPE(GREENS_TYPE), INTENT(IN) :: greens
    TYPE(FAULT_STRUCT), INTENT(IN) :: src
    TYPE(FAULT_STRUCT), INTENT(INOUT) :: rcv
    INTEGER, INTENT(IN) :: N2
    LOGICAL, INTENT(IN) :: isInit

    ! counters
    INTEGER :: i2

    ! slip components
    COMPLEX(KIND=8) :: s2

    ! traction components
    COMPLEX(KIND=8) :: s23,s33

    IF (isInit) THEN
!$omp do private(s2,s23,s33)
       DO i2=1,N2/2+1

          ! slip components
          s2=CMPLX(src%s2(2*i2-1),src%s2(2*i2),8)

          s23=greens%g23(i2)*s2
          s33=greens%g33(i2)*s2

          ! initialize
          rcv%s23(2*i2-1:2*i2)=(/ REAL(s23),AIMAG(s23) /)
          rcv%s33(2*i2-1:2*i2)=(/ REAL(s33),AIMAG(s33) /)

       END DO
!$omp end do
    ELSE
!$omp do private(s2,s23,s33)
       DO i2=1,N2/2+1

          ! slip components
          s2=CMPLX(src%s2(2*i2-1),src%s2(2*i2),8)

          s23=greens%g23(i2)*s2
          s33=greens%g33(i2)*s2

          ! update
          rcv%s23(2*i2-1:2*i2)=rcv%s23(2*i2-1:2*i2)+(/ REAL(s23),AIMAG(s23) /)
          rcv%s33(2*i2-1:2*i2)=rcv%s33(2*i2-1:2*i2)+(/ REAL(s33),AIMAG(s33) /)

       END DO
!$omp end do
    END IF

  END SUBROUTINE computeTraction

END MODULE greens_ps

