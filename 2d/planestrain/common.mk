
OBJRS=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_ps.o getdata.o greens_ps.o ratestate.o )

LIB=$(patsubst %,$(LIBDST)/%, \
      getopt_m.o exportnetcdf.o fft1d.o rk.o )

$(shell mkdir -p $(DST))
$(shell mkdir -p $(LIBDST))

$(DST)/%.o:$(SRC)/%.c
	$(COMPILE.c) $(CFLAGS) -c $^ -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f $(SRC)/macros.h90
	$(COMPILE.f) $(F77FLAGS) -c $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f90
	$(COMPILE.f) $(F90FLAGS) -c $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o -J $(DST)

$(DST)/%.o: $(SRC)/%.cpp
	$(COMPILE.f) $(CXXFLAGS) -c $^ -o $(DST)/$*.o

$(LIBDST)/%.o: $(LIBSRC)/%.f90
	$(COMPILE.f) $^ -o $(LIBDST)/$*.o -J $(LIBDST)

all: lib motorcycle-ps-ratestate-serial

motorcycle-ps-ratestate-serial: $(filter-out $(SRC)/macros.h90,$(OBJRS)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

lib: $(LIB)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:


