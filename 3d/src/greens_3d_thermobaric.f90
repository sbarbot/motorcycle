!-----------------------------------------------------------------------
! Copyright 2025 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

MODULE greens_3d_thermobaric

  USE mpi_f08
  USE types_3d_thermobaric

  IMPLICIT NONE

  PRIVATE

  PUBLIC :: computeTraction

CONTAINS

  !----------------------------------------------------------------------
  !> subroutine computeTraction
  !! computes the traction on a receiver fault due to slip on a source
  !! fault.
  !!
  !! INPUT:
  !! rcv         - receiver fault
  !! src         - source fault
  !! mu          - rigidity
  !! alpha       - elastic parameter alpha=1/2/(1-nu)=(l+mu)/(l+2*mu)
  !! isInit      - initialize rcv traction?
  !----------------------------------------------------------------------
  SUBROUTINE computeTraction(rcv,src,N1L,i1start,N1,N2,dx1,dx2,mu,alpha,isInit)
    TYPE(FAULT_STRUCT), INTENT(IN) :: src
    TYPE(FAULT_STRUCT), INTENT(INOUT) :: rcv
    INTEGER, INTENT(IN) :: N1L,i1start,N1,N2
    REAL*8, INTENT(IN) :: dx1,dx2
    REAL*8, INTENT(IN) :: mu,alpha
    LOGICAL, INTENT(IN) :: isInit

    INTEGER :: rank,csize,ierr

    ! pi
    REAL*8, PARAMETER :: PI = 3.1415926535897931159979634685441851615905_8

    ! I=SQRT(-1)
    COMPLEX(KIND=8), PARAMETER :: ci = CMPLX(0._8,1._8,8)

    ! counters
    INTEGER :: i1,i2

    ! radial wavenumber
    REAL*8 :: w

    ! wavenumbers
    REAL*8 :: w1,w2

    ! slip components
    COMPLEX(KIND=8) :: s1,s2

    ! traction components
    COMPLEX(KIND=8) :: s13,s23,s33

    ! fault-perpendicular distance
    REAL*8 :: x3

    ! decay coefficient
    REAL*8 :: decay

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    x3=rcv%x3-src%x3

    IF (isInit) THEN
       ! initialize
       DO i2=1,N2
          DO i1=1,N1L/2

             w1=2*PI*(DBLE(i1+i1start/2-1))/(N1*dx1)

             IF (i2 < N2/2+1) THEN
                w2= 2*PI*(DBLE(i2-1))/(N2*dx2)
             ELSE
                w2=-2*PI*(DBLE(N2-i2+1))/(N2*dx2)
             END IF

             ! radial wavenumber
             w=SQRT(w1**2+w2**2)
   
             ! slip components
             s1=CMPLX(src%s1c(2*i1-1,i2),src%s1c(2*i1,i2),8)
             s2=CMPLX(src%s2c(2*i1-1,i2),src%s2c(2*i1,i2),8)
   
             decay=EXP(-w*ABS(x3))
   
             s13=-mu/2/w*( &
                     (w2**2+2*alpha*w1**2*(1d0-w*DABS(x3)))*s1 &
                    +((2*alpha-1._8)-2*alpha*w*DABS(x3))*w1*w2*s2 &
                  )*decay
   
             s23=-mu/2/w*( &
                     ((2*alpha-1._8)-2*alpha*w*DABS(x3))*w1*w2*s1 &
                    +(w1**2+2*alpha*w2**2*(1d0-w*DABS(x3)))*s2 &
                  )*decay
   
             s33= mu*alpha*w*x3*(ci*w1*s1+ci*w2*s2)*decay
   
             ! initialize
             rcv%s13c(2*i1-1:2*i1,i2)=(/ REAL(s13),AIMAG(s13) /)
             rcv%s23c(2*i1-1:2*i1,i2)=(/ REAL(s23),AIMAG(s23) /)
             rcv%s33c(2*i1-1:2*i1,i2)=(/ REAL(s33),AIMAG(s33) /)
          END DO
       END DO

    ELSE

       ! update
       DO i2=1,N2
          DO i1=1,N1L/2
   
             w1=2*PI*(DBLE(i1+i1start/2-1))/(N1*dx1)
   
             IF (i2 < N2/2+1) THEN
                w2= 2*PI*(DBLE(i2-1))/(N2*dx2)
             ELSE
                w2=-2*PI*(DBLE(N2-i2+1))/(N2*dx2)
             END IF
   
             ! radial wavenumber
             w=SQRT(w1**2+w2**2)
   
             ! slip components
             s1=CMPLX(src%s1c(2*i1-1,i2),src%s1c(2*i1,i2),8)
             s2=CMPLX(src%s2c(2*i1-1,i2),src%s2c(2*i1,i2),8)
   
             decay=EXP(-w*ABS(x3))
   
             s13=-mu/2/w*( &
                     (w2**2+2*alpha*w1**2*(1d0-w*DABS(x3)))*s1 &
                    +((2*alpha-1._8)-2*alpha*w*DABS(x3))*w1*w2*s2 &
                  )*decay
   
             s23=-mu/2/w*( &
                     ((2*alpha-1._8)-2*alpha*w*DABS(x3))*w1*w2*s1 &
                    +(w1**2+2*alpha*w2**2*(1d0-w*DABS(x3)))*s2 &
                  )*decay
   
             s33= mu*alpha*w*x3*(ci*w1*s1+ci*w2*s2)*decay
   
             ! update
             rcv%s13c(2*i1-1:2*i1,i2)=rcv%s13c(2*i1-1:2*i1,i2)+(/ REAL(s13),AIMAG(s13) /)
             rcv%s23c(2*i1-1:2*i1,i2)=rcv%s23c(2*i1-1:2*i1,i2)+(/ REAL(s23),AIMAG(s23) /)
             rcv%s33c(2*i1-1:2*i1,i2)=rcv%s33c(2*i1-1:2*i1,i2)+(/ REAL(s33),AIMAG(s33) /)
          END DO
       END DO
    END IF

    IF (0 .EQ. i1start) THEN
       ! zero mean stress
       rcv%s13c(1:2,1)=0._8
       rcv%s23c(1:2,1)=0._8
       rcv%s33c(1:2,1)=0._8
    END IF

  END SUBROUTINE computeTraction

END MODULE greens_3d_thermobaric

