!> Program Motorcycle (cycles de terremotos) simulates evolution
!! of slip on multiple parallel faults using the spectral boundary
!! integral method with the radiation damping approximation.
!! Approximates thermal effects with shear heating and membrane
!! diffusion.
!!
!! \mainpage
!!
!! The stress interactions are evaluated in closed-form in the
!! Fourier domain, following the analytical solution
!!
!!    s13h=-mu/2./w (
!!           (w2^2+2*a*w1^2-2*a*w1^2*w*x3)*s1h
!!          +((2*a-1)-2*a*w*x3)*w1*w2*s2h )*exp(-w*abs(x3))
!!
!!    s23h=-mu/2/w*( 
!!           ((2*a-1)-2*a*w*x3)*w1*w2*s1h
!!          +(w1^2+2*a*w2^2-2*a*w2^2.*w*x3)*s2h)*exp(-w*abs(x3))
!!
!!    s33h=a*w*x3*(i*w1*s1h+i*w2*s2h)*exp(-w*abs(x3))
!!
!! where w1=2 pi k1, w2=2 pi k2, and w = sqrt(w1^2+w2^2), 
!! s1h and s2h are the Fourier transforms of the components of
!! fault slip (strike-slip and dip-slip), and x3 is the fault-
!! perpendicular distance.
!!
!! The time evolution is evaluated numerically using the 4/5th order
!! Runge-Kutta method with adaptive time steps. For a single-threaded
!! calculation, the state vector is as follows:
!!
!!    / F1 P1 1       \   +---------------------+ +-----------------+
!!    | .             |   |                     | |                 |
!!    | F1 P1 dPatch  |   |                     | |                 |
!!    | .             |   |                     | |                 |
!!    | .             |   |   nPatch * dPatch   | |                 |
!!    | .             |   |                     | |                 |
!!    | F1 Pn 1       |   |                     | |                 |
!!    | .             |   |                     | |     nFault      |
!!    | F1 Pn dPatch  |   +---------------------+ |        *        |
!!    | .             |                           |     nPatch      |
!!    | FN P1 1       |   +---------------------+ |        *        |
!!    | .             |   |                     | |     dPatch      |
!!    | FN P1 dPatch  |   |                     | |                 |
!!    | .             |   |                     | |                 |
!!    | .             |   |   nPatch * dPatch   | |                 |
!!    | .             |   |                     | |                 |
!!    | FN Pn 1       |   |                     | |                 |
!!    | .             |   |                     | |                 |
!!    \ FN Pn dPatch  /   +---------------------+ +-----------------+
!!
!! where nFault is the number of faults, nPatch is the number of patches
!! per fault, and dPatch is the degrees of freedom for patch. For each
!! patch, we have the following items in the state vector
!!
!!   /  s1   \  1
!!   |  s2   |  .
!!   |  t1   |  .
!!   |  t2   |  .
!!   |  t3   |  .
!!   ! theta*|  .
!!   |  v*   |  .
!!   \  T    /  dPatch
!!
!! where t1, t2, and t3 are the local traction in the strike, dip, and
!! normal directions, s1 and s2 are the total slip in the strike and dip 
!! directions, v*=log10(v) is the logarithm of the norm of the velocity,
!! and theta*=log10(theta) is the logarithm of the state variable in the 
!! rate and state friction framework, and T is the evolving temperature
!! on the fault patch.
!!
!! The data layout is different for multi-threaded calculations, with 
!! some information for all faults available in each thread.
!!
!! References:<br>
!!
!!   Barbot S., Modulation of fault strength during the seismic cycle
!!   by grain-size evolution around contact junctions, Tectonophysics,
!!   765, 129-145, doi:j.tecto.2019.05.004, 2019.
!!
!!   Barbot S.,A spectral boundary-integral method for quasi-dynamic 
!!   ruptures of multiple parallel faults, Bulletin of the Seismological
!!   Society of America, doi: 10.1785/0120210004, 2021.
!!
!! \author Sylvain Barbot, University of Southern California (2020-2025)
!----------------------------------------------------------------------
PROGRAM thermobaric3d

#define BATH 1
#include "macros.h90"

#ifdef NETCDF
  USE exportnetcdf
#endif
  USE fft2d_mpi_thermobaric
  USE greens_3d_thermobaric
  USE rk_mpi
  USE mpi_f08
  USE types_3d_thermobaric

  IMPLICIT NONE

  REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8

  ! MPI rank and size
  INTEGER :: rank,csize

  ! error flag
  INTEGER :: ierr

  CHARACTER(512) :: filename
  CHARACTER(512) :: format1,format2
  CHARACTER(512) :: importStateDir

  ! maximum velocity
  REAL*8, DIMENSION(:), ALLOCATABLE :: vMax,vMaxAll

#ifdef BATH
  ! maximum temperature
  REAL*8, DIMENSION(:), ALLOCATABLE :: tMax,tMaxAll
#endif

  ! moment rate
  REAL*8, DIMENSION(:), ALLOCATABLE :: momentRate,momentRateAll

  ! scaling factor
  REAL*8, PARAMETER :: lg10=LOG(1.d1)

  ! state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: y,yAll
  ! rate of change of state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: dydt,yscal
  ! temporary variables
  REAL*8, DIMENSION(:), ALLOCATABLE :: ytmp,ytmp1,ytmp2,ytmp3

  ! time
  REAL*8 :: time,t0
  ! time step
  REAL*8 :: dt_try,dt_next,dt_done

  ! counters
  INTEGER :: i,j

  ! type of evolution law (default)
  INTEGER :: evolutionLaw=1

  ! maximum number of time steps (default)
  INTEGER :: maximumIterations=1000000

  ! verbosity
  INTEGER :: verbose=2

  ! layout for parallelism
  TYPE(LAYOUT_STRUCT) :: layout

  ! model parameters
  TYPE(SIMULATION_STRUCT) :: in

!$  INTEGER, EXTERNAL :: omp_get_max_threads,omp_get_num_procs

  ! initialization
  CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

  ! start time
  time=0.d0

  ! initial tentative time step
  dt_next=1.0d-3

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  IF (3 .LE. verbose) CALL printParallelism()

  IF (in%isdryrun) THEN
     IF (0.EQ.rank) PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isdryrun .OR. in%isversion .OR. in%ishelp) THEN
     CALL MPI_FINALIZE(ierr)
     STOP
  END IF

  ALLOCATE(vMax(in%nFault),vMaxAll(in%nFault))
#ifdef BATH
  ALLOCATE(tMax(in%nFault),tMaxAll(in%nFault))
#endif
  ALLOCATE(momentRate(in%nFault),momentRateAll(in%nFault))

  ! number of fault patches in current thread
  in%nPatch=in%nFault*in%N1*layout%N2L(1+rank)

  ! state vector
  ALLOCATE(y(in%nPatch*STATE_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vector"

  IF (0 .EQ. rank) THEN
     ALLOCATE(yAll(in%N1*in%N2*STATE_VECTOR_DGF),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the state vector"
  END IF

  ! rate of state vector
  ALLOCATE(dydt (in%nPatch*STATE_VECTOR_DGF), &
           yscal(in%nPatch*STATE_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vectors"

  ALLOCATE(ytmp (in%nPatch*STATE_VECTOR_DGF), &
           ytmp1(in%nPatch*STATE_VECTOR_DGF), &
           ytmp2(in%nPatch*STATE_VECTOR_DGF), &
           ytmp3(in%nPatch*STATE_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the rungeKutta work space"

  ! allocate buffer from rk module
  ALLOCATE(buffer(in%nPatch*STATE_VECTOR_DGF,5),SOURCE=0.0d0,STAT=ierr)
  IF (ierr>0) STOP "could not allocate the buffer work space"

  ! report
  IF (0 .EQ. rank) THEN
     OPEN (UNIT=FPTIME,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
        STOP 1
     END IF
  END IF

  ! initialize the y vector
  IF (0.EQ.rank) PRINT '("# initialize state vector.")'
  CALL initStateVector(in%nPatch*STATE_VECTOR_DGF,y,in)

  IF (0.EQ.rank) PRINT 2000

  ! allocate work space for Fourier transforms
  DO j=1,in%nFault
     ! rows
     ALLOCATE(in%fault(j)%s1r(in%N1+2,layout%N2L(1+rank)), &
              in%fault(j)%s2r(in%N1+2,layout%N2L(1+rank)),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the work space"

     ! columns
     ALLOCATE(in%fault(j)%s1c(layout%N1L(1+rank),in%N2), &
              in%fault(j)%s2c(layout%N1L(1+rank),in%N2),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the work space"

     ! columns
     ALLOCATE(in%fault(j)%s13c(layout%N1L(1+rank),in%N2), &
              in%fault(j)%s23c(layout%N1L(1+rank),in%N2), &
              in%fault(j)%s33c(layout%N1L(1+rank),in%N2),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the work space"

     ! rows
     ALLOCATE(in%fault(j)%s13r(in%N1+2,layout%N2L(1+rank)), &
              in%fault(j)%s23r(in%N1+2,layout%N2L(1+rank)), &
              in%fault(j)%s33r(in%N1+2,layout%N2L(1+rank)),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the work space"
  END DO

  ! builds wisdom for DFT
  IF (0 .EQ. rank) PRINT '("# build FFTW wisdom.")'
!$  CALL dfftw_init_threads(ierr)
!$  CALL dfftw_plan_with_nthreads(omp_get_max_threads())
  CALL fftInit(in%N1,layout%N2L(1+rank),in%fault(1)%s1r,layout%N1L(1+rank),in%N2,in%fault(1)%s1c)

  ! gather maximum velocity
  CALL MPI_REDUCE(vMax,vMaxAll,in%nFault,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

#ifdef BATH
  ! gather maximum velocity
  CALL MPI_REDUCE(tMax,tMaxAll,in%nFault,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)
#endif

  ! gather moment rate
  CALL MPI_REDUCE(momentRate,momentRateAll,in%nFault,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

#ifdef NETCDF
  ! initialize netcdf output
  IF (in%isExportNetcdf) THEN
     DO i=1,in%nObservationProfile
        IF (rank .EQ. in%observationProfileVelocity(i)%rank) THEN
           IF (1 .EQ. in%observationProfileVelocity(i)%direction) THEN
              WRITE (in%observationProfileVelocity(i)%filename, &
                      '(a,"/fault-",I2.2,"-i2-",I4.4,"-log10v.grd")') &
                      TRIM(in%wdir),in%observationProfileVelocity(i)%fault, &
                      in%observationProfileVelocity(i)%index
           ELSE
              WRITE (in%observationProfileVelocity(i)%filename, &
                      '(a,"/fault-",I2.2,"-i1-",I4.4,"-log10v.grd")') &
                      TRIM(in%wdir),in%observationProfileVelocity(i)%fault, &
                      in%observationProfileVelocity(i)%index
           END IF
           CALL initProfile(in%observationProfileVelocity(i))

#ifdef BATH
           IF (1 .EQ. in%observationProfileTemperature(i)%direction) THEN
              WRITE (in%observationProfileTemperature(i)%filename, &
                      '(a,"/fault-",I2.2,"-i2-",I4.4,"-temp.grd")') &
                      TRIM(in%wdir),in%observationProfileTemperature(i)%fault, &
                      in%observationProfileTemperature(i)%index
           ELSE
              WRITE (in%observationProfileTemperature(i)%filename, &
                      '(a,"/fault-",I2.2,"-i1-",I4.4,"-temp.grd")') &
                      TRIM(in%wdir),in%observationProfileTemperature(i)%fault, &
                      in%observationProfileTemperature(i)%index
           END IF
           CALL initProfile(in%observationProfileTemperature(i))
#endif
        END IF
     END DO
  END IF
#endif

  ! initialize output
  IF (0 .EQ. rank) THEN
     WRITE (format1,'("(I9.9,ES20.13E2,ES19.12E2,",I3,"ES11.4E2)")') 2*in%nFault
     WRITE (format2,'("(ES20.14E2,ES19.12E2,",I3,"ES11.4E2,",I3,"ES20.12E2)")') 3*in%nFault,3*in%nFault
     WRITE(STDOUT,'("# export to ",a)') TRIM(in%wdir)
     PRINT 2000
     WRITE(STDOUT,'("#       n                time                 dt       vMax       tMax")')
     WRITE(STDOUT,format1) 0,time,dt_next,vMaxAll,tMaxAll
     WRITE(FPTIME,'("#               time                 dt               vMax         Moment-rate     tMax")')
  END IF

  ! initialize observation patch
  DO j=1,in%nObservationState
     IF (rank .EQ. in%observationState(j)%rank) THEN
        in%observationState(j)%id=100+j
        WRITE (filename,'(a,"/patch-",I2.2,"-",I5.5,"-",I5.5,".dat")') TRIM(in%wdir), &
                in%observationState(j)%fault, &
                in%observationState(j)%i1, &
                in%observationState(j)%i2
        OPEN (UNIT=in%observationState(j)%id, &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
     END IF
  END DO

  ! main loop
  DO i=1,maximumIterations

     CALL odefun(in%nPatch*STATE_VECTOR_DGF,time,y,dydt)
     CALL export()

#ifdef NETCDF
     IF (in%isExportNetcdf) THEN
        DO j=1,in%nFault
           IF (0 .EQ. MOD(i-1,in%sourceExportRate)) THEN
              CALL exportSourceNetcdf(j)
           END IF
        END DO

        DO j=1,in%nObservationProfile
           IF (1 .EQ. in%observationProfileVelocity(j)%direction) THEN
              ! horizontal profile
              IF (0 .EQ. MOD(i,in%observationProfileVelocity(j)%rate)) THEN
                 IF (rank .EQ. in%observationProfileVelocity(j)%rank) THEN
                    CALL exportHorizontalProfileVelocity(in%observationProfileVelocity(j))
#ifdef BATH
                    CALL exportHorizontalProfileTemperature(in%observationProfileTemperature(j))
#endif
                 END IF
              END IF
           ELSE
              ! vertical profile
              IF (0 .EQ. MOD(i,in%observationProfileVelocity(j)%rate)) THEN
                    CALL exportVerticalProfileVelocity(in%observationProfileVelocity(j))
#ifdef BATH
                    CALL exportVerticalProfileTemperature(in%observationProfileTemperature(j))
#endif
              END IF
           END IF
        END DO
     END IF
#endif

     dt_try=dt_next
     yscal(:)=ABS(y(:))+ABS(dt_try*dydt(:))+TINY

     t0=time
     CALL rungeKutta(in%nPatch*STATE_VECTOR_DGF,t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep45)

     time=time+dt_done

     IF (in%isExportState) THEN
        IF (0 .EQ. MOD(i,5000)) THEN
           IF (0.EQ. rank) PRINT '("# exporting state")'
           CALL exportState()
        END IF
     END IF

     ! end calculation
     IF (in%interval .LE. time) THEN
        EXIT
     END IF
   
  END DO

  ! save state
  IF (in%isExportState) THEN
     CALL exportState()
  END IF

  IF (0.EQ.rank) PRINT '(I9.9," time steps.")', i

  CLOSE(FPTIME)

  ! close observation state files
  DO j=1,in%nObservationState
     IF (rank .EQ. in%observationState(j)%rank) THEN
        CLOSE(in%observationState(j)%id)
     END IF
  END DO

  DEALLOCATE(vMax,vMaxAll)
#ifdef BATH
  DEALLOCATE(tMax,tMaxAll)
#endif
  DEALLOCATE(momentRate,momentRateAll)
  DEALLOCATE(y,dydt,yscal)
  DEALLOCATE(ytmp,ytmp1,ytmp2,ytmp3)
  DEALLOCATE(buffer)
  IF (0 .EQ. rank) DEALLOCATE(yAll)

  DO j=1,in%nFault
     DEALLOCATE(in%fault(j)%s1r)
     DEALLOCATE(in%fault(j)%s2r)
     DEALLOCATE(in%fault(j)%s1c)
     DEALLOCATE(in%fault(j)%s2c)
     DEALLOCATE(in%fault(j)%s13r)
     DEALLOCATE(in%fault(j)%s23r)
     DEALLOCATE(in%fault(j)%s33r)
     DEALLOCATE(in%fault(j)%s13c)
     DEALLOCATE(in%fault(j)%s23c)
     DEALLOCATE(in%fault(j)%s33c)
  END DO
  DEALLOCATE(in%fault)

#ifdef NETCDF
  IF (in%isExportNetcdf) THEN
     DO j=1,in%nObservationProfile
        IF (rank .EQ. in%observationProfileVelocity(j)%rank) THEN
           CALL closeNetcdfUnlimited(in%observationProfileVelocity(j)%ncid, &
                                     in%observationProfileVelocity(j)%y_varid, &
                                     in%observationProfileVelocity(j)%z_varid, &
                                     in%observationProfileVelocity(j)%ncCount)
#ifdef BATH
           CALL closeNetcdfUnlimited(in%observationProfileTemperature(j)%ncid, &
                                     in%observationProfileTemperature(j)%y_varid, &
                                     in%observationProfileTemperature(j)%z_varid, &
                                     in%observationProfileTemperature(j)%ncCount)
#endif
        END IF
     END DO
  END IF
  DEALLOCATE(in%observationProfileVelocity)
#ifdef BATH
  DEALLOCATE(in%observationProfileTemperature)
#endif
#endif

  ! free parallelism layout
  DEALLOCATE(layout%N1L)
  DEALLOCATE(layout%N2L)
  DEALLOCATE(layout%i1start)
  DEALLOCATE(layout%i2start)

  DO j=1,csize
     DO i=1,csize
        CALL MPI_TYPE_FREE(layout%vector(i,j),ierr)
     END DO
  END DO
  DEALLOCATE(layout%vector)
#ifdef NETCDF
  CALL MPI_TYPE_FREE(layout%profileVector,ierr)
#endif

  CALL MPI_FINALIZE(ierr)

!$  CALL dfftw_cleanup_threads()

2000 FORMAT ("# ----------------------------------------------------------------------------------------------")
     
CONTAINS
  
  !-----------------------------------------------------------------------
  !> subroutine exportState
  ! export state vector to disk to allow restart
  !----------------------------------------------------------------------
  SUBROUTINE exportState()
    WRITE (filename,'(a,"/state-",I4.4,".ode")') TRIM(in%wdir),rank
    OPEN(FPSTATE,FILE=filename,FORM="unformatted")
    WRITE(FPSTATE) time
    WRITE(FPSTATE) y
    CLOSE(FPSTATE)
  END SUBROUTINE exportState

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportSourceNetcdf
  ! export source properties (log10v, s13, s23) to netcdf
  !----------------------------------------------------------------------
  SUBROUTINE exportSourceNetcdf(j)
    INTEGER, INTENT(IN) :: j

    REAL*8, DIMENSION(:), ALLOCATABLE :: x_coords
    REAL*8, DIMENSION(:), ALLOCATABLE :: y_coords
    REAL*8, DIMENSION(:,:), ALLOCATABLE :: z_data

    INTEGER :: i1,i2,k,l
    CHARACTER(LEN=512) :: filename
    INTEGER :: offset

    layout%rcounts=layout%N2L*in%N1*STATE_VECTOR_DGF
    layout%displs=layout%i2start*in%N1*STATE_VECTOR_DGF
    offset=(j-1)*layout%N2L(1+rank)*in%N1*STATE_VECTOR_DGF

    CALL MPI_GATHERV(y(offset+1),layout%rcounts(1+rank),MPI_REAL8, &
                     yAll,       layout%rcounts,layout%displs,MPI_REAL8, &
                     0,MPI_COMM_WORLD,ierr)

    IF (0 .EQ. rank) THEN

       ALLOCATE(x_coords(in%N1),y_coords(in%N2),z_data(in%N1,in%N2))

       ! loop over all patch elements
       DO i1=1,in%N1
          x_coords(i1)=REAL((i1-1-in%N1/2),8)*in%dx1
       END DO
       DO i2=1,in%N2
          y_coords(i2)=REAL((i2-1-in%N2/2),8)*in%dx2
       END DO

       ! loop over patch elements
       DO i2=1,in%N2
          DO i1=1,in%N1

             ! patch index
             k=(i2-1)*in%N1+i1

             ! state vector index
             l=(k-1)*STATE_VECTOR_DGF+1

             ! norm of slip rate vector
             z_data(i1,i2)=yAll(l+STATE_VECTOR_VELOCITY)
          END DO
       END DO
       ! netcdf file is compatible with GMT
       WRITE (filename,'(a,"/fault-",I2.2,"-index-",I7.7,"-log10v.grd")') TRIM(in%wdir),j,i
       CALL writeNetcdf(filename,in%N1,x_coords,in%N2,y_coords,z_data(1,1),1)

       IF (in%isExportStress) THEN
          ! loop over patch elements
          DO i2=1,in%N2
             DO i1=1,in%N1
   
                ! patch index
                k=((j-1)*in%N2+(i2-1))*in%N1+i1

                ! state vector index
                l=(k-1)*STATE_VECTOR_DGF+1
   
                !patch=in%patch(k)
   
                ! shear stress
                z_data(i1,i2)=yAll(l+STATE_VECTOR_TRACTION_STRIKE)
             END DO
          END DO
          ! netcdf file is compatible with GMT
          WRITE (filename,'(a,"/fault-",I2.2,"-index-",I7.7,"-s13.grd")') TRIM(in%wdir),j,i
          CALL writeNetcdf(filename,in%N1,x_coords,in%N2,y_coords,z_data,1)
   
          ! loop over patch elements
          DO i2=1,in%N2
             DO i1=1,in%N1
   
                ! patch index
                k=((j-1)*in%N2+(i2-1))*in%N1+i1
                ! state vector index
                l=(k-1)*STATE_VECTOR_DGF+1
      
                !patch=in%patch(k)
   
                ! norm of slip rate vector
                z_data(i1,i2)=yAll(l+STATE_VECTOR_TRACTION_DIP)
             END DO
          END DO
          ! netcdf file is compatible with GMT
          WRITE (filename,'(a,"/fault-",I2.2,"-index-",I7.7,"-s23.grd")') TRIM(in%wdir),j,i
          CALL writeNetcdf(filename,in%N1,x_coords,in%N2,y_coords,z_data,1)
       END IF

#ifdef BATH
       IF (in%isExportTemperature) THEN
          ! loop over patch elements
          DO i2=1,in%N2
             DO i1=1,in%N1
   
                ! patch index
                k=((j-1)*in%N2+(i2-1))*in%N1+i1

                ! state vector index
                l=(k-1)*STATE_VECTOR_DGF+1
   
                !patch=in%patch(k)
   
                ! shear stress
                z_data(i1,i2)=yAll(l+STATE_VECTOR_TEMPERATURE)
             END DO
          END DO
          ! netcdf file is compatible with GMT
          WRITE (filename,'(a,"/fault-",I2.2,"-index-",I7.7,"-temp.grd")') TRIM(in%wdir),j,i
          CALL writeNetcdf(filename,in%N1,x_coords,in%N2,y_coords,z_data,1)
       END IF
#endif

       DEALLOCATE(x_coords,y_coords,z_data)
    END IF

  END SUBROUTINE exportSourceNetcdf

  !-----------------------------------------------------------------------
  !> subroutine initProfile
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE initProfile(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*8, DIMENSION(:), ALLOCATABLE :: x

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ! initialize the number of exports
    profile%ncCount=0

    IF (1 .EQ. profile%direction) THEN
       ! longitudinal profile

       ALLOCATE(x(in%N1),STAT=ierr)
       IF (ierr/=0) STOP "could not allocate netcdf coordinate"

       ! loop over all patch elements
       DO i=1,in%N1
          x(i)=REAL((i-1-in%N1/2)*in%dx1,8)
       END DO

       ! netcdf file is compatible with GMT
       CALL openNetcdfUnlimited( &
                profile%filename, &
                in%N1, &
                x, &
                profile%ncid, &
                profile%y_varid, &
                profile%z_varid)
    ELSE
       ! vertical profile

       ALLOCATE(x(in%N2),STAT=ierr)
       IF (ierr/=0) STOP "could not allocate netcdf coordinate"

       ! loop over all patch elements
       DO i=1,in%N2
          x(i)=REAL((i-1-in%N2/2)*in%dx2,8)
       END DO

       ! netcdf file is compatible with GMT
       CALL openNetcdfUnlimited( &
                profile%filename, &
                in%N2, &
                x, &
                profile%ncid, &
                profile%y_varid, &
                profile%z_varid)
    END IF

    DEALLOCATE(x)

  END SUBROUTINE initProfile

  !-----------------------------------------------------------------------
  !> subroutine exportHorizontalProfileVelocity
  ! export time series of log10(v) along horizontal profiles
  !----------------------------------------------------------------------
  SUBROUTINE exportHorizontalProfileVelocity(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(in%N1) :: z

    INTEGER :: i1,i2,l,offset

    ! update the export count
    profile%ncCount=profile%ncCount+1

    i2=profile%i2l

    offset=(profile%fault-1)*layout%N2L(1+rank)

    DO i1=1,in%N1
       ! state vector index
       l=((offset+i2-1)*in%N1+i1-1)*STATE_VECTOR_DGF+1

       ! norm of slip rate vector
       z(i1)=REAL(y(l+STATE_VECTOR_VELOCITY),4)
    END DO

    CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,in%N1,z)

    ! flush every so often
    IF (0 .EQ. MOD(profile%ncCount,20)) THEN
       CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
    END IF

  END SUBROUTINE exportHorizontalProfileVelocity

  !-----------------------------------------------------------------------
  !> subroutine exportHorizontalProfileTemperature
  ! export time series of temperature along horizontal profiles
  !----------------------------------------------------------------------
  SUBROUTINE exportHorizontalProfileTemperature(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(:), ALLOCATABLE :: z

    INTEGER :: i1,i2,l,offset

    ! update the export count
    profile%ncCount=profile%ncCount+1

    ALLOCATE(z(in%N1),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf z values"

    i2=profile%i2l

    offset=(profile%fault-1)*layout%N2L(1+rank)

    DO i1=1,in%N1
       ! state vector index
       l=((offset+i2-1)*in%N1+i1-1)*STATE_VECTOR_DGF+1

       ! norm of slip rate vector
       z(i1)=REAL(y(l+STATE_VECTOR_TEMPERATURE),4)
    END DO

    CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,in%N1,z)

    ! flush every so often
    IF (0 .EQ. MOD(profile%ncCount,20)) THEN
       CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
    END IF

    DEALLOCATE(z)

  END SUBROUTINE exportHorizontalProfileTemperature

  !-----------------------------------------------------------------------
  !> subroutine exportVerticalProfileVelocity
  ! export time series of log10(v) along vertical profiles
  !----------------------------------------------------------------------
  SUBROUTINE exportVerticalProfileVelocity(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(:), ALLOCATABLE :: z4
    REAL*8, DIMENSION(:), ALLOCATABLE :: z8
    INTEGER :: i

    IF (0 .EQ. rank) THEN
       ALLOCATE(z4(in%N2),z8(in%N2),STAT=ierr)
       IF (ierr/=0) STOP "could not allocate netcdf z values"

       layout%rcounts=layout%N2L
       layout%displs=layout%i2start
    END IF

    ! norm of slip rate vector
    i=((profile%fault-1)*layout%N2L(1+rank)*in%N1+(profile%index-1))*STATE_VECTOR_DGF+1+STATE_VECTOR_VELOCITY
    CALL MPI_GATHERV( &
            y(i),1,layout%profileVector, &
            z8(1),layout%rcounts,layout%displs,MPI_REAL8, &
            0,MPI_COMM_WORLD,ierr)

    IF (0 .EQ. rank) THEN
       ! update the export count
       profile%ncCount=profile%ncCount+1

       z4=REAL(z8,4)
       CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,in%N2,z4)
       DEALLOCATE(z4,z8)

       ! flush every so often
       IF (0 .EQ. MOD(profile%ncCount,20)) THEN
          CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
       END IF

    END IF

  END SUBROUTINE exportVerticalProfileVelocity

  !-----------------------------------------------------------------------
  !> subroutine exportVerticalProfileTemperature
  ! export time series of temperature along vertical profiles
  !----------------------------------------------------------------------
  SUBROUTINE exportVerticalProfileTemperature(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(:), ALLOCATABLE :: z4
    REAL*8, DIMENSION(:), ALLOCATABLE :: z8
    INTEGER :: i

    IF (0 .EQ. rank) THEN
       ! update the export count
       profile%ncCount=profile%ncCount+1

       ALLOCATE(z4(in%N2),z8(in%N2),STAT=ierr)
       IF (ierr/=0) STOP "could not allocate netcdf z values"

       layout%rcounts=layout%N2L
       layout%displs=layout%i2start
    END IF

    ! temperature
    i=((profile%fault-1)*layout%N2L(1+rank)*in%N1+(profile%index-1))*STATE_VECTOR_DGF+1+STATE_VECTOR_TEMPERATURE
    CALL MPI_GATHERV( &
            y(i),1,layout%profileVector, &
            z8(1),layout%rcounts,layout%displs,MPI_REAL8, &
            0,MPI_COMM_WORLD,ierr)

    IF (0 .EQ. rank) THEN
       z4=REAL(z8,4)
       CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,in%N2,z4)
       DEALLOCATE(z4,z8)

       ! flush every so often
       IF (0 .EQ. MOD(profile%ncCount,20)) THEN
          CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
       END IF

    END IF

  END SUBROUTINE exportVerticalProfileTemperature
#endif

  !-----------------------------------------------------------------------
  !> subroutine export
  ! write the state variables of patch elements, and other information.
  !----------------------------------------------------------------------
  SUBROUTINE export()

    ! counters
    INTEGER :: j,k,l

    ! format string
    CHARACTER(1024) :: formatString

    ! gather maximum velocity
    CALL MPI_REDUCE(vMax,vMaxAll,in%nFault,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather maximum temperature
    CALL MPI_REDUCE(tMax,tMaxAll,in%nFault,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather moment-rate
    CALL MPI_REDUCE(momentRate,momentRateAll,in%nFault,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    ! export observation state
    DO j=1,in%nObservationState

       IF (rank .EQ. in%observationState(j)%rank) THEN

          ! check observation state sampling rate
          IF (0 .EQ. MOD(i-1,in%observationState(j)%rate)) THEN
             formatString="(ES19.12E2"
             DO k=1,STATE_VECTOR_DGF
                formatString=TRIM(formatString)//",X,ES20.12E3,X,ES20.12E3"
             END DO
             formatString=TRIM(formatString)//")"
   
             l=(((in%observationState(j)%fault-1)*layout%N2L(1+rank)+(in%observationState(j)%i2l-1))*in%N1 &
                     +in%observationState(j)%i1-1)*STATE_VECTOR_DGF+1

             WRITE (in%observationState(j)%id,TRIM(formatString)) time, &
                       y(l:l+STATE_VECTOR_DGF-1), &
                    dydt(l:l+STATE_VECTOR_DGF-1)
          END IF

       END IF
    END DO

    IF (0 .EQ. rank) THEN
       WRITE(FPTIME,format2) time,dt_done,vMaxAll,momentRateAll,tMaxAll
       IF (0 .EQ. MOD(i,50)) THEN
          WRITE(STDOUT,format1) i,time,dt_done,vMaxAll,tMaxAll
          CALL FLUSH(STDOUT)
          CALL FLUSH(FPTIME)
       END IF
    END IF

  END SUBROUTINE export

  !-----------------------------------------------------------------------
  !> subroutine initStateVector
  ! initialize the state vector
  !
  ! INPUT:
  ! @param n - number of state elements own by current thread
  ! @param y - the state vector (segment owned by currect thread)
  !----------------------------------------------------------------------
  SUBROUTINE initStateVector(n,y,in)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(OUT)    :: y(n)
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    INTEGER :: i1,i2,k,l
    TYPE(PATCH_ELEMENT_STRUCT) :: p
    TYPE(ROCK_STRUCT) :: r
    TYPE(HEALING_STRUCT) :: h1,h2
    TYPE(FLOW_STRUCT) :: f1,f2,f3

    ! velocity perturbation
    REAL*8, PARAMETER :: modifier = 0.99d0

    ! initial stress
    REAL*8 :: tau0

    ! initial state variable (LOG10(d/do))
    REAL*8 :: Sinit

    ! initial velocity
    REAL*8 :: Vinit

    ! initial temperature
    REAL*8 :: Tinit

    ! velocity
    REAL*8 :: V1,V2,V3

    ! maximum velocity
    vMax=0._8

    ! zero out state vector
    y=0._8

    ! maximum temperature
    tMax=0._8

    ! moment-rate
    momentRate=0._8

    ! restart
    IF (in%isImportState) THEN
       WRITE (filename,'(a,"/state-",I4.4,".ode")') TRIM(importStateDir),rank
       IF (0 .EQ. rank) WRITE (STDOUT,'("# load state vector from ",a)') TRIM(filename)
       OPEN(FPSTATE,FILE=filename,FORM="unformatted")
       READ(FPSTATE) time
       READ(FPSTATE) y
       CLOSE(FPSTATE)

       ! loop over faults
       DO j=1,in%nFault
          ! loop over fault elements
          DO i2=1,layout%N2L(1+rank)
             DO i1=1,in%N1

                ! patch index
                k=((j-1)*layout%N2L(1+rank)+(i2-1))*in%N1+i1

                ! state vector index
                l=(k-1)*STATE_VECTOR_DGF+1

                Vinit=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

                ! maximum velocity
                vMax(j)=MAX(Vinit,vMax(j))
   
                ! maximum velocity
                tMax(j)=MAX(y(l+STATE_VECTOR_TEMPERATURE),tMax(j))
   
                ! moment-rate
                momentRate(j)=momentRate(j)+Vinit*in%mu*in%dx1*in%dx2

             END DO
          END DO
       END DO

       RETURN
    END IF

    ! loop over faults
    DO j=1,in%nFault

       ! loop over fault elements
       DO i2=1,layout%N2L(1+rank)
          DO i1=1,in%N1

             ! patch index
             k=((j-1)*layout%N2L(1+rank)+(i2-1))*in%N1+i1

             ! state vector index
             l=(k-1)*STATE_VECTOR_DGF+1

             ! current patch
             p=in%patch(k)

             ! current rock
             r=in%rock(p%rockType)

             ! strike slip
             y(l+STATE_VECTOR_SLIP_STRIKE) = 0._8

             ! dip slip
             y(l+STATE_VECTOR_SLIP_DIP) = 0._8

             ! steady-state temperature
             Tinit=p%Tb+r%mu0*p%sig*p%Vl/p%wRhoC/p%DW2

             ! initial velocity
             Vinit=modifier*p%Vl

             ! initial size of microasperities (LOG10(d/d0)) at Vinit
             SELECT CASE(r%nHealing)
             CASE(1)
                ! log10(dss/do)
                h1=r%healing(1)
                Sinit=1._8/h1%p*(LOG(2*p%h*h1%f0/r%lambda/h1%p/Vinit) &
                                +h1%q*LOG(p%sig/r%sigma0) &
                                -h1%H/in%R*(1._8/Tinit-1._8/h1%To))/lg10
             CASE(2)
                h1=r%healing(1)
                h2=r%healing(2)
                Sinit=LOG(findRoot2( &
                        h1%f0*(r%d0)**h1%p/h1%p*(p%sig/r%sigma0)**h1%q*EXP(-h1%H/in%R*(1._8/Tinit-1._8/h1%To)),-h1%p, &
                        h2%f0*(r%d0)**h2%p/h2%p*(p%sig/r%sigma0)**h2%q*EXP(-h2%H/in%R*(1._8/Tinit-1._8/h2%To)),-h2%p, &
                        r%lambda*Vinit/(2*p%h))/r%d0)/lg10
             CASE DEFAULT
                WRITE (0,'("incorrect healing term ", I2)') r%nHealing
                WRITE_DEBUG_INFO(100)
                STOP 3
             END SELECT

             ! initial traction
             IF (0 .GT. p%tau0) THEN
   
                ! initial velocity
                Vinit=modifier*p%Vl
   
                SELECT CASE(r%nFlow)
                CASE(1)
                   f1=r%flow(1)
                   tau0=(f1%c0+r%mu0*p%sig)*(Vinit/f1%Vo)**(1._8/f1%n) &
                                          *EXP(r%alpha*Sinit*lg10) &
                                          *(p%sig/r%sigma0)**(-r%beta+f1%zeta/f1%n) &
                                          *EXP(-f1%Q/in%R/f1%n*(1._8/Tinit-1._8/f1%To))
                CASE(2)
                   f1=r%flow(1)
                   f2=r%flow(2)
                   tau0=findRoot2( &
                           f1%Vo/(f1%c0+r%mu0*p%sig)**f1%n &
                                       *EXP(-r%alpha*f1%n*Sinit*lg10) &
                                       *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                                       *EXP(-f1%Q/in%R*(1._8/Tinit-1._8/f1%To)), f1%n, &
                           f2%Vo/(f2%c0+r%mu0*p%sig)**f2%n &
                                       *EXP(-r%alpha*f2%n*Sinit*lg10) &
                                       *(p%sig/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                                       *EXP(-f2%Q/in%R*(1._8/Tinit-1._8/f2%To)), f2%n, &
                           Vinit)
                CASE(3)
                   f1=r%flow(1)
                   f2=r%flow(2)
                   f3=r%flow(3)
                   ! no state variable nor normal-stress dependence for f3
                   ! except for the pressure-dependence of activation temperature
                   tau0=findRoot3( &
                           f1%Vo/(f1%c0+r%mu0*p%sig)**f1%n &
                                       *EXP(-r%alpha*f1%n*Sinit*lg10) &
                                       *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                                       *EXP(-f1%Q/in%R*(1._8/Tinit-1._8/f1%To)), f1%n, &
                           f2%Vo/(f2%c0+r%mu0*p%sig)**f2%n &
                                       *EXP(-r%alpha*f2%n*Sinit*lg10) &
                                       *(p%sig/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                                       *EXP(-f2%Q/in%R*(1._8/Tinit-1._8/f2%To)), f2%n, &
                           f3%Vo/(f3%c0              )**f3%n &
                                       *(p%sig/r%sigma0)**(           -f3%zeta) &
                                       *EXP(-f3%Q/in%R*(1._8/Tinit-1._8/f3%To)), f3%n, &
                           Vinit);
                CASE DEFAULT
                   WRITE (0,'("incorrect flow law ", I2)') r%nFlow
                   WRITE_DEBUG_INFO(100)
                   STOP 3
                END SELECT

             ELSE
                tau0 = p%tau0

                ! set initial velocity corresponding to initial stress
                SELECT CASE(r%nFlow)
                CASE(1)
                   V1=f1%Vo*(tau0/(f1%c0+r%mu0*p%sig))**(f1%n) &
                           *DEXP(-r%alpha*f1%n*Sinit*lg10) &
                           *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                           *DEXP(-f1%Q/in%R*(1._8/Tinit-1._8/f1%To))
                   Vinit=V1
                CASE(2)
                   V1=f1%Vo*(tau0/(f1%c0+r%mu0*p%sig))**(f1%n) &
                           *DEXP(-r%alpha*f1%n*Sinit*lg10) &
                           *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                           *DEXP(-f1%Q/in%R*(1._8/Tinit-1._8/f1%To))
                   V2=f2%Vo*(tau0/(f2%c0+r%mu0*p%sig))**(f2%n) &
                           *DEXP(-r%alpha*f2%n*Sinit*lg10) &
                           *(p%sig/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                           *DEXP(-f2%Q/in%R*(1._8/Tinit-1._8/f2%To))
                   Vinit=V1+V2
                CASE(3)
                   V1=f1%Vo*(tau0/(f1%c0+r%mu0*p%sig))**(f1%n) &
                           *DEXP(-r%alpha*f1%n*Sinit*lg10) &
                           *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                           *DEXP(-f1%Q/in%R*(1._8/Tinit-1._8/f1%To))
                   V2=f2%Vo*(tau0/(f2%c0+r%mu0*p%sig))**(f2%n) &
                           *DEXP(-r%alpha*f2%n*Sinit*lg10) &
                           *(p%sig/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                           *DEXP(-f2%Q/in%R*(1._8/Tinit-1._8/f2%To))
                   V3=f3%Vo*(tau0/(f3%c0             ))**(f3%n) &
                           *(p%sig/r%sigma0)**(           -f3%zeta) &
                           *DEXP(-f3%Q/in%R*(1._8/Tinit-1._8/f3%To))
                   Vinit=V1+V2+V3
                CASE DEFAULT
                   WRITE (0,'("incorrect flow law ", I2)') r%nFlow
                   WRITE_DEBUG_INFO(100)
                   STOP 3
                END SELECT
             END IF

             ! Dirichlet boundary condition
             IF (p%dirichlet) THEN
                Vinit=p%Vl
             END IF
      
             ! traction in strike direction
             y(l+STATE_VECTOR_TRACTION_STRIKE) = tau0*COS(p%rake)
      
             ! traction in dip direction
             y(l+STATE_VECTOR_TRACTION_DIP) = tau0*SIN(p%rake)
      
             ! traction in normal direction
             y(l+STATE_VECTOR_TRACTION_NORMAL) = 0._8
      
             ! state variable log10(d/d0)
             y(l+STATE_VECTOR_STATE_1) = Sinit
      
             ! maximum velocity
             vMax(j)=MAX(Vinit,vMax(j))
   
             ! maximum temperature
             tMax(j)=MAX(Tinit,tMax(j))

             ! moment-rate
             momentRate(j)=momentRate(j)+(Vinit-p%Vl)*in%mu*in%dx1*in%dx2

             ! slip velocity log10(V)
             y(l+STATE_VECTOR_VELOCITY) = log(Vinit)/lg10

             ! initial temperature
             y(l+STATE_VECTOR_TEMPERATURE) = Tinit

          END DO
       END DO
    END DO

  END SUBROUTINE initStateVector

  !-----------------------------------------------------------------------
  !> subroutine odefun
  ! evalutes the derivative of the state vector
  !
  ! @param n - number of state elements own by current thread
  ! @param m - degrees of freedom
  !
  ! DESCRIPTION:
  !   1- extract slip velocity and strain rate from state vector
  !   2- calculate the rate of traction and rate of stress
  !   3- calculate the rate of remaining state variables
  !----------------------------------------------------------------------
  SUBROUTINE odefun(n,time,y,dydt)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: time
    REAL*8, INTENT(INOUT)    :: y(n)
    REAL*8, INTENT(INOUT) :: dydt(n)

    INTEGER :: i1,i2,j,k,l
    TYPE(PATCH_ELEMENT_STRUCT) :: p
    TYPE(ROCK_STRUCT) :: r
    TYPE(HEALING_STRUCT) :: h1,h2
    TYPE(FLOW_STRUCT) :: f1,f2,f3

    REAL*8 :: correction

    ! traction components in the strike and dip directions
    REAL*8 :: ts, td

    ! norm of shear traction and rate of change
    REAL*8 :: tau,dtau

    ! effective property
    REAL*8 :: mut

    ! velocity scalar
    REAL*8 :: V,V1,V2,V3

    ! temperature
    REAL*8 :: T

    ! slip velocity in the strike and dip directions
    REAL*8 :: vs,vd

    ! rake of traction and velocity
    REAL*8 :: rake

    ! normal stress
    REAL*8 :: sigma

    ! effective parameters
    REAL*8 :: nt,Qt,zetat

    ! maximum velocity
    vMax=0._8

    ! maximum temperature
    tMax=0._8

    ! initialize moment-rate
    momentRate=0._8

    !--------------------------------------------------------------------
    ! step 1/3 - extract slip velocity and strain rate from state vector
    !--------------------------------------------------------------------

    ! loop over faults
    DO j=1,in%nFault
       ! loop of fault elements
!$OMP PARALLEL DO PRIVATE(i1,k,l,ts,td,rake,velocity,vs,vd) REDUCTION(MAX:vMax) REDUCTION(MAX:tMax)
       DO i2=1,layout%N2L(1+rank)
          DO i1=1,in%N1
          
             ! patch index
             k=((j-1)*layout%N2L(1+rank)+(i2-1))*in%N1+i1

             ! state vector index
             l=(k-1)*STATE_VECTOR_DGF+1

             ! traction and rake
             ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
             td=y(l+STATE_VECTOR_TRACTION_DIP)
             rake=ATAN2(td,ts)
   
             ! slip velocity
             V=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)
             vs=V*COS(rake)
             vd=V*SIN(rake)

             ! maximum velocity
             vMax(j)=MAX(V,vMax(j))

             ! maximum temperature
             tMax(j)=MAX(y(l+STATE_VECTOR_TEMPERATURE),tMax(j))

             ! update state vector (rate of slip components)
             dydt(l+STATE_VECTOR_SLIP_STRIKE)=vs
             dydt(l+STATE_VECTOR_SLIP_DIP   )=vd
 
             ! slip velocity
             in%fault(j)%s1r(i1,i2)=vs-in%patch(k)%Vl*COS(in%patch(k)%rake)
             in%fault(j)%s2r(i1,i2)=vd-in%patch(k)%Vl*SIN(in%patch(k)%rake)

          END DO
       END DO
!$OMP END PARALLEL DO
    END DO

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

    ! forward Fourier transform source
    DO j=1,in%nFault
       CALL fft2(in%N1,layout%N2L(1+rank),in%fault(j)%s1r,layout%N1L(1+rank),in%N2,in%fault(j)%s1c,layout)
       CALL fft2(in%N1,layout%N2L(1+rank),in%fault(j)%s2r,layout%N1L(1+rank),in%N2,in%fault(j)%s2c,layout)
    END DO

    ! loop over receiver faults
    DO k=1,in%nFault
       ! loop over source faults
       DO j=1,in%nFault

          ! traction component on fault k
          CALL computeTraction(in%fault(k),in%fault(j),layout%N1L(1+rank),layout%i1start(1+rank), &
                  in%N1,in%N2,in%dx1,in%dx2,in%mu,in%alpha,(j .EQ. 1))

       END DO
    END DO

    DO j=1,in%nFault
       CALL ifft2(layout%N1L(1+rank),in%N2,in%fault(j)%s13c,in%N1,layout%N2L(1+rank),in%fault(j)%s13r,layout)
       CALL ifft2(layout%N1L(1+rank),in%N2,in%fault(j)%s23c,in%N1,layout%N2L(1+rank),in%fault(j)%s23r,layout)
       CALL ifft2(layout%N1L(1+rank),in%N2,in%fault(j)%s33c,in%N1,layout%N2L(1+rank),in%fault(j)%s33r,layout)
    END DO

    !-----------------------------------------------------------------
    ! step 3/3 - calculate the rate of remaining state variables
    !-----------------------------------------------------------------

    ! loop over faults
    DO j=1,in%nFault
       ! loop of fault elements
!$OMP PARALLEL DO PRIVATE(i1,k,l,p,r,h1,h2,f1,f2,f3,ts,td,tau,rake,V,V1,V2,V3,d,dtau,sigma,T,nt,Qt,zetat,correction) &
!$OMP & REDUCTION(+:momentRate) 
       DO i2=1,layout%N2L(1+rank)
          DO i1=1,in%N1

             ! cumulative patch index
             k=((j-1)*layout%N2L(1+rank)+(i2-1))*in%N1+i1

             ! state vector index
             l=(k-1)*STATE_VECTOR_DGF+1

             ! current fault patch
             p=in%patch(k)

             ! constant velocity boundary condition
             IF (p%dirichlet) THEN
                dydt(l+STATE_VECTOR_TRACTION_STRIKE)=0._8
                dydt(l+STATE_VECTOR_TRACTION_DIP   )=0._8
                dydt(l+STATE_VECTOR_TRACTION_NORMAL)=0._8
                dydt(l+STATE_VECTOR_STATE_1)=0._8
                dydt(l+STATE_VECTOR_VELOCITY)=0._8
#ifdef BATH
                dydt(l+STATE_VECTOR_TEMPERATURE)=0._8
#endif
                CYCLE
             END IF

             ! current rock
             r=in%rock(p%rockType)

             ! traction and rake
             ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
             td=y(l+STATE_VECTOR_TRACTION_DIP)
             tau=SQRT(ts**2+td**2)
             rake=ATAN2(td,ts)

             ! slip velocity
             V=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)
      
             ! temperature
             T=y(l+STATE_VECTOR_TEMPERATURE)

             ! moment-rate
             momentRate(j)=momentRate(j)+V*in%mu*in%dx1*in%dx2
      
             ! rate of state
             SELECT CASE (evolutionLaw)
             CASE(1)
                ! aging-law end-member
                SELECT CASE (r%nHealing)
                CASE(1)
                   h1=r%healing(1)
                   dydt(l+STATE_VECTOR_STATE_1)=( &
                           h1%f0*DEXP(-h1%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h1%p &
                           *(p%sig/r%sigma0)**h1%q &
                           *DEXP(-h1%H/in%R*(1._8/T-1._8/h1%To)) &
                           -r%lambda*V/2/p%h)/lg10
                CASE(2)
                   h1=r%healing(1)
                   h2=r%healing(2)
                   dydt(l+STATE_VECTOR_STATE_1)=( &
                           h1%f0*DEXP(-h1%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h1%p &
                           *(p%sig/r%sigma0)**h1%q &
                           *DEXP(-h1%H/in%R*(1._8/T-1._8/h1%To)) &
                          +h2%f0*DEXP(-h2%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h2%p &
                          *(p%sig/r%sigma0)**h2%q &
                           *DEXP(-h2%H/in%R*(1._8/T-1._8/h2%To)) &
                           -r%lambda*V/2/p%h)/lg10
                CASE DEFAULT
                   WRITE (0,'("unhandled option ", I2, " (this is a bug")') r%nHealing
                   WRITE_DEBUG_INFO(100)
                   STOP 3
                END SELECT
             CASE(2)
                ! slip-law end-member
                SELECT CASE (r%nHealing)
                CASE(1)
                   h1=r%healing(1)
                   dydt(l+STATE_VECTOR_STATE_1)=(r%lambda*V*LOG( &
                           2*p%h/r%lambda/V*( &
                           h1%f0*DEXP(-h1%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h1%p &
                           *(p%sig/r%sigma0)**h1%q &
                           *DEXP(-h1%H/in%R*(1._8/T-1._8/h1%To)) &
                           )))/lg10
                CASE(2)
                   h1=r%healing(1)
                   h2=r%healing(2)
                   dydt(l+STATE_VECTOR_STATE_1)=(r%lambda*V*LOG( &
                           2*p%h/r%lambda/V*( &
                           h1%f0*DEXP(-h1%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h1%p &
                           *(p%sig/r%sigma0)**h1%q &
                           *DEXP(-h1%H/in%R*(1._8/T-1._8/h1%To)) &
                          +h2%f0*DEXP(-h2%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h2%p &
                           *(p%sig/r%sigma0)**h2%q &
                           *DEXP(-h2%H/in%R*(1._8/T-1._8/h2%To)) &
                           )))/lg10
                CASE DEFAULT
                   WRITE (0,'("unhandled option ", I2, " (this is a bug")') r%nHealing
                   WRITE_DEBUG_INFO(100)
                   STOP 3
                END SELECT
             CASE DEFAULT
                WRITE (0,'("unhandled option ", I2, " (this is a bug")') evolutionLaw
                WRITE_DEBUG_INFO(100)
                STOP 3
             END SELECT
         
             ! rate of temperature
             dydt(l+STATE_VECTOR_TEMPERATURE)=-p%DW2*(T-p%Tb)+tau*V/p%wRhoC

             ! scalar rate of shear traction
             dtau=in%fault(j)%s13r(i1,i2)*COS(rake) &
                 +in%fault(j)%s23r(i1,i2)*SIN(rake)
                
             ! normal stress
             sigma=p%sig-y(l+STATE_VECTOR_TRACTION_NORMAL)
             !sigma=p%sig
      
             ! acceleration (1/V dV/dt) / log(10)
             SELECT CASE(r%nFlow)
             CASE(1)
                f1=r%flow(1)
                nt=f1%n
                Qt=f1%Q
                zetat=f1%zeta
                mut=r%mu0*sigma/(f1%c0+r%mu0*sigma)
             CASE(2)
                f1=r%flow(1)
                f2=r%flow(2)
                V1=f1%Vo*(tau/(f1%c0+r%mu0*p%sig))**f1%n &
                        *DEXP(-r%alpha*f1%n*y(l+STATE_VECTOR_STATE_1)*lg10) &
                        *(sigma/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                        *DEXP(-f1%Q/in%R*(1._8/T-1._8/f1%To))
                V2=f2%Vo*(tau/(f2%c0+r%mu0*p%sig))**f2%n &
                        *DEXP(-r%alpha*f2%n*y(l+STATE_VECTOR_STATE_1)*lg10) &
                        *(sigma/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                        *DEXP(-f2%Q/in%R*(1._8/T-1._8/f2%To))
                nt=(V1*f1%n+V2*f2%n)/(V1+V2)
                Qt=(V1*f1%Q+V2*f2%Q)/(V1+V2)
                zetat=(V1*f1%zeta+V2*f2%zeta)/(V1+V2)
                mut=(r%mu0*sigma/(f1%c0+r%mu0*sigma)*V1 &
                    +r%mu0*sigma/(f2%c0+r%mu0*sigma)*V2)/(V1+V2)
             CASE(3)
                f1=r%flow(1)
                f2=r%flow(2)
                f3=r%flow(3)
                V1=f1%Vo*(tau/(f1%c0+r%mu0*p%sig))**f1%n &
                        *DEXP(-r%alpha*f1%n*y(l+STATE_VECTOR_STATE_1)*lg10) &
                        *(sigma/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                        *DEXP(-f1%Q/in%R*(1._8/T-1._8/f1%To))
                V2=f2%Vo*(tau/(f2%c0+r%mu0*p%sig))**f2%n &
                        *DEXP(-r%alpha*f2%n*y(l+STATE_VECTOR_STATE_1)*lg10) &
                        *(sigma/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                        *DEXP(-f2%Q/in%R*(1._8/T-1._8/f2%To))
                V3=f3%Vo*(tau/(f3%c0             ))**f3%n &
                        *(sigma/r%sigma0)**(           -f3%zeta) &
                        *DEXP(-f3%Q/in%R*(1._8/T-1._8/f3%To))
                nt=(V1*f1%n+V2*f2%n+V3*f3%n)/(V1+V2+V3)
                Qt=(V1*f1%Q+V2*f2%Q+V3*f3%Q)/(V1+V2+V3)
                zetat=(V1*f1%zeta+V2*f2%zeta+V3*f3%zeta)/(V1+V2+V3)
                mut=(r%mu0*sigma/(f1%c0+r%mu0*sigma)*V1 &
                    +r%mu0*sigma/(f2%c0+r%mu0*sigma)*V2)/(V1+V2+V3)
             CASE DEFAULT
                WRITE (0,'("invalid flow type ", I2, " (this is a bug")') r%nFlow
                WRITE_DEBUG_INFO(100)
                STOP 3
             END SELECT
         
             dydt(l+STATE_VECTOR_VELOCITY)= &
                     (dtau - r%alpha*tau*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                           + (mut-r%beta+zetat/nt)*tau*dydt(l+STATE_VECTOR_TRACTION_NORMAL)/sigma &
                           + Qt*tau/nt/in%R*dydt(l+STATE_VECTOR_TEMPERATURE)/T**2) &
                    /(tau/nt+in%damping*V) / lg10
             ! correction
             correction=in%damping*V*dydt(l+STATE_VECTOR_VELOCITY)*lg10
      
             ! traction rate
             dydt(l+STATE_VECTOR_TRACTION_STRIKE)=in%fault(j)%s13r(i1,i2)-correction*COS(rake)
             dydt(l+STATE_VECTOR_TRACTION_DIP   )=in%fault(j)%s23r(i1,i2)-correction*SIN(rake)
             dydt(l+STATE_VECTOR_TRACTION_NORMAL)=in%fault(j)%s33r(i1,i2)
          END DO
       END DO
!$OMP END PARALLEL DO
    END DO

  END SUBROUTINE odefun

  !-----------------------------------------------------------------------
  !> subroutine printParallelism()
  !! displays the data layout
  !-----------------------------------------------------------------------
  SUBROUTINE printParallelism()

    INTEGER :: j

    IF (0 .EQ. rank) THEN
       PRINT '("# data layout")'
       PRINT '("# ---------- rows ------------")'
       PRINT '("# rank        N1   N2L i2start")'
       DO j=0,csize-1
          PRINT '(I6.3,X,I5.5,"(+2)",X,I5.5,X,I7.5)',j,in%N1,layout%N2L(1+j),layout%i2start(1+j)
       END DO
       PRINT '("# ")'
       PRINT '("# --------- columns ----------")'
       PRINT '("# rank    N1   N2L i1start")'
       DO j=0,csize-1
          PRINT '(I6.3,X,I5.5,X,I5.5,X,I7.5)',rank,layout%N1L(1+j),in%N2,layout%i1start(1+j)
       END DO
       CALL FLUSH(STDOUT)
    END IF

  END SUBROUTINE printParallelism

  !-----------------------------------------------------------------------
  !> subroutine initParallelism()
  !! initialize variables describe the data layout for parallelism.
  !!
  !! OUTPUT:
  !! layout    - list of receiver type and type index
  !-----------------------------------------------------------------------
  SUBROUTINE initParallelism()
    IMPLICIT NONE

    INTEGER :: i,j,n,remainder,ierr

    ALLOCATE(layout%N1L(csize), &
             layout%N2L(csize), &
             layout%i1start(csize), &
             layout%i2start(csize),STAT=ierr)
    IF (0/=ierr) STOP "could not allocate layout"

    ALLOCATE(layout%rcounts(csize),layout%displs(csize),STAT=ierr)
    IF (0/=ierr) STOP "could not allocate layout"

    ALLOCATE(layout%vector(csize,csize),STAT=ierr)
    IF (0/=ierr) STOP "could not allocate layout"

    ! each column must have a factor of 2 REAL to convert to COMPLEX
    ! two-column buffer added for Fourier transform
    n=(in%N1+2)/2
    remainder=n-INT(n/csize)*csize
    IF (0 .LT. remainder) THEN
       layout%N1L(1:(csize-remainder))      =INT(n/csize)*2
       layout%N1L((csize-remainder+1):csize)=(INT(n/csize)+1)*2
    ELSE
       layout%N1L=INT(n/csize)*2
    END IF
    CALL cumsum1(csize,layout%N1L,layout%i1start)

    n=in%N2
    remainder=n-INT(n/csize)*csize
    IF (0 .LT. remainder) THEN
       layout%N2L(1:(csize-remainder))      =INT(n/csize)
       layout%N2L((csize-remainder+1):csize)=INT(n/csize)+1
    ELSE
       layout%N2L(1:csize)=INT(n/csize)
    END IF
    CALL cumsum1(csize,layout%N2L,layout%i2start)

    ! data layout for MPI gather and scatter
    DO j=1,csize
       DO i=1,csize
          CALL MPI_TYPE_VECTOR(layout%N2L(j),layout%N1L(i),in%N1+2,MPI_REAL8,layout%vector(i,j),ierr)
          CALL MPI_TYPE_COMMIT(layout%vector(i,j),ierr)
       END DO
    END DO

#ifdef NETCDF
    CALL MPI_TYPE_VECTOR(layout%N2L(1+rank),1,in%N1*STATE_VECTOR_DGF,MPI_REAL8,layout%profileVector,ierr)
    CALL MPI_TYPE_COMMIT(layout%profileVector,ierr)
#endif

  END SUBROUTINE initParallelism

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE getopt_m

    TYPE(SIMULATION_STRUCT), INTENT(OUT) :: in

    CHARACTER :: ch
    CHARACTER(512) :: dataline
    CHARACTER(256) :: filename
    INTEGER :: iunit,noptions,l
    LOGICAL :: new
    TYPE(OPTION_S) :: opts(15)

    REAL*8 :: d_diff,w_diff,w_heat,rhoc
    REAL*4 :: d_diff_temp,w_diff_temp,w_heat_temp,rhoc_temp
    REAL*4 :: d_diff_min,w_diff_min,w_heat_min,rhoc_min
    REAL*4 :: d_diff_max,w_diff_max,w_heat_max,rhoc_max
  
    INTEGER :: i1,i2,k,ierr,position
    INTEGER, PARAMETER :: psize=512
    CHARACTER, DIMENSION(psize) :: packed

    TYPE(PATCH_ELEMENT_STRUCT4), DIMENSION(:), ALLOCATABLE :: patchAll
    TYPE(PATCH_ELEMENT_STRUCT4) :: patchTemp
    TYPE(PATCH_ELEMENT_STRUCT4) :: patchMin, patchMax

    REAL*8, DIMENSION(:,:), ALLOCATABLE :: z

    ! define long options, such as --dry-run
    ! parse the command line for options
    opts( 1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts( 2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts( 3)=OPTION_S("epsilon",.TRUE.,'e')
    opts( 4)=OPTION_S("export-netcdf",.FALSE.,'n')
    opts( 5)=OPTION_S("export-stress",.FALSE.,'s')
    opts( 6)=OPTION_S("export-temperature",.FALSE.,CHAR(23))
    opts( 7)=OPTION_S("source-export-rate",.TRUE.,'r')
    opts( 8)=OPTION_S("import-state",.TRUE.,'t')
    opts( 9)=OPTION_S("export-state",.FALSE.,'x')
    opts(10)=OPTION_S("evolution-law",.TRUE.,'f')
    opts(11)=OPTION_S("grd-input",.FALSE.,'g')
    opts(12)=OPTION_S("maximum-step",.TRUE.,'m')
    opts(13)=OPTION_S("maximum-iterations",.TRUE.,'i')
    opts(14)=OPTION_S("help",.FALSE.,'h')
    opts(15)=OPTION_S("verbose",.TRUE.,'v')

    noptions=0
    DO
       ch=getopt("he:f:gi:m:nr:st:v:x",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isversion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isdryrun=.TRUE.
       CASE('e')
          ! numerical accuracy (variable epsilon sits in the ode45 module)
          READ(optarg,*) epsilon
          noptions=noptions+1
       CASE('r')
          ! source export rate
          READ(optarg,*) in%sourceExportRate
          noptions=noptions+1
       CASE('f')
          ! type of evolution law
          READ(optarg,*) evolutionLaw
          noptions=noptions+1
       CASE('g')
          ! option dry-run
          in%isGrdInput=.TRUE.
       CASE('i')
          ! maximum number of iterations
          READ(optarg,*) maximumIterations
          noptions=noptions+1
       CASE('m')
          ! maximum time step (variable maximumTimeStep sits in the ode45 module)
          READ(optarg,*) maximumTimeStep
          noptions=noptions+1
       CASE('n')
          ! export in netcdf format
          in%isExportNetcdf=.TRUE.
       CASE('s')
          ! export stress in netcdf format
          in%isExportStress=.TRUE.
       CASE(CHAR(23))
          ! export temperature in netcdf format
          in%isExportTemperature=.TRUE.
       CASE('t')
          ! export in netcdf format
          in%isImportState=.TRUE.
          READ(optarg,'(a)') importStateDir
          noptions=noptions+1
       CASE('x')
          ! export in netcdf format
          in%isExportState=.TRUE.
       CASE('h')
          ! option help
          in%ishelp=.TRUE.
       CASE('v')
          ! verbosity
          READ(optarg,*) verbose
          noptions=noptions+1
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%ishelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled command-line option ", a, " (this is a bug)")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO

    IF (in%isversion) THEN
       CALL printversion()
       ! abort parameter input
       STOP
    END IF

    IF (in%ishelp) THEN
       CALL printhelp()
       ! abort parameter input
       STOP
    END IF

    IF (0.EQ.rank) THEN
       PRINT 2000
       PRINT '("# MOTORCYCLE")'
       PRINT '("# quasi-dynamic earthquake simulation in three-dimensional media")'
       PRINT '("# with the spectral boundary integral method, accounting for temperature")'
       PRINT '("# effects and the thermobaric activation of fault friction.")'
       SELECT CASE(evolutionLaw)
       CASE(1)
          PRINT '("# evolution law: aging-law end-member")'
       CASE(2)
          PRINT '("# evolution law: slip-law end-member")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') evolutionLaw
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       IF (in%isExportNetcdf) THEN
          PRINT '("# export velocity to netcdf:      yes")'
       ELSE
          PRINT '("# export velocity to netcdf:       no")'
       END IF
       IF (in%isImportState) THEN
          PRINT '("# import state vector:            yes")'
       END IF
       IF (in%isExportState) THEN
          PRINT '("# export state vector:            yes")'
       END IF
   
       PRINT '("# numerical accuracy:     ",ES11.4)', epsilon
       PRINT '("# maximum iterations:     ",I11)', maximumIterations
       PRINT '("# maximum time step:     ",ES12.4)', maximumTimeStep
       PRINT '("# number of MPI threads: ",I12)', csize
!$     PRINT '("# number of OpenMP threads:   ",I3.3,"/",I3.3)', &
!$                  omp_get_max_threads(),omp_get_num_procs()
       PRINT 2000

       IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
          ! read from input file
          iunit=25
          CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
          OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
       ELSE
          ! get input parameters from standard input
          iunit=5
       END IF

       PRINT '("# output directory")'
       CALL getdata(iunit,dataline)
       READ (dataline,'(a)') in%wdir
       PRINT '(2X,a)', TRIM(in%wdir)
   
       in%timeFilename=TRIM(in%wdir)//"/time.dat"
   
       ! test write permissions on output directory
       OPEN (UNIT=FPTIME,FILE=in%timeFilename,POSITION="APPEND",&
               IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
          STOP 1
       END IF
       CLOSE(FPTIME)
   
       PRINT '("# elastic moduli (Lame, rigidity), radiation damping (G/2Vs), universal gas constant")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%lambda,in%mu,in%damping,in%R
       PRINT '(4ES9.2E1)', in%lambda,in%mu,in%damping,in%R
   
       IF (0 .GT. in%mu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: shear modulus must be positive")')
          STOP 2
       END IF
   
       IF (0 .GT. in%damping) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: radiation damping must be positive")')
          STOP 2
       END IF

       IF (0 .GE. in%R) THEN
          WRITE_DEBUG_INFO(200)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("error in input file: R must be positive.")')
          STOP 1
       END IF

       in%nu=in%lambda/2._8/(in%lambda+in%mu)
       in%alpha=1._8/2/(1._8-in%nu)
   
       IF (-1._8 .GT. in%nu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be greater than -1.")')
          STOP 2
       END IF
       IF (0.5_8 .LT. in%nu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be lower than 0.5.")')
          STOP 2
       END IF
   
       PRINT '("# time interval")'
          CALL getdata(iunit,dataline)
       READ  (dataline,*) in%interval
       PRINT '(ES20.12E2)', in%interval
   
       IF (in%interval .LE. 0._8) THEN
          WRITE (STDERR,'("**** error **** ")')
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("simulation time must be positive. exiting.")')
          STOP 1
       END IF
   
       PRINT '("# number of faults")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nFault
       PRINT '(I3)', in%nFault
   
       PRINT '("# grid dimension (N1,N2)")'
       CALL getdata(iunit,dataline)
       READ (dataline,*) in%N1,in%N2
       PRINT '(2I5)', in%N1, in%N2

       IF (1 .EQ. MOD(in%N1,2)) THEN
          WRITE (STDERR,'("**** error **** ")')
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("N1 must be a factor of 2. exiting.")')
          STOP 1
       END IF
   
       PRINT '("# sampling (dx1,dx2)")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%dx1,in%dx2
       PRINT '(2ES9.2E1)', in%dx1,in%dx2
   
       ! number of rocks
       PRINT '("# number of rocks")'
       CALL getdata(iunit,dataline)
       READ (dataline,*) in%nRock
       PRINT '(2I2)', in%nRock

       ! check for rock rheology model
       IF (0.GE.in%nRock) STOP "could not allocate the state vector"

       ! rheology models
       ALLOCATE(in%rock(in%nRock),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the rheology list"

       DO j=1,in%nRock
          PRINT '("# rock index, rock name")'
          CALL getdata(iunit,dataline)
          READ (dataline,*) i,in%rock(j)%name
          PRINT '(I2,X,a)', i, TRIM(in%rock(j)%name)
          IF (i.NE.j) STOP "index mismatch for rock type"

          PRINT '("#     mu0        d0    sigma0     alpha      beta")'
          CALL getdata(iunit,dataline)
          READ (dataline,*) in%rock(j)%mu0
          READ (dataline,*)  &
                in%rock(j)%mu0, &
                in%rock(j)%d0, &
                in%rock(j)%sigma0, &
                in%rock(j)%alpha, &
                in%rock(j)%beta

          PRINT '(F9.3,4ES10.2E1)', &
               in%rock(j)%mu0, &
               in%rock(j)%d0, &
               in%rock(j)%sigma0, &
               in%rock(j)%alpha, &
               in%rock(j)%beta

          PRINT '("# number of flow laws for rock ",I2)',j
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) in%rock(j)%nFlow
          PRINT '(I2)', in%rock(j)%nFlow
          IF (0.GE.in%rock(j)%nFlow) STOP "rocks must have a flow law"

          ! rheology models
          ALLOCATE(in%rock(j)%flow(in%rock(j)%nFlow),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the flow list"

          PRINT '("#  n        Vo        c0         n         Q        To      zeta")'
          DO k=1,in%rock(j)%nFlow
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%rock(j)%flow(k)%Vo, &
                   in%rock(j)%flow(k)%c0, &
                   in%rock(j)%flow(k)%n, &
                   in%rock(j)%flow(k)%Q, &
                   in%rock(j)%flow(k)%To, &
                   in%rock(j)%flow(k)%zeta
             IF (i.NE.k) STOP "flow law index mismatch"

             PRINT '(I4,6ES10.4E1)', i, &
                  in%rock(j)%flow(k)%Vo, &
                  in%rock(j)%flow(k)%c0, &
                  in%rock(j)%flow(k)%n, &
                  in%rock(j)%flow(k)%Q, &
                  in%rock(j)%flow(k)%To, &
                  in%rock(j)%flow(k)%zeta
          END DO
          PRINT '("# number of healing terms for rock ",I2)',j
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) in%rock(j)%nHealing
          PRINT '(I2)', in%rock(j)%nHealing
          IF (0.GE.in%rock(j)%nHealing) STOP "rocks must have at least one healing term."

          ! healing terms
          ALLOCATE(in%rock(j)%healing(in%rock(j)%nHealing),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the healing list"

          PRINT '("#  n        fo         p         q         H        To")'
          DO k=1,in%rock(j)%nHealing
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%rock(j)%healing(k)%f0, &
                   in%rock(j)%healing(k)%p, &
                   in%rock(j)%healing(k)%q, &
                   in%rock(j)%healing(k)%H, &
                   in%rock(j)%healing(k)%To
             IF (i.NE.k) STOP "healing index mismatch"

             PRINT '(I4,5ES10.4E1)', i, &
                  in%rock(j)%healing(k)%f0, &
                  in%rock(j)%healing(k)%p, &
                  in%rock(j)%healing(k)%q, &
                  in%rock(j)%healing(k)%H, &
                  in%rock(j)%healing(k)%To

             IF (0 .GT. in%rock(j)%healing(k)%f0) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("reference rate of healing must be positive")')
                STOP 1
             END IF

             IF (0 .GT. in%rock(j)%healing(k)%p) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("size power exponent must be positive")')
                STOP 1
             END IF

             IF (0 .GT. in%rock(j)%healing(k)%q) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("normal stress power exponent must be positive")')
                STOP 1
             END IF

             IF (0 .GT. in%rock(j)%healing(k)%H) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("activation energy must be positive")')
                STOP 1
             END IF

             IF (0 .GE. in%rock(j)%healing(k)%To) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("activation temperature must be position")')
                STOP 1
             END IF

          END DO

          ! remove second healing mechanism with null contribution
          IF (2 .EQ. in%rock(j)%nHealing) THEN
             IF (0._8 .EQ. in%rock(j)%healing(2)%f0) THEN
                in%rock(j)%nHealing=1
             END IF
          END IF

          PRINT '("# reciprocal of characteristic strain for weakening (lambda)")'
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) in%rock(j)%lambda
          PRINT '(2ES9.2E1)', in%rock(j)%lambda
          IF (0._8.GE.in%rock(j)%lambda) STOP "characteristic strain must be positive."

       END DO ! number of rocks
   
       ! setup data layout for threads
       CALL initParallelism()

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        A L L O C A T E   M E M O R Y
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
   
       ! local patches
       ALLOCATE(in%patch(in%nFault*in%N1*layout%N2L(1+rank)),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the patch list"

       ! all patches
       ALLOCATE(patchAll(in%nFault*in%N1*in%N2),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the patch list"
   
       ! work space for all faults
       ALLOCATE(in%fault(in%nFault),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the fault list"
   
       ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       !   T H E R M O - M E C H A N I C A L   P R O P E R T I E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   
#ifdef NETCDF
       IF (in%isGrdInput) THEN
          ! read friction properties from .grd files

          DO j=1,in%nFault
             PRINT 2000
             IF (1 .EQ. j) THEN
                PRINT '("# physical properites of fault 1")'
                in%fault(j)%x3=0
             ELSE
                PRINT '("# relative position of fault ",I2)', j
                CALL getdata(iunit,dataline)
                READ  (dataline,*) in%fault(j)%x3
                PRINT '(ES9.2E1)', in%fault(j)%x3
             END IF

             ! allocate temporary data
             ALLOCATE(z(in%N1,in%N2),STAT=ierr)
             IF (ierr>0) STOP "could not allocate temporary data"

             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
             !   L I T H O L O G Y   
             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
             PRINT 2000
             PRINT '("# rock of fault ",I2)', j
             PRINT '("# .grd file for rock type")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             d_diff_min= HUGE(1.e0)
             d_diff_max=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%rockType=INT(z(i1,i2))
                END DO
             END DO

             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
             !        T H E R M A L   P R O P E R T I E S
             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
      
             PRINT 2000
             PRINT '("# thermal properties of fault ",I2)', j
      
             PRINT '("# .grd file for temperature diffusivity")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             d_diff_min= HUGE(1.e0)
             d_diff_max=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%DW2=REAL(z(i1,i2),4)
                   d_diff_min=MIN(d_diff_min,patchAll(k)%DW2)
                   d_diff_max=MAX(d_diff_max,patchAll(k)%DW2)
                END DO
             END DO

             PRINT '("# .grd file for membrane diffusion width")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             w_diff_min= HUGE(1.e0)
             w_diff_max=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%DW2=REAL(patchAll(k)%DW2/z(i1,i2)**2,4)
                   w_diff_min=MIN(w_diff_min,REAL(z(i1,i2),4))
                   w_diff_max=MAX(w_diff_max,REAL(z(i1,i2),4))
                END DO
             END DO

             PRINT '("# .grd file for shear zone width")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             w_heat_min= HUGE(1.e0)
             w_heat_max=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%wRhoC=REAL(z(i1,i2),4)
                   w_heat_min=MIN(w_heat_min,patchAll(k)%wRhoC)
                   w_heat_max=MAX(w_heat_max,patchAll(k)%wRhoC)
                END DO
             END DO

             PRINT '("# .grd file for volumetric heat capacity")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             rhoc_min= HUGE(1.e0)
             rhoc_max=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%wRhoC=REAL(patchAll(k)%wRhoC*z(i1,i2),4)
                   rhoc_min=MIN(rhoc_min,REAL(z(i1,i2),4))
                   rhoc_max=MAX(rhoc_max,REAL(z(i1,i2),4))
                END DO
             END DO

             PRINT '("# .grd file for bath temperature")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             patchMin%Tb= HUGE(1.e0)
             patchMax%Tb=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%Tb=REAL(z(i1,i2),4)
                   patchMin%Tb=MIN(patchMin%Tb,patchAll(k)%Tb)
                   patchMax%Tb=MAX(patchMax%Tb,patchAll(k)%Tb)
                END DO
             END DO

             PRINT 2000
             PRINT '("#               D        W        w     rhoc       Tb")'
             PRINT 2000
             PRINT '("# min:  ",5ES9.3E1)', &
                  d_diff_min, &
                  w_diff_min, &
                  w_heat_min, &
                  rhoc_min, &
                  patchMin%Tb

             PRINT '("# max:  ",5ES9.3E1)', &
                  d_diff_max, &
                  w_diff_max, &
                  w_heat_max, &
                  rhoc_max, &
                  patchMax%Tb

             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
             !     M E C H A N I C A L   P R O P E R T I E S
             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
      
             PRINT '("# .grd file for tau0")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             patchMin%tau0=HUGE(1.e0)
             patchMax%tau0=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%tau0=REAL(z(i1,i2),4)
                   patchMin%tau0     =MIN(patchMin%tau0,patchAll(k)%tau0)
                   patchMax%tau0     =MAX(patchMax%tau0,patchAll(k)%tau0)
                END DO
             END DO

             PRINT '("# .grd file for sigma")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             patchMin%sig=HUGE(1.e0)
             patchMax%sig=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%sig=REAL(z(i1,i2),4)
                   patchMin%sig      =MIN(patchMin%sig,patchAll(k)%sig)
                   patchMax%sig      =MAX(patchMax%sig,patchAll(k)%sig)
                END DO
             END DO

             PRINT '("# .grd file for fault thickness h (m)")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             patchMin%h=HUGE(1.e0)
             patchMax%h=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%h=REAL(z(i1,i2),4)
                   patchMin%h        =MIN(patchMin%h,patchAll(k)%h)
                   patchMax%h        =MAX(patchMax%h,patchAll(k)%h)
                END DO
             END DO

             PRINT '("# .grd file for Vl")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             patchMin%Vl=HUGE(1.e0)
             patchMax%Vl=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%Vl=REAL(z(i1,i2),4)
                   patchMin%Vl       =MIN(patchMin%Vl,patchAll(k)%Vl)
                   patchMax%Vl       =MAX(patchMax%Vl,patchAll(k)%Vl)
                END DO
             END DO

             PRINT '("# .grd file for rake")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             patchMin%rake=HUGE(1.e0)
             patchMax%rake=-HUGE(1.e0)
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%rake=REAL(z(i1,i2),4)
                   patchMin%rake     =MIN(patchMin%rake,patchAll(k)%rake)
                   patchMax%rake     =MAX(patchMax%rake,patchAll(k)%rake)
                END DO
             END DO

             PRINT '("# .grd file for boundary condition (0 == false)")'
             CALL getdata(iunit,dataline)
             READ (dataline,'(a)') filename
             PRINT '(a)', TRIM(filename)
             CALL readNetcdf(filename,in%N1,in%N2,z)
             patchMin%dirichlet=.TRUE.
             patchMax%dirichlet=.FALSE.
             DO i2=1,in%N2
                DO i1=1,in%N1
                   k=i1+((i2-1)+(j-1)*in%N2)*in%N1
                   patchAll(k)%dirichlet=(0 .NE. z(i1,i2))
                   patchMin%dirichlet=patchMin%dirichlet .AND. patchAll(k)%dirichlet
                   patchMax%dirichlet=patchMax%dirichlet .OR. patchAll(k)%dirichlet
                END DO
             END DO

             PRINT 2000
             PRINT '("#            tau0     sig       h      Vl     rake  BC")'
             PRINT 2000
             PRINT '(" minimum",ES9.2E1,ES8.2E1,ES8.2E1,ES9.2E2,ES9.2E1,L4)', &
                  patchMin%tau0, &
                  patchMin%sig, &
                  patchMin%h, &
                  patchMax%Vl, &
                  patchMin%rake, &
                  patchMin%dirichlet

             PRINT '(" maximum",ES9.2E1,ES8.2E1,ES8.2E1,ES9.2E2,ES9.2E1,L4)', &
                  patchMax%tau0, &
                  patchMax%sig, &
                  patchMax%h, &
                  patchMax%Vl, &
                  patchMax%rake, &
                  patchMax%dirichlet

          END DO ! in%nFault

          DEALLOCATE(z)
   
       ELSE
#endif
          DO j=1,in%nFault
             PRINT 2000
             IF (1 .EQ. j) THEN
                PRINT '("# physical properites of fault 1")'
                in%fault(j)%x3=0
             ELSE
                PRINT '("# relative position of fault ",I2)', j
                CALL getdata(iunit,dataline)
                READ  (dataline,*) in%fault(j)%x3
                PRINT '(ES9.2E1)', in%fault(j)%x3
             END IF
      
             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
             !   L I T H O L O G Y   
             ! - - - - - - - - - - - - - - - - - - - - - - - - - -

             ! temporary variable for compression of input
             patchTemp%rockType=HUGE(1)

             PRINT 2000
             PRINT '("#     n rock")'
             PRINT 2000
             DO i2=1,in%N2
                DO i1=1,in%N1
      
                   ! patch index
                   k=((j-1)*in%N2+(i2-1))*in%N1+i1
      
                   CALL getdata(iunit,dataline)
                   READ (dataline,*,IOSTAT=ierr) i
                   IF (0 .GT. i) THEN
                      patchAll(k)%rockType=patchTemp%rockType
                      i=-i
                      new=.FALSE.
                   ELSE
                      READ (dataline,*,IOSTAT=ierr) i,patchAll(k)%rockType

                      IF (0.GE.patchAll(k)%rockType) STOP "rock type must be positive."
                      IF (in%nRock.LT.patchAll(k)%rockType) STOP "undefined rock type."

                      new=.TRUE.
                   END IF
   
                   IF ((2 .LE. verbose) .OR. (new) .OR. &
                       (4 .GE. i1 .AND. 1 .EQ. i2) .OR. &
                       (in%N1-3 .LE. i1 .AND. in%N2 .EQ. i2)) THEN
   
                      PRINT '(I7,I5)',i,patchAll(k)%rockType
                   END IF
                      
                   IF (i .NE. k-(j-1)*in%N1*in%N2) THEN
                      WRITE_DEBUG_INFO(200)
                      WRITE (STDERR,'("error in input file: unexpected index")')
                      STOP 1
                   END IF
      
                   ! save previous value for compression
                   patchTemp=patchAll(k)

                END DO
             END DO

             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
             !        T H E R M A L   P R O P E R T I E S
             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
      
             PRINT '("# thermal properties of fault ",I2)', j
      
             PRINT 2000
             PRINT '("#     n        D        W        w     rhoc       Tb")'
             PRINT 2000

             ! parameter range
             d_diff_min=HUGE(1.e0)
             w_diff_min=HUGE(1.e0)
             w_heat_min=HUGE(1.e0)
             rhoc_min=HUGE(1.e0)
             patchMin%Tb=HUGE(1.e0)
   
             d_diff_max=-HUGE(1.e0)
             w_diff_max=-HUGE(1.e0)
             w_heat_max=-HUGE(1.e0)
             rhoc_max=-HUGE(1.e0)
             patchMax%Tb=-HUGE(1.e0)

             ! temporary variable for compression of input
             d_diff_temp=HUGE(1.e0)
             w_diff_temp=HUGE(1.e0)
             w_heat_temp=HUGE(1.e0)
             rhoc_temp=HUGE(1.e0)
             patchTemp%Tb=HUGE(1.e0)

             new=.FALSE.
             DO i2=1,in%N2
                DO i1=1,in%N1
   
                   ! cumulative patch index
                   k=((j-1)*in%N2+(i2-1))*in%N1+i1

                   CALL getdata(iunit,dataline)
                   READ (dataline,*,IOSTAT=ierr) i

                   IF (0 .GT. i) THEN
                      patchAll(k)%wRhoC=patchTemp%wRhoC
                      patchAll(k)%DW2=patchTemp%DW2
                      patchAll(k)%Tb=patchTemp%Tb
                      i=-i
                      new=.FALSE.
                   ELSE
                      READ (dataline,*,IOSTAT=ierr) i, &
                            d_diff,w_diff,w_heat,rhoc, &
                            patchAll(k)%Tb

                      ! lumped parameters
                      patchAll(k)%wRhoC=REAL(w_heat*rhoc,4)
                      patchAll(k)%DW2=REAL(d_diff/w_diff**2,4)
   
                      new=.TRUE.
                   END IF

                   IF ((2 .LE. verbose) .OR. (new) .OR. &
                       (4 .GE. i1 .AND. 1 .EQ. i2) .OR. &
                       (in%N1-3 .LE. i1 .AND. in%N2 .EQ. i2)) THEN

                      PRINT '(I7,5ES9.3E1)',i, &
                            d_diff,w_diff,w_heat,rhoc, &
                            patchAll(k)%Tb
                   END IF
                   
                   ! parameter range
                   d_diff_min =MIN(d_diff_min, d_diff)
                   w_diff_min =MIN(w_diff_min, w_diff)
                   w_heat_min =MIN(w_heat_min, w_heat)
                   rhoc_min   =MIN(rhoc_min,   rhoc)
                   patchMin%Tb=MIN(patchMin%Tb,patchAll(k)%Tb)

                   d_diff_max =MAX(d_diff_max, d_diff)
                   w_diff_max =MAX(w_diff_max, w_diff)
                   w_heat_max =MAX(w_heat_max, w_heat)
                   rhoc_max   =MAX(rhoc_max,   rhoc)
                   patchMax%Tb=MAX(patchMax%Tb,patchAll(k)%Tb)
   
                   ! save previous value for compression
                   patchTemp=patchAll(k)

                   IF (i .NE. k-(j-1)*in%N1*in%N2) THEN
                      WRITE_DEBUG_INFO(200)
                      WRITE (STDERR,'("invalid thermal property definition for patch ",I6)') i
                      WRITE (STDERR,'(a)') TRIM(dataline)
                      WRITE (STDERR,'("error in input file: unexpected index")')
                      STOP 1
                   END IF
   
                END DO
             END DO

             PRINT 2000
             PRINT '("# min: ",5ES9.3E1)', &
                  d_diff_min, &
                  w_diff_min, &
                  w_heat_min, &
                  rhoc_min, &
                  patchMin%Tb

             PRINT '("# max: ",5ES9.3E1)', &
                  d_diff_max, &
                  w_diff_max, &
                  w_heat_max, &
                  rhoc_max, &
                  patchMax%Tb

             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
             !      M E C H A N I C A L   P R O P E R T I E S
             ! - - - - - - - - - - - - - - - - - - - - - - - - - -
      
             PRINT '("# mechanical properties of fault ",I2)', j
      
             PRINT 2000
             PRINT '("#     n     tau0     sig        h       Vl     rake  BC")'
             PRINT 2000

             ! parameter range
             patchMin%tau0=HUGE(1.e0)
             patchMin%sig=HUGE(1.e0)
             patchMin%h=HUGE(1.e0)
             patchMin%Vl=HUGE(1.e0)
             patchMin%rake=HUGE(1.e0)
             patchMin%dirichlet=.TRUE.
   
             patchMax%tau0=-HUGE(1.e0)
             patchMax%sig=-HUGE(1.e0)
             patchMax%h=-HUGE(1.e0)
             patchMax%Vl=-HUGE(1.e0)
             patchMax%rake=-HUGE(1.e0)
             patchMax%dirichlet=.FALSE.
   
             patchTemp%tau0=HUGE(1.e0)
             patchTemp%sig=HUGE(1.e0)
             patchTemp%h=HUGE(1.e0)
             patchTemp%Vl=HUGE(1.e0)
             patchTemp%rake=HUGE(1.e0)
             patchTemp%dirichlet=.TRUE.
   
             new=.FALSE.
             DO i2=1,in%N2
                DO i1=1,in%N1
      
                   ! patch index
                   k=((j-1)*in%N2+(i2-1))*in%N1+i1
      
                   CALL getdata(iunit,dataline)
                   READ (dataline,*,IOSTAT=ierr) i
                   IF (0 .GT. i) THEN
                      patchAll(k)%tau0=patchTemp%tau0
                      patchAll(k)%sig=patchTemp%sig
                      patchAll(k)%h=patchTemp%h
                      patchAll(k)%Vl=patchTemp%Vl
                      patchAll(k)%rake=patchTemp%rake
                      patchAll(k)%dirichlet=patchTemp%dirichlet
                      i=-i
                      new=.FALSE.
                   ELSE
                      READ (dataline,*,IOSTAT=ierr) i, &
                            patchAll(k)%tau0, &
                            patchAll(k)%sig, &
                            patchAll(k)%h, &
                            patchAll(k)%Vl, &
                            patchAll(k)%rake, &
                            patchAll(k)%dirichlet
                      new=.TRUE.
                   END IF
   
                   IF ((2 .LE. verbose) .OR. (new) .OR. &
                       (4 .GE. i1 .AND. 1 .EQ. i2) .OR. &
                       (in%N1-3 .LE. i1 .AND. in%N2 .EQ. i2)) THEN
   
                      PRINT '(I7,ES9.2E1,ES8.2E1,ES9.2E1,ES9.2E2,ES9.2E1,L4)',i, &
                           patchAll(k)%tau0, &
                           patchAll(k)%sig, &
                           patchAll(k)%h, &
                           patchAll(k)%Vl, &
                           patchAll(k)%rake, &
                           patchAll(k)%dirichlet
                   END IF
                      
                   ! parameter range
                   patchMin%tau0     =MIN(patchMin%tau0,patchAll(k)%tau0)
                   patchMin%sig      =MIN(patchMin%sig,patchAll(k)%sig)
                   patchMin%h        =MIN(patchMin%h,patchAll(k)%h)
                   patchMin%Vl       =MIN(patchMin%Vl,patchAll(k)%Vl)
                   patchMin%rake     =MIN(patchMin%rake,patchAll(k)%rake)
                   patchMin%dirichlet=patchMin%dirichlet .AND. patchAll(k)%dirichlet
   
                   patchMax%tau0     =MAX(patchMax%tau0,patchAll(k)%tau0)
                   patchMax%sig      =MAX(patchMax%sig,patchAll(k)%sig)
                   patchMax%Vl       =MAX(patchMax%Vl,patchAll(k)%Vl)
                   patchMax%h        =MAX(patchMax%h,patchAll(k)%h)
                   patchMax%rake     =MAX(patchMax%rake,patchAll(k)%rake)
                   patchMax%dirichlet=patchMax%dirichlet .OR. patchAll(k)%dirichlet
   
                   ! save previous value for compression
                   patchTemp%tau0=patchAll(k)%tau0
                   patchTemp%sig=patchAll(k)%sig
                   patchTemp%h=patchAll(k)%h
                   patchTemp%Vl=patchAll(k)%Vl
                   patchTemp%rake=patchAll(k)%rake
                   patchTemp%dirichlet=patchAll(k)%dirichlet
   
                   ! convert to radian
                   patchAll(k)%rake=REAL(patchAll(k)%rake*DEG2RAD,4)
      
                   IF (i .NE. k-(j-1)*in%N1*in%N2) THEN
                      WRITE_DEBUG_INFO(200)
                      WRITE (STDERR,'("error in input file: unexpected index")')
                      STOP 1
                   END IF
      
                END DO
             END DO

             PRINT 2000
             PRINT '("# min: ",ES9.2E1,ES8.2E1,ES9.2E1,ES9.2E2,ES9.2E1,L4)', &
                  patchMin%tau0, &
                  patchMin%sig, &
                  patchMin%h, &
                  patchMin%Vl, &
                  patchMin%rake, &
                  patchMin%dirichlet

             PRINT '("# max: ",ES9.2E1,ES8.2E1,ES9.2E1,ES9.2E2,ES9.2E1,L4)', &
                  patchMax%tau0, &
                  patchMax%sig, &
                  patchMax%h, &
                  patchMax%Vl, &
                  patchMax%rake, &
                  patchMax%dirichlet

          END DO
#ifdef NETCDF
       END IF ! isGrdInput
#endif
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation patches")'
          CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationState
       PRINT '(I5)', in%nObservationState
       IF (0 .LT. in%nObservationState) THEN
          ALLOCATE(in%observationState(in%nObservationState),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation patches"
          PRINT 2000
          PRINT '("#   n fault     i1     i2 rate")'
          PRINT 2000
          DO k=1,in%nObservationState
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationState(k)%fault, &
                     in%observationState(k)%i1, &
                     in%observationState(k)%i2, &
                     in%observationState(k)%rate
   
             PRINT '(I5,X,I5,X,I6,X,I6,X,I4)', i, &
                     in%observationState(k)%fault, &
                     in%observationState(k)%i1, &
                     in%observationState(k)%i2, &
                     in%observationState(k)%rate
   
             IF (in%nFault .LT. in%observationState(k)%fault) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: wrong fault index")')
                STOP 1
             END IF
   
             IF (in%N1 .LT. in%observationState(k)%i1) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid coordinate i1")')
                STOP 1
             END IF
   
             IF (in%N2 .LT. in%observationState(k)%i2) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid coordinate i2")')
                STOP 1
             END IF
   
             IF (0 .GE. in%observationState(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid subsampling rate")')
                STOP 1
             END IF
   
             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF
   
#ifdef NETCDF
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P R O F I L E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation profiles")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationProfile
       PRINT '(I5)', in%nObservationProfile
       IF (0 .LT. in%nObservationProfile) THEN
          ALLOCATE(in%observationProfileVelocity(in%nObservationProfile),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation profiles"
          PRINT 2000
          PRINT '("#   n fault index direction rate")'
          PRINT 2000
          DO k=1,in%nObservationProfile
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationProfileVelocity(k)%fault, &
                     in%observationProfileVelocity(k)%index, &
                     in%observationProfileVelocity(k)%direction, &
                     in%observationProfileVelocity(k)%rate
   
             PRINT '(I5,X,I5,X,I5,X,I9,X,I4)', i, &
                     in%observationProfileVelocity(k)%fault, &
                     in%observationProfileVelocity(k)%index, &
                     in%observationProfileVelocity(k)%direction, &
                     in%observationProfileVelocity(k)%rate
   
             IF (0 .GE. in%observationProfileVelocity(k)%fault .OR. &
                 0 .GE. in%observationProfileVelocity(k)%index .OR. &
                 0 .GE. in%observationProfileVelocity(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid profile declaration")')
                STOP 1
             END IF
   
             IF (in%nFault .LT. in%observationProfileVelocity(k)%fault) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid fault number.")')
                STOP 1
             END IF
   
             IF (1 .NE. in%observationProfileVelocity(k)%direction .AND. &
                 2 .NE. in%observationProfileVelocity(k)%direction) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid profile direction (must be 1 or 2.)")')
                STOP 1
             END IF
   
             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF
#endif
      
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !                  E V E N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of events")'
       CALL getdata(iunit,dataline)
       READ (dataline,*) in%ne
       PRINT '(I6)', in%ne
       IF (in%ne .GT. 0) ALLOCATE(in%event(in%ne),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the event list"
       
       DO i=1,in%ne
       IF (1 .NE. i) THEN
             PRINT '("# time of next event")'
             CALL getdata(iunit,dataline)
             READ (dataline,*) in%event(i)%time
             in%event(i)%i=i-1
             PRINT '(ES9.2E1)', in%event(i)%time
   
             IF (in%event(i)%time .LE. in%event(i-1)%time) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'(a,a)') "input file error. ", &
                     "timing of perturbations must increase, quiting."
                STOP 1
             END IF
          ELSE
             in%event(1)%time=0._8
             in%event(1)%i=0
          END IF
   
       END DO
   
       ! test input file
       IF ((in%N1*in%N2 .LE. 0) .OR. &
           (in%interval .LE. 0._8)) THEN
   
          WRITE_DEBUG_INFO(300)
          WRITE (STDERR,'("nothing to do. exiting.")')
          STOP 1
       END IF
   
       PRINT 2000
       ! flush standard output
       CALL FLUSH(6)      

       ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       ! BROADCAST INPUT PARAMETERS
       ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

       position=0
       CALL MPI_PACK(in%interval,           1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%lambda,             1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%damping,            1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
#ifdef BATH
       CALL MPI_PACK(in%R,                  1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
#endif
       CALL MPI_PACK(in%mu,                 1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nu,                 1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%alpha,              1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%dx1,                1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%dx2,                1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nFault,             1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nRock,              1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%N1,                 1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%N2,                 1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationState,  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationProfile,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationPoint,  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%ne,                 1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       CALL MPI_BCAST(in%wdir,512,MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)

       ! send the rocks
       DO i=1,in%nRock
          position=0
          CALL MPI_PACK(in%rock(i)%nFlow,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%nHealing,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%mu0,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%d0,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%sigma0,  1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%alpha,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%beta,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%lambda,  1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(in%rock(i)%name,80,MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)

          ! send the flow laws
          DO j=1,in%rock(i)%nFlow
             position=0
             CALL MPI_PACK(in%rock(i)%flow(j)%Vo,  1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%flow(j)%c0,  1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%flow(j)%n,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%flow(j)%Q,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%flow(j)%To,  1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%flow(j)%zeta,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO

          ! send the healing terms
          DO j=1,in%rock(i)%nHealing
             position=0
             CALL MPI_PACK(in%rock(i)%healing(j)%f0,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%healing(j)%p, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%healing(j)%q, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%healing(j)%H, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%healing(j)%To,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END DO

       ! send the patches (geometry and friction properties) 
       DO k=1,in%nFault
          DO i2=1,layout%N2L(1+rank)
             DO i1=1,in%N1

                i=((k-1)*in%N2             +i2-1)*in%N1+i1
                l=((k-1)*layout%N2L(1+rank)+i2-1)*in%N1+i1

                in%patch(l)%tau0     =REAL(patchAll(i)%tau0,8)
                in%patch(l)%sig      =REAL(patchAll(i)%sig,8)
                in%patch(l)%h        =REAL(patchAll(i)%h,8)
                in%patch(l)%Vl       =REAL(patchAll(i)%Vl,8)
                in%patch(l)%rake     =REAL(patchAll(i)%rake,8)
#ifdef BATH
                in%patch(l)%wRhoC    =REAL(patchAll(i)%wRhoC,8)
                in%patch(l)%DW2      =REAL(patchAll(i)%DW2,8)
                in%patch(l)%Tb       =REAL(patchAll(i)%Tb,8)
#endif
                in%patch(l)%rockType =patchAll(i)%rockType
                in%patch(l)%dirichlet=patchAll(i)%dirichlet
             END DO
          END DO
       END DO

       ! send the patches (geometry and friction properties) to other threads
       DO j=1,csize-1
          DO k=1,in%nFault
             CALL MPI_SEND(in%fault(k)%x3,1,MPI_REAL8,j,0,MPI_COMM_WORLD,ierr)
             DO i2=1,layout%N2L(j+1)
                DO i1=1,in%N1/2
                   position=0
                   DO l=1,2
                      i=((k-1)*in%N2+layout%i2start(j+1)+i2-1)*in%N1+2*(i1-1)+l
                      CALL MPI_PACK(REAL(patchAll(i)%tau0,8), 1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
                      CALL MPI_PACK(REAL(patchAll(i)%sig,8),  1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
                      CALL MPI_PACK(REAL(patchAll(i)%h,8),    1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
                      CALL MPI_PACK(REAL(patchAll(i)%Vl,8),   1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
                      CALL MPI_PACK(REAL(patchAll(i)%rake,8), 1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
#ifdef BATH
                      CALL MPI_PACK(REAL(patchAll(i)%wRhoC,8),1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
                      CALL MPI_PACK(REAL(patchAll(i)%DW2,8),  1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
                      CALL MPI_PACK(REAL(patchAll(i)%Tb,8),   1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
#endif
                      CALL MPI_PACK(patchAll(i)%rockType,     1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
                      CALL MPI_PACK(patchAll(i)%dirichlet,    1,MPI_LOGICAL,packed,psize,position,MPI_COMM_WORLD,ierr)
                   END DO
                   CALL MPI_SEND(packed,psize,MPI_PACKED,j,0,MPI_COMM_WORLD,ierr)
                END DO
             END DO
          END DO
       END DO

       ! send the observation patches
       DO i=1,in%nObservationState
          position=0
          CALL MPI_PACK(in%observationState(i)%fault,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%observationState(i)%i1   ,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%observationState(i)%i2   ,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%observationState(i)%rate ,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

#ifdef NETCDF
       ! send the observation profiles
       DO i=1,in%nObservationProfile
          position=0
          CALL MPI_PACK(in%observationProfileVelocity(i)%fault,    1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%observationProfileVelocity(i)%index,    1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%observationProfileVelocity(i)%direction,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%observationProfileVelocity(i)%rate,     1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO
#endif

       ! send the observation points
       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_PACK(in%observationPoint(i)%name,10,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_PACK(in%observationPoint(i)%x(k),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the perturbation events
       DO k=1,in%ne
          CALL MPI_PACK(in%event(k)%time,1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%event(k)%i,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! free memory
       DEALLOCATE(patchAll)

    ELSE ! if 0.NE.rank

       !------------------------------------------------------------------
       ! S L A V E S
       !------------------------------------------------------------------

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%interval,           1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%lambda,             1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%damping,            1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
#ifdef BATH
       CALL MPI_UNPACK(packed,psize,position,in%R,                  1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
#endif
       CALL MPI_UNPACK(packed,psize,position,in%mu,                 1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nu,                 1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%alpha,              1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%dx1,                1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%dx2,                1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nFault,             1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nRock,              1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%N1,                 1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%N2,                 1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationState,  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationProfile,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationPoint,  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%ne,                 1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

       CALL MPI_BCAST(in%wdir,512,MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)

       ! setup data layout for threads
       CALL initParallelism()

       ALLOCATE(in%rock(in%nRock),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for rocks"

       ! receive the rocks
       DO k=1,in%nRock
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%nFlow,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%nHealing,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%mu0,     1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%d0,      1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%sigma0,  1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%alpha,   1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%beta,    1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%lambda,  1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(in%rock(k)%name,80,MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)

          ! flow laws
          ALLOCATE(in%rock(k)%flow(in%rock(k)%nFlow),STAT=ierr)
          IF (ierr>0) STOP "slave could not allocate memory for flow laws"

          ! receive the flow laws
          DO j=1,in%rock(k)%nFlow
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%Vo,    1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%c0,    1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%n,     1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%Q,     1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%To,    1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%zeta,  1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          END DO

          ! healing terms
          ALLOCATE(in%rock(k)%healing(in%rock(k)%nHealing),STAT=ierr)
          IF (ierr>0) STOP "slave could not allocate memory for healing terms"

          ! receive the healing terms
          DO j=1,in%rock(k)%nHealing
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%healing(j)%f0, 1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%healing(j)%p,  1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%healing(j)%q,  1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%healing(j)%H,  1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%healing(j)%To, 1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          END DO
       END DO

       ALLOCATE(in%patch(in%nFault*in%N1*layout%N2L(1+rank)),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for patches"

       ALLOCATE(in%fault(in%nFault),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for faults"

       DO k=1,in%nFault
          CALL MPI_RECV(in%fault(k)%x3,1,MPI_REAL8,0,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
          DO i2=1,layout%N2L(1+rank)
             DO i1=1,in%N1/2
                position=0
                CALL MPI_RECV(packed,psize,MPI_PACKED,0,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
                DO l=1,2
                   i=((k-1)*layout%N2L(1+rank)+i2-1)*in%N1+(i1-1)*2+l
                   CALL MPI_UNPACK(packed,psize,position,in%patch(i)%tau0,     1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
                   CALL MPI_UNPACK(packed,psize,position,in%patch(i)%sig,      1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
                   CALL MPI_UNPACK(packed,psize,position,in%patch(i)%h,        1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
                   CALL MPI_UNPACK(packed,psize,position,in%patch(i)%Vl,       1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
                   CALL MPI_UNPACK(packed,psize,position,in%patch(i)%rake,     1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
#ifdef BATH
                   CALL MPI_UNPACK(packed,psize,position,in%patch(i)%wRhoC,    1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
                   CALL MPI_UNPACK(packed,psize,position,in%patch(i)%DW2,      1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
                   CALL MPI_UNPACK(packed,psize,position,in%patch(i)%Tb,       1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
#endif
                   CALL MPI_UNPACK(packed,psize,position,in%patch(i)%rockType, 1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
                   CALL MPI_UNPACK(packed,psize,position,in%patch(i)%dirichlet,1,MPI_LOGICAL,MPI_COMM_WORLD,ierr)
                END DO
             END DO
          END DO
       END DO

       IF (0 .LT. in%nObservationState) &
                    ALLOCATE(in%observationState(in%nObservationState),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation states"

       DO i=1,in%nObservationState
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(i)%fault,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(i)%i1,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(i)%i2,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(i)%rate, 1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

#ifdef NETCDF
       ALLOCATE(in%observationProfileVelocity(in%nObservationProfile),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation profiles"

       DO i=1,in%nObservationProfile
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationProfileVelocity(i)%fault,    1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationProfileVelocity(i)%index,    1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationProfileVelocity(i)%direction,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationProfileVelocity(i)%rate,     1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO
#endif

       IF (0 .LT. in%nObservationPoint) &
                    ALLOCATE(in%observationPoint(in%nObservationPoint), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation points"

       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%name,10,MPI_CHARACTER,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%x(k),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END DO

       IF (0 .LT. in%ne) ALLOCATE(in%event(in%ne),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       DO i=1,in%ne
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%event(i)%time,1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%event(i)%i,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       CALL FLUSH(6)      

    END IF

    ! find owner of observation patches
    DO i=1,in%nObservationState
       DO j=0,csize-1
          IF ((in%observationState(i)%i2 .GT. layout%i2start(1+j)) .AND. &
              (in%observationState(i)%i2 .LE. layout%i2start(1+j)+layout%N2L(1+j))) THEN
             in%observationState(i)%rank=j
             in%observationState(i)%i2l=in%observationState(i)%i2-layout%i2start(1+j)
          END IF
       END DO
    END DO

#ifdef NETCDF
    ! find owner of horizontal and vertical profiles
    DO i=1,in%nObservationProfile
       IF (2 .EQ. in%observationProfileVelocity(i)%direction) THEN
          ! vertical profiles
          in%observationProfileVelocity(i)%rank=0
       ELSE
          DO j=0,csize-1
             IF ((in%observationProfileVelocity(i)%index .GT. layout%i2start(1+j)) .AND. &
                 (in%observationProfileVelocity(i)%index .LE. layout%i2start(1+j)+layout%N2L(1+j))) THEN
                in%observationProfileVelocity(i)%rank=j
                in%observationProfileVelocity(i)%i2l=in%observationProfileVelocity(i)%index-layout%i2start(1+j)
             END IF
          END DO
       END IF
    END DO

#ifdef BATH
    ! temperature profiles overlap velocity profiles
    ALLOCATE(in%observationProfileTemperature(in%nObservationProfile),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the temperature profiles"
    DO i=1,in%nObservationProfile
       in%observationProfileTemperature(i)%fault    =in%observationProfileVelocity(i)%fault
       in%observationProfileTemperature(i)%index    =in%observationProfileVelocity(i)%index
       in%observationProfileTemperature(i)%direction=in%observationProfileVelocity(i)%direction
       in%observationProfileTemperature(i)%rate     =in%observationProfileVelocity(i)%rate
       in%observationProfileTemperature(i)%rank     =in%observationProfileVelocity(i)%rank
       in%observationProfileTemperature(i)%i2l      =in%observationProfileVelocity(i)%i2l
    END DO
#endif
#endif

2000 FORMAT ("# ----------------------------------------------------------------------------------------------")
   
  END SUBROUTINE init
   
  !-----------------------------------------------
  !> subroutine cumsum1
  !! cumulative sum offset by 1
  !-----------------------------------------------
  SUBROUTINE CUMSUM1(n,in,out)
    INTEGER, INTENT(IN) :: n
    INTEGER, INTENT(IN), DIMENSION(n) :: in
    INTEGER, INTENT(OUT), DIMENSION(n) :: out

    INTEGER :: i

    out(1)=0
    DO i=2,n
       out(i)=in(i-1)+out(i-1)
    END DO

  END SUBROUTINE CUMSUM1

  !-----------------------------------------------
  !> function findRoot2
  !! finds the stress that satisfies
  !!
  !!   V = A1 tau^n1 + A2 tau^n2
  !-----------------------------------------------
  REAL*8 FUNCTION findRoot2(A1,n1,A2,n2,V)
    REAL*8, INTENT(IN) :: A1,n1,A2,n2
    REAL*8, INTENT(IN) :: V

    REAL*8 :: V1,V2
    REAL*8 :: tau0,tau1,tau2
    REAL*8, DIMENSION(10) :: extras
    REAL*8, EXTERNAL :: rootbisection

    ! first guess
    tau1=(V/A1)**(1/n1);
    tau2=(V/A2)**(1/n2);
    tau0=2/(1/tau1+1/tau2);

    ! refinement
    V1=A1*tau0**n1;
    V2=A2*tau0**n2;
    tau0=(V1+V2)/(V1/tau1+V2/tau2);

    ! numerical solution
    extras(1:5)=(/ A1,n1,A2,n2,V /)
    findRoot2=rootbisection(polynomial2,extras,tau0*0.80d0,tau0*1.20d0,1d-8);

  END FUNCTION findRoot2

  REAL*8 FUNCTION polynomial2(x,extras)
     IMPLICIT NONE
     REAL*8, INTENT(IN) :: x
     REAL*8, INTENT(IN), DIMENSION(10) :: extras

     polynomial2=extras(1)*x**extras(2)+extras(3)*x**extras(4)-extras(5)
  END FUNCTION polynomial2

  !-----------------------------------------------
  !> function findRoot3
  !! finds the stress that satisfies
  !!
  !!   V = A1 tau^n1 + A2 tau^n2 + A3 tau^n3
  !-----------------------------------------------
  REAL*8 FUNCTION findRoot3(A1,n1,A2,n2,A3,n3,V)
    REAL*8, INTENT(IN) :: A1,n1,A2,n2,A3,n3
    REAL*8, INTENT(IN) :: V

    REAL*8 :: V1,V2,V3
    REAL*8 :: tau0,tau1,tau2,tau3
    REAL*8, DIMENSION(10) :: extras
    REAL*8, EXTERNAL :: rootbisection

    ! first guess
    tau1=(V/A1)**(1/n1);
    tau2=(V/A2)**(1/n2);
    tau3=(V/A3)**(1/n3);
    tau0=3/(1/tau1+1/tau2+1/tau3);

    ! refinement
    V1=A1*tau0**n1;
    V2=A2*tau0**n2;
    V3=A3*tau0**n3;
    tau0=(V1+V2+V3)/(V1/tau1+V2/tau2+V3/tau3);

    ! numerical solution
    extras(1:7)=(/ A1,n1,A2,n2,A3,n3,V /)
    findRoot3=rootbisection(polynomial3,extras,tau0*0.70,tau0*1.30,1d-8)

  END FUNCTION findRoot3

  REAL*8 FUNCTION polynomial3(x,extras)
     IMPLICIT NONE
     REAL*8, INTENT(IN) :: x
     REAL*8, INTENT(IN), DIMENSION(10) :: extras

     polynomial3=extras(1)*x**extras(2)+extras(3)*x**extras(4) &
                +extras(5)*x**extras(6)-extras(7)
  END FUNCTION polynomial3

  SUBROUTINE printRock()
       INTEGER :: i,j,k

       PRINT '("# thread number")'
       PRINT '(I2)', rank

       ! number of rocks
       PRINT '("# number of rocks")'
       PRINT '(2I2)', in%nRock

       DO j=1,in%nRock
          PRINT '("# rock index, rock name")'
          PRINT '(I2,X,a)', j, TRIM(in%rock(j)%name)

          PRINT '("#     mu0        d0    sigma0     alpha      beta")'
          PRINT '(F9.3,4ES10.2E1)', &
               in%rock(j)%mu0, &
               in%rock(j)%d0, &
               in%rock(j)%sigma0, &
               in%rock(j)%alpha, &
               in%rock(j)%beta

          PRINT '("# number of flow laws for rock ",I2)',j
          PRINT '(I2)', in%rock(j)%nFlow

          PRINT '("#  n        Vo        c0         n         Q        To      zeta")'
          DO k=1,in%rock(j)%nFlow
             PRINT '(I4,6ES10.4E1)', i, &
                  in%rock(j)%flow(k)%Vo, &
                  in%rock(j)%flow(k)%c0, &
                  in%rock(j)%flow(k)%n, &
                  in%rock(j)%flow(k)%Q, &
                  in%rock(j)%flow(k)%To, &
                  in%rock(j)%flow(k)%zeta
          END DO
          PRINT '("# number of healing terms for rock ",I2)',j
          PRINT '(I2)', in%rock(j)%nHealing

          PRINT '("#  n        fo         p         q         H        To")'
          DO k=1,in%rock(j)%nHealing
             PRINT '(I4,5ES10.4E1)', i, &
                  in%rock(j)%healing(k)%f0, &
                  in%rock(j)%healing(k)%p, &
                  in%rock(j)%healing(k)%q, &
                  in%rock(j)%healing(k)%H, &
                  in%rock(j)%healing(k)%To
          END DO

          PRINT '("# reciprocal of characteristic strain for weakening (lambda)")'
          PRINT '(2ES9.2E1)', in%rock(j)%lambda

       END DO ! number of rocks
   
  END SUBROUTINE printRock

  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message.
  !-----------------------------------------------
  SUBROUTINE printhelp()

    IF (0.EQ.rank) THEN
       PRINT '("usage:")'
       PRINT '("")'
       PRINT '("mpirun -n 2 motorcycle-3d-thermobaric [-h] [--dry-run] [--help] [--epsilon 1e-6] [filename]")'
       PRINT '("                                      [--export-netcdf] [--export-stress] [--source-export-rate 50]")'
       PRINT '("")'
       PRINT '("options:")'
       PRINT '("   -h                      prints this message and aborts calculation")'
       PRINT '("   --dry-run               abort calculation, only output geometry")'
       PRINT '("   --help                  prints this message and aborts calculation")'
       PRINT '("   --version               print version number and exit")'
       PRINT '("   --epsilon               set the numerical accuracy [1E-6]")'
       PRINT '("   --export-netcdf         export information to .grd netcdf files")'
       PRINT '("   --export-stress         export stress components s13 and s23")'
       PRINT '("   --export-temperature    export temperature")'
       PRINT '("   --export-state          export the state vector periodically to facilitate restart")'
       PRINT '("   --import-state wdir     import the state vector from a previous simulation")'
       PRINT '("   --evolution-law         type of friction law [1]")'
       PRINT '("       1: aging-law end-member evolution law")'
       PRINT '("       2: slip-law end-member evolution law")'
       PRINT '("   --grd-input             input physical parameters with GMT-compatible .grd files")'
       PRINT '("   --source-export-rate r   set the rate of snapshots [50]")'
       PRINT '("   --maximum-iterations    set the maximum time step [1000000]")'
       PRINT '("   --maximum-step          set the maximum time step [none]")'
       PRINT '("")'
       PRINT '("description:")'
       PRINT '("   simulates elasto-dynamics on faults in three dimensions")'
       PRINT '("   following the radiation-damping approximation")'
       PRINT '("   using the integral method.")'
       PRINT '("")'
       PRINT '("see also: ""man motorcycle""")'
       PRINT '("")'
       PRINT '("                ,      ")'
       PRINT '("             .-/c-.,:: ")'
       PRINT '("             (_)''==(_) ")'
       PRINT '("")'
       CALL FLUSH(6)
    END IF

  END SUBROUTINE printhelp

  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version.
  !-----------------------------------------------
  SUBROUTINE printversion()

    IF (0.EQ.rank) THEN
       PRINT '("motorcycle-3d-thermobaric version 1.0.0, compiled on ",a)', __DATE__
       PRINT '("")'
       CALL FLUSH(6)
    END IF

  END SUBROUTINE printversion

END PROGRAM thermobaric3d

