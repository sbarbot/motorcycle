  !-----------------------------------------------
  !> function rootbisection
  !! Using bisection, find the root of a function 
  !! func known to lie between x1 and x2. The root
  !! will be refined until its accuracy is ±xacc.
  !!
  !! Parameter: MAXIT is the maximum allowed number
  !!            of bisections.
  !-----------------------------------------------
  REAL*8 FUNCTION rootbisection(func,extras,x1,x2,xacc)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x1,x2,xacc
    REAL*8, INTENT(IN), DIMENSION(10) :: extras
  
    INTERFACE
      FUNCTION func(x,extras)
        IMPLICIT NONE
        REAL*8, INTENT(IN) :: x
        REAL*8, DIMENSION(10), INTENT(IN) :: extras
        REAL*8 :: func
      END FUNCTION func
    END INTERFACE

    ! maximum number of bisections
    INTEGER, PARAMETER :: MAXIT=40

    INTEGER :: j
    REAL*8 :: dx,f,fmid,xmid

    fmid=func(x2,extras)
    f=func(x1,extras)
    IF (f*fmid >= 0.0) STOP "rootbisection:root must be bracketed"

    ! orient the search so that f>0 lies at x+dx.
    IF (f < 0.0) THEN 
       rootbisection=x1
       dx=x2-x1
    ELSE
       rootbisection=x2
       dx=x1-x2
    END IF

    ! bisection loop
    DO j=1,MAXIT
       dx=dx*0.5_8
       xmid=rootbisection+dx
       fmid=func(xmid,extras)
       IF (fmid <= 0.0) rootbisection=xmid
       IF (abs(dx) < xacc .OR. fmid == 0.0) RETURN
    END DO
    STOP "rootbisection:too many bisections"
  END FUNCTION rootbisection

