!-----------------------------------------------------------------------
! Copyright 2020-2025 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.h90"

#define BATH 1

MODULE types_3d_thermobaric

  USE mpi_f08

  IMPLICIT NONE

#ifdef THERMOBARIC
  TYPE FLOW_STRUCT
      SEQUENCE
      REAL*8 :: Vo,c0,n,Q,To,zeta
  END TYPE FLOW_STRUCT

  TYPE HEALING_STRUCT
      SEQUENCE
      REAL*8 :: f0,p,q,H,To
  END TYPE HEALING_STRUCT

  TYPE ROCK_STRUCT
      SEQUENCE
      REAL*8 :: mu0,d0,sigma0,alpha,beta,lambda
      INTEGER :: nFlow,nHealing
      TYPE(FLOW_STRUCT), DIMENSION(:), ALLOCATABLE :: flow
      TYPE(HEALING_STRUCT), DIMENSION(:), ALLOCATABLE :: healing
      CHARACTER(80) :: name
  END TYPE ROCK_STRUCT
#endif

  TYPE PATCH_ELEMENT_STRUCT4
     SEQUENCE
     REAL*4 :: tau0,sig,h
     REAL*4 :: Vl,rake
#ifdef BATH
     ! heat equation coefficients
     REAL*4 :: wRhoC,DW2,Tb
#endif
#ifdef THERMOBARIC
     INTEGER :: rockType
#endif
     LOGICAL :: dirichlet
  END TYPE PATCH_ELEMENT_STRUCT4

  TYPE PATCH_ELEMENT_STRUCT
     SEQUENCE
     REAL*8 :: tau0,sig,h
     REAL*8 :: Vl,rake
#ifdef BATH
     ! heat equation coefficients
     REAL*8 :: wRhoC,DW2,Tb
#endif
#ifdef THERMOBARIC
     INTEGER :: rockType
#endif
     LOGICAL :: dirichlet
  END TYPE PATCH_ELEMENT_STRUCT

  TYPE EVENT_STRUCT
     REAL*8 :: time
     INTEGER*4 :: i
  END TYPE EVENT_STRUCT
  
  TYPE FAULT_STRUCT
     ! offset relative to reference fault
     REAL*8 :: x3
     ! slip components (rows)
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: s1r,s2r
     ! slip components (columns)
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: s1c,s2c
     ! stress components (rows)
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: s13r,s23r,s33r
     ! stress components (columns)
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: s13c,s23c,s33c
  END TYPE FAULT_STRUCT

  TYPE OBSERVATION_STATE_STRUCT
     ! fault index
     INTEGER :: fault
     ! coordinates
     INTEGER :: i1,i2
     ! sampling rate
     INTEGER :: rate
     ! file number
     INTEGER :: id
     ! owner rank
     INTEGER :: rank
     ! local index
     INTEGER :: i2l
  END TYPE OBSERVATION_STATE_STRUCT
  
  TYPE OBSERVATION_POINT_STRUCT
     SEQUENCE
     INTEGER :: file
     REAL*8, DIMENSION(3) :: x
     CHARACTER(LEN=10) :: name
  END TYPE OBSERVATION_POINT_STRUCT

#ifdef NETCDF
  TYPE PROFILE_STRUCT
     SEQUENCE
     INTEGER :: fault,index,direction,rate
     ! netcdf file
     INTEGER :: ncid,y_varid,z_varid,ncCount
     ! owner rank
     INTEGER :: rank
     ! local index
     INTEGER :: i2l
     ! filename
     CHARACTER(512) :: filename
  END TYPE PROFILE_STRUCT
#endif

  TYPE :: SIMULATION_STRUCT

     ! elastic moduli
     REAL*8 :: lambda,mu,nu,alpha

#ifdef THERMOBARIC
     ! radiation damping term
     REAL*8 :: damping
#endif

#ifdef BATH
     ! universal gas constant
     REAL*8 :: R
#endif

#ifdef THERMOBARIC
     ! number of rock types
     INTEGER :: nRock

     ! list of rock types
     TYPE(ROCK_STRUCT), DIMENSION(:), ALLOCATABLE :: rock
#endif

     ! simulation time
     REAL*8 :: interval

     ! fault dimension
     INTEGER :: N1,N2

     ! sampling size
     REAL*8 :: dx1,dx2

     ! number of fault patches
     INTEGER :: nPatch

     ! patches
     TYPE(PATCH_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: patch

     ! number of parallel faults
     INTEGER :: nFault

     ! fault structures
     TYPE(FAULT_STRUCT), DIMENSION(:), ALLOCATABLE :: fault

     ! output directory
     CHARACTER(512) :: wdir

     ! filenames
     CHARACTER(256) :: timeFilename

     ! number of observation states
     INTEGER :: nObservationState

     ! observation state (patches and volumes)
     TYPE(OBSERVATION_STATE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationState

#ifdef NETCDF
     ! observation profiles
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationProfileVelocity
#ifdef BATH
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationProfileTemperature
#endif
#endif

     ! number of observation profiles
     INTEGER :: nObservationProfile

     ! number of observation points
     INTEGER :: nObservationPoint

     ! observation points
     TYPE(OBSERVATION_POINT_STRUCT), DIMENSION(:), ALLOCATABLE :: observationPoint

     ! number of perturbation events
     INTEGER :: ne

     ! perturbation events
     TYPE(EVENT_STRUCT), DIMENSION(:), ALLOCATABLE :: event

     ! other options
     LOGICAL :: isdryrun=.FALSE.
     LOGICAL :: isExportNetcdf=.FALSE.
     LOGICAL :: isExportSlip=.FALSE.
     LOGICAL :: isExportStress=.FALSE.
     LOGICAL :: isImportState=.FALSE.
     LOGICAL :: isExportState=.FALSE.
     LOGICAL :: isGrdInput=.FALSE.
#ifdef BATH
     LOGICAL :: isExportTemperature=.FALSE.
#endif
     LOGICAL :: ishelp=.FALSE.
     LOGICAL :: isversion=.FALSE.
     INTEGER :: sourceExportRate=100

  END TYPE SIMULATION_STRUCT

  TYPE LAYOUT_STRUCT

     ! list of number of elements in threads
     INTEGER, DIMENSION(:), ALLOCATABLE :: N1L,N2L

     ! list of start index in threads
     INTEGER, DIMENSION(:), ALLOCATABLE :: i1start,i2start

     ! size and offsets for MPI_IGATHERV
     INTEGER, DIMENSION(:), ALLOCATABLE :: rcounts, displs

     ! mapping functions
     TYPE(MPI_DATATYPE), DIMENSION(:,:), ALLOCATABLE :: vector

     ! layout for vertical profile
     TYPE(MPI_DATATYPE) :: profileVector

  END TYPE LAYOUT_STRUCT

END MODULE types_3d_thermobaric

