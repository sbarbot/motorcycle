!-----------------------------------------------------------------------
! Copyright 2007-2023 Sylvain Barbot
!
! This file is part of MOTORCYCLE
!
! MOTORCYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! MOTORCYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with MOTORCYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

MODULE fft2d_mpi_bath

  USE mpi_f08
  USE types_3d_bath

  IMPLICIT NONE

  PRIVATE

  INCLUDE 'fftw3.f'

  PUBLIC :: fftInit
  PUBLIC :: fft1,ifft1
  PUBLIC :: fft2,ifft2

CONTAINS

  !----------------------------------------------------------------------
  !> subroutine FFTINIT builds wisdom for future DFT execution.
  !----------------------------------------------------------------------
  SUBROUTINE fftInit(r1,r2,row,c1,c2,column)
    INTEGER, INTENT(IN) :: r1,r2
    REAL*8, DIMENSION(r1+2,r2), INTENT(INOUT) :: row
    INTEGER, INTENT(IN) :: c1,c2
    REAL*8, DIMENSION(c1,c2), INTENT(OUT) :: column

    INTEGER*8 :: plan

    INTEGER, PARAMETER :: dimension=1
    INTEGER :: idist,odist,stride,howmany

    ! real to complex discrete Fourier transform rows
    stride=1
    idist=r1+2
    odist=r1/2+1
    howmany=r2
    CALL dfftw_plan_many_dft_r2c(plan,dimension,r1,howmany, &
                                row(1,1),r1,stride,idist, &
                                row(1,1),r1,stride,odist,FFTW_MEASURE)
    CALL dfftw_destroy_plan(plan)

    ! complex to complex forward discrete Fourier transform columns
    stride=c1/2
    idist=1
    odist=1
    howmany=c1/2
    CALL dfftw_plan_many_dft(plan,dimension,c2,howmany, &
                             column(1,1),c2,stride,idist, &
                             column(1,1),c2,stride,odist, &
                             FFTW_FORWARD,FFTW_MEASURE)
    CALL dfftw_destroy_plan(plan)

    ! complex to complex discrete inverse Fourier transform columns
    stride=c1/2
    idist=1
    odist=1
    howmany=c1/2
    CALL dfftw_plan_many_dft(plan,dimension,c2,howmany, &
                             column(1,1),c2,stride,idist, &
                             column(1,1),c2,stride,odist, &
                             FFTW_BACKWARD,FFTW_MEASURE)
    CALL dfftw_destroy_plan(plan)

    ! complex to real discrete Fourier transform columns
    stride=1
    idist=r1/2+1
    odist=r1+2
    howmany=r2
    CALL dfftw_plan_many_dft_c2r(plan,dimension,r1,howmany, &
                                row(1,1),r1,stride,idist, &
                                row(1,1),r1,stride,odist,FFTW_MEASURE)
    CALL dfftw_destroy_plan(plan)

  END SUBROUTINE fftInit

  !----------------------------------------------------------------------
  !> subroutine FFT1 performs normalized forward fourier transform.
  !!
  !! for real array the fourier transform returns a sx1/2+1 complex array
  !! and enough space must be reserved.
  !----------------------------------------------------------------------
  SUBROUTINE fft1(r1,r2,row)
    INTEGER, INTENT(IN) :: r1,r2
    REAL*8, DIMENSION(r1+2,r2), INTENT(INOUT) :: row

    INTEGER*8 :: plan

    INTEGER, PARAMETER :: dimension=1
    INTEGER :: idist,odist,stride,howmany

    ! real to complex discrete Fourier transform rows
    stride=1
    idist=r1+2
    odist=r1/2+1
    howmany=r2
    CALL dfftw_plan_many_dft_r2c(plan,dimension,r1,howmany, &
                                row(1,1),r1,stride,idist, &
                                row(1,1),r1,stride,odist,FFTW_MEASURE)
    CALL dfftw_execute_dft_r2c(plan,row(1,1),row(1,1))
    CALL dfftw_destroy_plan(plan)

  END SUBROUTINE fft1
    
  !----------------------------------------------------------------------
  !> subroutine iFFT1 performs normalized forward fourier transform.
  !!
  !! for real array the fourier transform returns a sx1/2+1 complex array
  !! and enough space must be reserved.
  !----------------------------------------------------------------------
  SUBROUTINE ifft1(r1,r2,row)
    INTEGER, INTENT(IN) :: r1,r2
    REAL*8, DIMENSION(r1+2,r2), INTENT(INOUT) :: row

    INTEGER*8 :: plan

    INTEGER, PARAMETER :: dimension=1
    INTEGER :: idist,odist,stride,howmany

    ! complex to real discrete Fourier transform columns
    stride=1
    idist=r1/2+1
    odist=r1+2
    howmany=r2
    CALL dfftw_plan_many_dft_c2r(plan,dimension,r1,howmany, &
                                row(1,1),r1,stride,idist, &
                                row(1,1),r1,stride,odist,FFTW_MEASURE)
    CALL dfftw_execute_dft_c2r(plan,row(1,1),row(1,1))
    CALL dfftw_destroy_plan(plan)
    row=row/r1

  END SUBROUTINE ifft1
    
  !----------------------------------------------------------------------
  !> subroutine FFT2 performs normalized forward fourier transform.
  !!
  !! for real array the fourier transform returns a sx1/2+1 complex array
  !! and enough space must be reserved.
  !----------------------------------------------------------------------
  SUBROUTINE fft2(r1,r2,row,c1,c2,column,layout)
    INTEGER, INTENT(IN) :: r1,r2
    REAL*8, DIMENSION(r1+2,r2), INTENT(INOUT) :: row
    INTEGER, INTENT(IN) :: c1,c2
    REAL*8, DIMENSION(c1,c2), INTENT(OUT) :: column
    TYPE(LAYOUT_STRUCT), INTENT(INOUT) :: layout

    INTEGER :: rank,csize,ierr

    INTEGER*8 :: plan
    INTEGER :: i

    INTEGER, PARAMETER :: dimension=1
    INTEGER :: idist,odist,stride,howmany

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! real to complex discrete Fourier transform rows
    stride=1
    idist=r1+2
    odist=r1/2+1
    howmany=r2
    CALL dfftw_plan_many_dft_r2c(plan,dimension,r1,howmany, &
                                row(1,1),r1,stride,idist, &
                                row(1,1),r1,stride,odist,FFTW_MEASURE)
    CALL dfftw_execute_dft_r2c(plan,row(1,1),row(1,1))
    CALL dfftw_destroy_plan(plan)

    ! transfer complex data from rows to columns
    DO i=0,csize-1
       layout%rcounts=layout%N1L(rank+1)*layout%N2L
       layout%displs=layout%i2start*layout%N1L(rank+1)
     
       CALL MPI_GATHERV( &
               row(layout%i1start(1+i)+1,1),1,layout%vector(i+1,1+rank), &
               column(1,1),layout%rcounts,layout%displs,MPI_REAL8, &
               i,MPI_COMM_WORLD,ierr)
    END DO
    
    ! complex to complex forward discrete Fourier transform columns
    stride=c1/2
    idist=1
    odist=1
    howmany=c1/2
    CALL dfftw_plan_many_dft(plan,dimension,c2,howmany, &
                             column(1,1),c2,stride,idist, &
                             column(1,1),c2,stride,odist, &
                             FFTW_FORWARD,FFTW_MEASURE)
    CALL dfftw_execute_dft(plan,column(1,1),column(1,1))
    CALL dfftw_destroy_plan(plan)

  END SUBROUTINE fft2

  !----------------------------------------------------------------------
  !> subroutine iFFT2 performs normalized inverse fourier transform of 
  !! real 2d data.
  !!
  !! for real array the fourier transform returns a sx1/2+1 complex array
  !! and enough space must be reserved.
  !----------------------------------------------------------------------
  SUBROUTINE ifft2(c1,c2,column,r1,r2,row,layout)
    INTEGER, INTENT(IN) :: c1,c2
    REAL*8, DIMENSION(c1,c2), INTENT(OUT) :: column
    INTEGER, INTENT(IN) :: r1,r2
    REAL*8, DIMENSION(r1+2,r2), INTENT(INOUT) :: row
    TYPE(LAYOUT_STRUCT), INTENT(INOUT) :: layout

    INTEGER :: rank,csize,ierr

    INTEGER*8 :: plan
    INTEGER :: i

    INTEGER, PARAMETER :: dimension=1
    INTEGER :: idist,odist,stride,howmany

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! complex to complex discrete inverse Fourier transform columns
    stride=c1/2
    idist=1
    odist=1
    howmany=c1/2
    CALL dfftw_plan_many_dft(plan,dimension,c2,howmany, &
                             column(1,1),c2,stride,idist, &
                             column(1,1),c2,stride,odist, &
                             FFTW_BACKWARD,FFTW_MEASURE)
    CALL dfftw_execute_dft(plan,column(1,1),column(1,1))
    CALL dfftw_destroy_plan(plan)

    ! transfer complex data from columns to rows
    DO i=0,csize-1
       layout%rcounts=layout%N1L(rank+1)*layout%N2L
       layout%displs=layout%i2start*layout%N1L(rank+1)
       
       CALL MPI_SCATTERV( &
               column(1,1),layout%rcounts,layout%displs,MPI_REAL8, &
               row(layout%i1start(1+i)+1,1),1,layout%vector(i+1,1+rank), &
               i,MPI_COMM_WORLD,ierr)
    END DO

    ! complex to real discrete Fourier transform columns
    stride=1
    idist=r1/2+1
    odist=r1+2
    howmany=r2
    CALL dfftw_plan_many_dft_c2r(plan,dimension,r1,howmany, &
                                row(1,1),r1,stride,idist, &
                                row(1,1),r1,stride,odist,FFTW_MEASURE)
    CALL dfftw_execute_dft_c2r(plan,row(1,1),row(1,1))
    CALL dfftw_destroy_plan(plan)
    row=row/(r1*c2)

  END SUBROUTINE ifft2

END MODULE fft2d_mpi_bath
