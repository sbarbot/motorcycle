
OBJRS=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_3d.o getdata.o greens_3d.o fft2d_mpi.o rk_mpi.o ratestate.o )

OBJTH=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_3d_bath.o getdata.o greens_3d_bath.o fft2d_mpi_bath.o rk_mpi.o ratestate_bath.o )

OBJTB=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_3d_thermobaric.o getdata.o greens_3d_thermobaric.o fft2d_mpi_thermobaric.o rootbisection.o rk_mpi.o thermobaric3d.o )

LIB=$(patsubst %,$(LIBDST)/%, \
      getopt_m.o exportnetcdf.o )

$(shell mkdir -p $(DST))
$(shell mkdir -p $(LIBDST))

$(DST)/%.o:$(SRC)/%.c
	$(COMPILE.c) $(CFLAGS) $^ -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f $(SRC)/macros.h90
	$(COMPILE.f) $(F77FLAGS) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f90 $(SRC)/macros.h90
	$(COMPILE.f) $(F90FLAGS) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o -J $(DST)

$(DST)/%.o: $(SRC)/%.cpp
	$(COMPILE.f) $(CXXFLAGS) $^ -o $(DST)/$*.o
      
$(LIBDST)/%.o: $(LIBSRC)/%.f90
	$(COMPILE.f) $^ -o $(LIBDST)/$*.o -J $(LIBDST)

all: lib motorcycle-3d-ratestate motorcycle-3d-ratestate-bath motorcycle-3d-thermobaric

motorcycle-3d-ratestate: $(filter-out $(SRC)/macros.h90,$(OBJRS)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

motorcycle-3d-ratestate-bath: $(filter-out $(SRC)/macros.h90,$(OBJTH)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

motorcycle-3d-thermobaric: $(filter-out $(SRC)/macros.h90,$(OBJTB)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

lib: $(LIB)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:

