
clear all

wdir='output-bp4-1000-L120-W80-contour';
[time,~,~,~]=textread(sprintf('%s/time.dat',wdir),'%f %f %f %f','commentstyle','shell');
files=dir(sprintf('%s/fault-01-index-*-log10v.grd',wdir));

[x,y,z1]=unicycle.export.grdread([files(1).folder '/' files(1).name]);
x=repmat(x,length(y),1);
y=repmat(y',1,length(x));
chrons=zeros(size(z1));
mask=zeros(size(z1));

for i=1:length(files)-30
    index=str2num(files(i).name(16:22));
    [~,~,z]=unicycle.export.grdread([files(i).folder '/' files(i).name]);
    z=10.^z;
    pos=1e-3<=z & 0==mask;
    chrons(pos)=time(index);
    mask(pos)=1;
end

%%
figure(1);clf;
hold on
pcolor(x,y,chrons), shading flat
contour(x,y,chrons,20:100,'k');
set(gca,'YDir','Reverse')
colorbar
axis equal
set(gca,'xlim',[-1 1]*40e3,'ylim',[-1 1]*20e3)

%%

pos=abs(x)<=40e3 & abs(y)<=20e3;

fid=fopen(sprintf('%s/rupture.dat',wdir),'wt');
fprintf(fid,'# problem=SEAS Benchmark No.4\n');
fprintf(fid,'# author=S. Barbot\n');
fprintf(fid,'# code=Motorcycle\n');
fprintf(fid,'x2 x3 t\n');
fprintf(fid,'%f %f %f\n',[x(pos),y(pos),chrons(pos)]');
fclose(fid);