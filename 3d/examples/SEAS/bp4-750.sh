#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Benchmark problem BP4 is for a three-dimensional (3D) extension of BP1 to a problem in
# a whole-space (quasi-dynamic approximation is still assumed), although some parameters
# are changed to make the computations more feasible. The model size, resolution, initial and
# boundary conditions, and model output are designed specically for 3D problems.

selfdir=$(dirname $0)

WDIR=$selfdir/output-bp4-750
FLT=$WDIR/flt.3d

GDIR=$selfdir/greens-16-750

if [ ! -e $GDIR ]; then
	echo adding directory $GDIR
	mkdir $GDIR
fi

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

NY=160
NZ=108
DX=750

VER=`echo "" | awk -v nw=$NZ -v nl=$NY \
	'BEGIN{i=nl/2}{
		for (j=1;j<=nw;j++){ 
			printf "%d \n",i+(j-1)*nl
		}
	}'`

HOR=`echo "" | awk -v nl=$NY -v nw=$NZ \
	'BEGIN{j=nw/2}{
		for (i=1;i<=nl;i++){ 
			printf "%d\n",i+(j-1)*nl
		};
	}'`

ALL=`echo "" | awk -v nl=$NY -v nw=$NZ \
	'BEGIN{skip=4}{
		for (j=1;j<=nw;j=j+skip){ 
			for (i=1;i<=nl;i=i+skip){ 
				printf "%d\n",i+(j-1)*nl
			};
		};
	}'`

# observation patches
OPT=`cat <<EOF
-36.0e3 +0.00e3
-22.5e3 -7.50e3
-16.5e3 -12.0e3
-16.5e3 +0.00e3
-16.5e3 +12.0e3
+0.00e3 -21.0e3
+0.00e3 -12.0e3
+0.00e3 +0.00e3
+0.00e3 +12.0e3
+0.00e3 +21.0e3
+16.5e3 -12.0e3
+16.5e3 +0.00e3
+16.5e3 +12.0e3
+36.0e3 +0.00e3
EOF`

echo "" | awk -v nl=$NY -v nw=$NZ -v dx=$DX 'BEGIN{k=1;pi=atan2(1.0,0.0)*2.0}{
       	x1o=0;
       	x2o=0;
       	x3o=300e3;
       	len=dx;
       	width=dx;
	strike=90;
       	dip=90;
	rake=180;
	s1=cos(pi*strike/180.0)
	s2=sin(pi*strike/180.0)
	s3=0
	d1=1-sin(pi*strike/180.0)*cos(pi*dip/180.0)-1
	d2=1+cos(pi*strike/180.0)*cos(pi*dip/180.0)-1
	d3=1+sin(pi*dip/180.0)-1
	for (j=-nw/2;j<(nw/2);j++){
		for (i=-nl/2;i<(nl/2);i++){
			x1=x1o+i*s1*len+j*d1*width;
			x2=x2o+i*s2*len+j*d2*width;
       	        	x3=x3o+i*s3*len+j*d3*width;
       	        	printf "%05d %e %16.11e %16.11e %16.11e %f %f %f %f %f\n", k,1e-9,x1,x2,x3,len,width,strike,dip,rake;
			k=k+1;
		}
       	}
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa)
32.03812032e3 32.03812032e3
# time interval (s)
4.725e10
# number of rectangular patches
`grep -v "#" $FLT | wc -l`
# n  Vl      x1    x2   x3 length width strike dip rake
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk -v dx=$DX '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	{ 
	x2=$4;
	x3=$5-300e3;
	mu0=0.6;
	sig=50;
	a0=0.0065;
	amax=0.025;
	a=a0+ramp(max(abs(x3)-15e3,abs(x2)-30e3)/3e3)*(amax-a0);
	b=0.013;
	Vo=1e-6;
	L=4e-2;
	rho=2670;
	Vs=3464;
	G=rho*Vs^2/1e6;
	damping=G/Vs/2;
	Vl=1e-9;
	tau0=a*sig*asinh(Vl/Vo/2*exp((mu0+b*log(Vo/Vl))/a));
	# stress perturbation
	if (1==boxcar((x2-(30e3-1.5e3-6e3))/12e3)*boxcar((x3-(15e3-1.5e3-6e3))/12e3)){
		tau0=a*sig*asinh(1e-3/Vo/2*exp((mu0+b*log(Vo/Vl))/a));
	}
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,damping;
	}'`
# number of triangular patches
0
# number of observation patches
`for i in 1; do echo "$VER"; echo "$HOR"; echo "$ALL"; echo "$OPT"; done | wc -l | awk '{print $1}'`
# n   index rate
`for i in 1; do echo "$VER"; echo "$HOR"; done | awk '{print NR,$1,20}'`
`echo "$ALL" | awk -v nl=$NY -v nw=$NZ '{print nl+nw+NR,$1,50}'`
`echo "$OPT" | awk -v nl=$NY -v nw=$NZ -v dx=$DX '{i=int($1/dx)+nl/2+1;j=int($2/dx)+nw/2+1;print nl*nw/16+nl+nw+NR,i+j*nl+1,1}'`
# number of observation points
0
# number of events
0
EOF

# load Greens function if they exist
if [ -e $GDIR/greens-0000.grd ]; then
	IMPORT_GREENS="--import-greens $GDIR"
fi
 
mpirun -n 16 unicycle-3d-ratestate \
	$* \
	--epsilon 1e-4 \
	--friction-law 3 \
	--maximum-step 3.15e7 \
	--maximum-iterations 1000000 \
	--export-greens $GDIR \
	$IMPORT_GREENS \
	$WDIR/in.param

echo "$OPT" | awk -v nl=$NY -v nw=$NZ -v dx=$DX '{i=int($1/dx)+nl/2+1;j=int($2/dx)+nw/2+1;print $1,$2,nl*nw/16+nl+nw+NR,i+j*nl+1}' | while IFS=" " read x y opt index
do
	station=$WDIR/fltst_strk`echo $x $y | awk '{print $1"0",$2"0"}' | sed 's/\.//g' | awk '{print substr($1,0,4)"dp"substr($2,0,4)}'`
	echo "# exporting to $station"
	file=$WDIR/patch-$(printf "%08d" $opt)-$(printf "%08d" $index).dat

	cat <<EOF > $station
# This is the file header:
# problem=SEAS Benchmark No.4
# code=unicycle
# version=1.0
# modeler=S. Barbot
# date=2020/01/08
# element size=$DX m
# location= on fault, `echo $x | awk '{print $1/1e3}'`km along strike, `echo $y | awk '{print $1/1e3}'`km depth
# minimum time step=0.1
# maximum time step=3.157e6
# num time steps=`grep -v "#" $file | wc -l | awk '{print $1}'`
# Column #1 = Time (s)
# Column #2 = Slip 2 (m)
# Column #3 = Slip 3 (m)
# Column #4 = Slip rate 2 (log10 m/s)
# Column #5 = Slip rate 3 (log10 m/s)
# Column #6 = Shear stress 2 (MPa)"
# Column #7 = Shear stress 3 (MPa)
# Column #8 = State (log10 s)
# The line below lists the names of the data fields
t slip_2 slip_3 slip_rate_2 slip_rate_3 shear_stress_2 shear_stress_3 state
# Here is the time-series data.
EOF
	grep -v "#" $file | awk 'function abs(x){return (x>0)?x:-x}{print $1,-$2,$3,log(abs(-$9))/log(10),log(abs($10))/log(10),-$4,$5,$7}' >> $station
done

echo "# exporting to $WDIR/global.dat"
cat <<EOF > $WDIR/global.dat
# This is the file header:
# problem=SEAS Benchmark No.4
# code=unicycle
# version=1.0
# modeler=S. Barbot
# date=2020/01/08
# element size=${DX} m
# location= All domain
# minimum time step=0.1
# maximum time step=3.157e6
# num time steps=`grep -v "#" $WDIR/time.dat | wc -l | awk '{print $1}'`
# Column #1 = Time (s)
# Column #2 = Max slip rate (log10 m/s)
# Column #3 = Moment rate (N-m/s)
# The line below lists the names of the data fields
t max_slip_rate moment_rate
# Here is the time-series data.
EOF
grep -v "#" $WDIR/time-weakening.dat | \
	awk -v Vpl=1e-9 -v G=32.03812032e3 -v nw=$NZ -v nl=NY -v dx=$DX \
		'{printf "%24.18e %e %e\n",$1,log($3)/log(10),($4+G*Vpl*nw*nl*dx)*1e6}' >> $WDIR/global.dat


#
# section 4.5 Rupture Time Contour Output
#

echo "# exporting to $WDIR/rupture.dat"
files=`echo "$ALL" | awk -v d=$WDIR -v nl=$NY -v nw=$NZ '{printf "%s/patch-%08d-%08d.dat ",d,nl+nw+NR,$1}'`
cat <<EOF > $WDIR/rupture.dat
# This is the file header:
# problem=SEAS Benchmark No.4
# author=S. Barbot
# date=2019/01/08
# code=unicycle
# code version=1.0
# element size=$DX m
# Column #1 = x2 (m)
# Column #2 = x3 (m)
# Column #3 = time (s)
# The line below lists the names of the data fields
x2 x3 t
# Here are the data
EOF

for i in $files; do
	index=`echo $(basename $i .dat) | awk -F"-" '{print $3}'`
	IFS=" " read x y <<< `echo $index | awk -v nl=$NY -v nw=$NZ -v dx=$DX '{print (($1-1)%nl-nl/2)*dx,(int(($1-1)/nl)-nw/2)*dx}'`
	time=`grep -v "#" $i | awk 'BEGIN{f=0}{v=exp(log(10)*$8);if (v>=1e-3){print $1; f=1;exit}}END{if (0==f){print "1.0E+09"}}'`
	echo $x $y $time >> $WDIR/rupture.dat
done

echo "# exporting to $WDIR/slip-log10v-ver.grd"
files=`echo "$VER" | awk -v d=$WDIR '{printf "%s/patch-%08d-%08d.dat ",d,NR,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print -$2}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
	c=`echo $c | awk '{print $1+1}'`
	grep -v "#" $i | awk -v i=$c '{print i,-$2,$8}'
done | surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$NZ/0/$yRange -G$WDIR/slip-log10v-ver.grd

echo "# exporting to $WDIR/slip-log10v-hor.grd"
files=`echo "$HOR" | awk -v d=$WDIR -v nw=$NZ '{printf "%s/patch-%08d-%08d.dat ",d,NR+nw,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print -$2}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
	c=`echo $c | awk '{print $1+1}'`
	grep -v "#" $i | awk -v i=$c '{print i,-$2,$8}'
done | surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$NY/0/$yRange -G$WDIR/slip-log10v-hor.grd

echo "# exporting to $WDIR/log10v-ver.grd"
files=`echo "$VER" | awk -v d=$WDIR '{printf "%s/patch-%08d-%08d.dat ",d,NR,$1}'`
xRange=`echo "$VER" | wc -l | awk '{print $1}'`
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
c=0
for i in $files; do
	c=`echo $c | awk '{print $1+1}'`
	grep -v "#" $i | awk -v i=$c '{print i,(NR-1)*20+1,$8}'
done | xyz2grd -I1/20 -R1/$xRange/1/$yRange -G$WDIR/log10v-ver.grd

echo "# exporting to $WDIR/log10v-hor.grd"
files=`echo "$HOR" | awk -v d=$WDIR -v nw=$NZ '{printf "%s/patch-%08d-%08d.dat ",d,NR+nw,$1}'`
xRange=`echo "$HOR" | wc -l | awk '{print $1}'`
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
c=0
for i in $files; do
	c=`echo $c | awk '{print $1+1}'`
	grep -v "#" $i | awk -v i=$c '{print i,(NR-1)*20+1,$8}'
done | xyz2grd -I1/20 -R1/$xRange/1/$yRange -G$WDIR/log10v-hor.grd

echo "# exporting to $WDIR/time-log10v-ver.grd"
files=`echo "$VER" | awk -v d=$WDIR '{printf "%s/patch-%08d-%08d.dat ",d,NR,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
	c=`echo $c | awk '{print $1+1}'`
	grep -v "#" $i | awk -v i=$c '{print i,$1/3.15e7,$8}'
done | \
blockmean -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$NZ/0/$yRange -E | awk '{print $1,$2,$6}' | \
surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$NZ/0/$yRange -G$WDIR/time-log10v-ver.grd

echo "# exporting to $WDIR/time-log10v-hor.grd"
files=`echo "$HOR" | awk -v d=$WDIR -v nw=$NZ '{printf "%s/patch-%08d-%08d.dat ",d,NR+nw,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
	c=`echo $c | awk '{print $1+1}'`
	grep -v "#" $i | awk -v i=$c '{print i,$1/3.15e7,$8}'
done | \
blockmean -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$NY/0/$yRange -E | awk '{print $1,$2,$6}' | \
surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$NY/0/$yRange -G$WDIR/time-log10v-hor.grd


