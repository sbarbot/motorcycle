#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Benchmark problem BP4 is for a three-dimensional (3D) extension of BP1 to a problem in
# a whole-space (quasi-dynamic approximation is still assumed), although some parameters
# are changed to make the computations more feasible. The model size, resolution, initial and
# boundary conditions, and model output are designed specically for 3D problems.

selfdir=$(dirname $0)

#WDIR=$selfdir/output-bp4-500-L120-W120-contour
#WDIR=$selfdir/output-bp4-500-L120-W80-contour
WDIR=$selfdir/output-bp4-500-L120-W80

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

N1=256
N2=256
DX=500
P1=8
#P2=8 # W120
P2=48 # W80

while getopts "p" flag
do
	case "$flag" in
	p) pset="1";;
	esac
done
for item in $pset;do
	shift
done

if [ "" == "$pset" ]; then

	cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli lambda, rigidity (MPa)
32.03812032e3 32.03812032e3
# time interval (s)
4.725e10
#120
# number of faults
1
# grid dimension (N1,N2)
$N1 $N2
# sampling (dx1,dx2)
$DX $DX
#   n tau0 mu0 sig    a     b    L   Vo G/2Vs Vl rake Dirichlet
`echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX -v p1=$P1 -v p2=$P2 '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	BEGIN{
	c=1;
	tau0_p=-1;mu0_p =-1;sig_p=-1;
	a_p=-1;b_p=-1;L_p=-1;
	Vo_p=-1;damping_p=-1;
	Vl_p=-1;rake_p=-1;dirichlet_p="T";
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=(i2-n2/2)*dx; 
			mu0=0.6;
			sig=50;
			a0=0.0065;
			amax=0.025;
			a=a0+ramp(max(abs(x2)-15e3,abs(x1)-30e3)/3e3)*(amax-a0);
			b=0.013;
			Vo=1e-6;
			L=4e-2;
			rho=2670;
			Vs=3464;
			G=rho*Vs^2/1e6;
			damping=G/Vs/2;
			rake=180;
			dirichlet=((i2<p2) || (i2>=(n2-p2)) || (i1<p1) || (i1>=(n1-p1)))?"T":"F";
			Vl=1e-9;
			tau0=a*sig*asinh(Vl/Vo/2*exp((mu0+b*log(Vo/Vl))/a));
			# stress perturbation
			if (1==boxcar((x1-(-30e3+1.5e3+6e3))/12e3)*boxcar((x2-(15e3-1.5e3-6e3))/12e3)){
				tau0=a*sig*asinh(1e-3/Vo/2*exp((mu0+b*log(Vo/Vl))/a));
			}
			if ((tau0_p == tau0) && (mu0_p==mu0) && (sig_p==sig) && (a_p==a) && (b_p==b) && (L_p==L) && 
			    (Vo_p==Vo) && (damping_p==damping) && (Vl_p==Vl) && (rake_p==rake) && (dirichlet_p==dirichlet)){
				printf "%5d\n",-c;
			} else {
				printf "%5d %11.4e %10.4e %10.4e %10.4e %10.3e %10.4e %10.4e %10.4e %10.4e %10.3e %s\n", 
	         			c,  tau0,   mu0,   sig,     a,     b,     L,    Vo, damping, Vl, rake, dirichlet;
			}
			c++;
			tau0_p=tau0;mu0_p=mu0;sig_p=sig;
			a_p=a;b_p=b;L_p=L;
			Vo_p=Vo;damping_p=damping;
			Vl_p=Vl;rake_p=rake;dirichlet_p=dirichlet;
		}
	}
}'`
# number of observation patches
14
# n fault i1 i2 rate
`cat <<EOF | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{print NR,1,int($1/dx)+n1/2+1,int($2/dx)+n2/2+1,10}'
-36.0e3 +0.00e3
-22.5e3 -7.50e3
-16.5e3 -12.0e3
-16.5e3 +0.00e3
-16.5e3 +12.0e3
+0.00e3 -21.0e3
+0.00e3 -12.0e3
+0.00e3 +0.00e3
+0.00e3 +12.0e3
+0.00e3 +21.0e3
+16.5e3 -12.0e3
+16.5e3 +0.00e3
+16.5e3 +12.0e3
+36.0e3 +0.00e3
EOF`
# number of profiles
2
# n fault index direction rate
  1     1  `echo $N2 | awk '{print int($1/2)+1}'` 1 50
  2     1  `echo $N1 | awk '{print int($1/2)+1}'` 2 50
# number of events
0
EOF

OMP_NUM_THREADS=1 mpirun -n 2 motorcycle-3d-ratestate \
	$* \
	--verbose 1 \
	--export-netcdf --export-state \
	--source-export-rate 5000 \
	--epsilon 1e-4 \
	--friction-law 3 \
	--maximum-step 3.15e7 \
	--maximum-iterations 2000000 \
	$WDIR/in.param

else
	echo "$0: skipping simulation"
fi


minStep=`grep -v "#" $WDIR/time.dat | gmt minmax -C -H1 | awk '{print $3}'`
maxStep=`grep -v "#" $WDIR/time.dat | gmt minmax -C | awk '{print $4}'`
numStep=`grep -cv "#" $WDIR/time.dat`

echo "$0: process $WDIR/global.dat"
grep -v "#" $WDIR/time-weakening.dat | \
	awk -v d="`date`" -v minStep=$minStep -v maxStep=$maxStep -v numStep=$numStep -v width=$DX 'BEGIN{
	print "# problem=Benchmark problem (BP3-QD)";
	print "# author=Sylvain Barbot";
	print "# date="d;
	print "# code=motorcycle-3d-ratestate";
	print "# code_version=beta";
	print "# element_size="width" m";
	print "# minimum_time_step="minStep;
	print "# maximum_time_step="maxStep;
	print "# num_time_steps="numStep;
	print "# Column #1 = Time (s)";
	print "# Column #2 = Max_slip_rate (log10 m/s)";
	print "# Column #4 = Moment_rate (N-m/s)";
	print "# The line below lists the names of the data fields";
	print "t max_slip_rate moment_rate"
	print "# Here is the time-series data.";
}{
	print $1,log($3)/log(10),$4
}' > $WDIR/global.dat

minStep=`grep -v "#" $WDIR/time.dat | gmt minmax -C -H1 | awk '{print $3}'`
maxStep=`grep -v "#" $WDIR/time.dat | gmt minmax -C | awk '{print $4}'`
numStep=`grep -cv "#" $WDIR/time.dat`

for i in $WDIR/patch-01-*-*.dat; do
	I=`echo $(basename $i .dat) | awk -F "-" -v n1=$N1 -v dx=$DX '{printf "%+04d\n",($3-n1/2-1)*dx/100}'`
	J=`echo $(basename $i .dat) | awk -F "-" -v n2=$N2 -v dx=$DX '{printf "%+04d\n",($4-n2/2-1)*dx/100}'`
	station=$WDIR/fltst_strk${I}dp${J}
	numStep=`wc -l $i | awk '{print $1}'`

	echo "# export to $station"

	grep -v "#" $i | \
		awk -v d="`date`" -v minStep=$minStep -v maxStep=$maxStep -v numStep=$numStep -v width=$DX '
		function abs(x){return (x>0)?x:-x}
		function log10(x){return (0==x)?-20:log(abs(x))/log(10)}
		BEGIN{
		print "# problem=Benchmark problem (BP4-QD)";
		print "# author=Sylvain Barbot";
		print "# date="d;
		print "# code=motorcycle-3d-ratestate";
		print "# code_version=beta";
		print "# element_size="width" m";
		print "# minimum_time_step="minStep;
		print "# maximum_time_step="maxStep;
		print "# num_time_steps="numStep;
		print "# location=on fault, "depth"km depth";
		print "# Column #1 = Time (s)";
		print "# Column #2 = Slip_2 (m)";
		print "# Column #3 = Slip_3 (m)";
		print "# Column #4 = Slip_rate_2 (log10 m/s)";
		print "# Column #5 = Slip_rate_3 (log10 m/s)";
		print "# Column #6 = Shear_stress_2 (MPa)";
		print "# Column #7 = Shear_stress_3 (MPa)";
		print "# Column #8 = State (log10 s)";
		print "# The line below lists the names of the data fields";
		print "t slip_2 slip_3 slip_rate_2 slip_rate_3 shear_stress_2 shear_stress_3 state";
		print "# Here is the time-series data.";
	}{
		print $1,-$2,$3,log10(-$9),log10($10),$4,$5,$7;
	}' > $station
done

