#!/usr/bin/perl
# SEAS upload files.
# Version 1.1.
# Michael Barall 05/22/2015, SEAS version 03/20/2018, revised 09/01/2020 for new website address.
# Based on a script written by Jeremy Kozdon, who in turn modified a script from:
#    http://www.willmaster.com/library/manage-forms/using_perl_to_submit_a_form.php

# This script uploads the files for a benchmark.


use strict;
use warnings;

my $SCEC_SEAS = "https://strike.scec.org/cvws/cgi-bin/seas.cgi";

my $NARGS = $#ARGV+1;
if ($NARGS == 0 || $ARGV[0] =~ /^(help|-help|--help)$/i)
{
  print "\n";
  print "Upload the files for a benchmark.\n";
  print "\n";
  print "Usage:  seas_upload.pl [SWITCHES] USERNAME PASSWORD PROBLEM\n";
  print "Usage:  seas_upload.pl [SWITCHES] USERNAME PASSWORD PROBLEM VERSION\n";
  print "\n";
  print "Example: seas_upload.pl amodeler secret bp1\n";
  print "\n";
  print "   The user named 'amodeler' with password 'secret' is uploading files\n";
  print "   for benchmark problem 'bp1'.  Files are uploaded to the initial\n";
  print "   version, which is version number 1, and is called 'amodeler'.\n";
  print "\n";
  print "Example: seas_upload.pl amodeler secret bp1 2\n";
  print "\n";
  print "   The user named 'amodeler' with password 'secret' is uploading files\n";
  print "   for benchmark problem 'bp1'.  Files are uploaded to version number 2,\n";
  print "   which is called 'amodeler.2'.\n";
  print "\n";
  print "Example: seas_upload.pl --delete amodeler secret bp2 3\n";
  print "\n";
  print "   The user named 'amodeler' with password 'secret' is deleting files\n";
  print "   for benchmark problem 'bp2'.  All existing files on the web server\n";
  print "   are deleted from version number 3, which is called 'amodeler.3'.\n";
  print "\n";
  print "Before running this script, change to the directory that contains the\n";
  print "simulation output files.  The script obtains a list of expected filenames\n";
  print "from the web server.  It then looks in the current directory for local\n";
  print "filenames that match the expected filenames.  A match occurs if the\n";
  print "expected filename is a substring of the local filename.  For example, if\n";
  print "the web server expects a file called 'fltst_dp100', then the local\n";
  print "filename could be 'fltst_dp100' or 'fltst_dp100.dat' or\n";
  print "'bp1_fltst_dp100' or 'bp1_fltst_dp100.dat' or any other name\n";
  print "that contains 'fltst_dp100' somewhere in the name.  The comparison\n";
  print "is case-sensitive.\n";
  print "\n";
  print "The VERSION must be a number between 1 and 99.  If omitted, the default\n";
  print "value is 1.  For versions 2 through 99, you must use the website to create\n";
  print "the version before uploading any files to that version.  Version 1 always\n";
  print "exists.\n";
  print "\n";
  print "The following optional SWITCHES can be used:\n";
  print "\n";
  print "--dryrun    List the files to be uploaded, but do not upload them.\n";
  print "            You can use --dryrun to see what would be uploaded.\n";
  print "\n";
  print "--delete    Delete existing files, but do not upload new files.\n";
  print "            You can use --delete --dryrun to see what would be deleted.\n";
  print "            (When deleting, it is not necessary for the current directory\n";
  print "            to contain simulation output files.)\n";
  print "\n";
  print "--help      Display this help information.\n";
  print "            (With --help, no other command parameters are needed.)\n";
  print "\n";
  exit 0;
}

my $k = 0;

my $altsn = 0;       # controls use of alternate station filenames, activate with --altsn switch
my $dryrun = 0;      # list files but do not upload them, activate with --dryrun switch
my $deleting = 0;    # delete existing files, activate with --delete switch

# Scan command-line switches

while ($k < $NARGS && $ARGV[$k] =~ /^-/)
{
  if ($ARGV[$k] =~ /^--$/)      # the -- switch ends processing of switches
  {
    $k = $k+1;
    last;
  }

  if ($ARGV[$k] =~ /^--altsn$/i)
  {
    $altsn = 1;
    $k = $k+1;
    next;
  }

  if ($ARGV[$k] =~ /^--dryrun$/i)
  {
    $dryrun = 1;
    $k = $k+1;
    next;
  }

  if ($ARGV[$k] =~ /^--delete$/i)
  {
    $deleting = 1;
    $k = $k+1;
    next;
  }

  print "Invalid command-line switch: $ARGV[$k]\n";
  exit 1;
}

# Scan command-line positional arguments

if ($k >= $NARGS) {print "Too few command arguments.\n"; exit 1;}
my $UNAME = lc($ARGV[$k]);
$k = $k+1;

if ($k >= $NARGS) {print "Too few command arguments.\n"; exit 1;}
my $PSSWD = $ARGV[$k];
$k = $k+1;

if ($k >= $NARGS) {print "Too few command arguments.\n"; exit 1;}
my $TPV   = lc($ARGV[$k]);
$k = $k+1;

my $VERNUM = 1;
if($k < $NARGS)
{
  $VERNUM = $ARGV[$k];
  if ($VERNUM !~ /^[1-9][0-9]?$/) {print "Invalid version number: $VERNUM\n"; exit 1;}
  $k = $k+1;
}

if ($k < $NARGS) {print "Too many command arguments.\n"; exit 1;}

# Combine username and version number

my $URNM  = $UNAME;
if($VERNUM != 1)
{
  $URNM  = "$UNAME.$VERNUM";
}

my $directory = '.';

# Ask user to confirm operation

print "\n";
if ($dryrun == 0)
{
  if ($deleting == 0)
  {
    print "Proceed to upload files?\n";
  }
  else
  {
    print "Proceed to delete files?\n";
  }
}
else
{
  if ($deleting == 0)
  {
    print "Proceed to list files? No uploads will occur.\n";
  }
  else
  {
    print "Proceed to list files? No deletions will occur.\n";
  }
}
print "  Problem:         $TPV\n";
print "  User name:       $UNAME\n";
print "  Version number:  $VERNUM\n";
print "  User version:    $URNM\n";
print "Enter 'yes' to proceed or 'no' to cancel:\n";

chomp(my $input = <STDIN>);
if ($input !~ /^\s*yes\s*$/i)
{
  print "Cancelled.\n";
  exit 0;
}

# Modules with routines for making the browser.
use LWP::UserAgent;
use HTTP::Request::Common;

# Create the browser that will post the information.
my $Browser = new LWP::UserAgent;
$Browser->ssl_opts( verify_hostname => 0 , SSL_verify_mode => 0x00);

# POST command to read the change version page
# Note urv is the default version 1, which is $UNAME, not $URNM

my %CHVER_DATA = (
  "G1084"      => "  Change Version  ",
  "u"          => $UNAME,
  "p"          => $PSSWD,
  "m"          => $TPV,
  "urv"        => $UNAME,
  "o"          => "1005"
);

# Post the information to the CGI program.
my $ChVerPage = $Browser->request(POST $SCEC_SEAS,\%CHVER_DATA);

if ($ChVerPage->is_success)
{
  # Check the info provided
  if(index($ChVerPage->content,"Forbidden Operation") != -1)
  {
    print "Username or password is likely wrong.\n";
    exit 1;
  }
  if(index($ChVerPage->content,"Benchmark Not Found") != -1)
  {
    print "Benchmark $TPV not found.\n";
    exit 2;
  }

  # Check that our version exists

  if(index($ChVerPage->content,"name=\"G1090${URNM}\"") == -1)
  {
    print "\n";
    print "Version does not exist: $URNM\n";
    print "Versions must be created on the website before you can use them.\n";
    print "\n";
    exit 2;
  }

}
else
{
  # Come here if we failed to contact the web server when reading the change version page

  print "Failed to communicate with web server.\n";
  print $ChVerPage->message;
  print "\n";

  exit 2;
}

# POST command to read the upload page

my %LIST_DATA = (
  "G1090$URNM" => "Select",
  "u"          => $UNAME,
  "p"          => $PSSWD,
  "m"          => $TPV,
  "urv"        => $URNM,
  "o"          => "1005"
);

# Post the information to the CGI program.
my $Page = $Browser->request(POST $SCEC_SEAS,\%LIST_DATA);

if ($Page->is_success)
{
  # Check the info provided
  if(index($Page->content,"Forbidden Operation") != -1)
  {
    print "Username or password is likely wrong.\n";
    exit 1;
  }
  if(index($Page->content,"Benchmark Not Found") != -1)
  {
    print "Benchmark $TPV not found.\n";
    exit 2;
  }

  # Deletion command

  if ($deleting != 0)
  {
    # If not a dry run, ask user to confirm one more time

    if ($dryrun == 0)
    {
      print "\n";
      print "Are you sure?\n";
      print "Enter 'yes' to delete existing files or 'no' to cancel:\n";
      
      chomp($input = <STDIN>);
      if ($input !~ /^\s*yes\s*$/i)
      {
        print "Cancelled.\n";
        exit 0;
      }
    }

    # Initialize counters for final summary
    
    my $delcount = 0;
    my $badcount = 0;

    # Loop over time series files listed on the upload page
    
    print "\n";
    print "Processing time series files.\n";
    
    foreach my $linets (split(/\n/,$Page->content))
    {
      $linets =~ /name="G1024([^"]+)" value="Delete">/ || next;
      my ($station) = ($1);
      $delcount = $delcount + 1;
  
      if ($dryrun != 0)
      {
        print "Existing time series file: $station\n";
        next;
      }

      # Delete the file

      print "Deleting time series file: $station\n";
  
      my %DELETE_CMD = (
        "G1025"=>"Delete",
        "u"=>$UNAME,
        "p"=>$PSSWD,
        "m"=>$TPV,
        "s"=>$station,
        "urv"=>$URNM,
        "o"=>"1005",
      );
      my $PageDelete = $Browser->request(POST $SCEC_SEAS,\%DELETE_CMD);
     
      if(index($PageDelete->content,"File Deleted") == -1)
      {
        print "   >>>>> Delete Failed!\n";
        $badcount = $badcount + 1;
      }
    }

    # Loop over contour plot files listed on the upload page
    
    print "\n";
    print "Processing contour plot files.\n";
    
    foreach my $linecp (split(/\n/,$Page->content))
    {
      $linecp =~ /name="G1035([^"]+)" value="Delete">/ || next;
      my ($station) = ($1);
      $delcount = $delcount + 1;
  
      if ($dryrun != 0)
      {
        print "Existing contour plot file: $station\n";
        next;
      }

      # Delete the file

      print "Deleting contour plot file: $station\n";
  
      my %DELETE_CMD = (
        "G1036"=>"Delete",
        "u"=>$UNAME,
        "p"=>$PSSWD,
        "m"=>$TPV,
        "s"=>$station,
        "urv"=>$URNM,
        "o"=>"1005",
      );
      my $PageDelete = $Browser->request(POST $SCEC_SEAS,\%DELETE_CMD);
     
      if(index($PageDelete->content,"File Deleted") == -1)
      {
        print "   >>>>> Delete Failed!\n";
        $badcount = $badcount + 1;
      }
    }

    # Display final summary

    print "\n";
    if ($dryrun == 0)
    {
      print "Deleted $delcount file(s).\n";
      if ($badcount != 0)
      {
        print "Delete apparently failed for $badcount file(s).\n";
      }
    }
    else
    {
      print "Found $delcount existing file(s).\n";
    }
    print "\n";

    exit 0;
  
  }  # end of deletion command

  # Check if there are already files on the web server

  if(index($Page->content,"value=\"Delete\">") != -1)
  {
    print "\n";
    print "There are already files on the web server.\n";

    if ($dryrun == 0)
    {
      print "Enter 'yes' to replace existing files or 'no' to cancel:\n";
      
      chomp($input = <STDIN>);
      if ($input !~ /^\s*yes\s*$/i)
      {
        print "Cancelled.\n";
        exit 0;
      }
    }
  }

  # Initialize counters for final summary

  my $failcount = 0;
  my $goodcount = 0;
  my $notfcount = 0;

  # Loop over time series files listed on the upload page

  print "\n";
  print "Processing time series files.\n";

  foreach my $linets (split(/\n/,$Page->content))
  {
    $linets =~ /name="G1027([^"]+)"/ || next;
    my ($station) = ($1);
    my $localsn = $station;
    my $found = 0;

    # Alternate name convention has a plus sign in front of any non-negative number,
    # and does not use the 'st' or 'dp' separators

    if ($altsn != 0)
    {
      $localsn =~ s/(body)([0-9]{3,3})/$1+$2/;
      $localsn =~ s/(st)([0-9]{3,3})/+$2/;
      $localsn =~ s/(st)-([0-9]{3,3})/-$2/;
      $localsn =~ s/(dp)([0-9]{3,3})/+$2/;
      $localsn =~ s/(dp)-([0-9]{3,3})/-$2/;
    }

    # Scan the disk directory until we find a matching filename

    opendir (DIR, $directory) or die $!;
    while (my $file = readdir(DIR))
    {
      if (index($file,$localsn) == -1)
      {
        next;
      }
      $found = 1;

      if ($dryrun != 0)
      {
        print "Found time series file: $file\n";
        $goodcount = $goodcount + 1;
        last;
      }

      # Found matching file, now upload it

      print "Uploading time series file: $file\n";

      my $UpPage = $Browser->post(
        $SCEC_SEAS,
        [
          "file"=>[$file],
          "G1029"=>"Click Once to Upload",
          "u"=>$UNAME,
          "p"=>$PSSWD,
          "m"=>$TPV,
          "s"=>$station,
          "urv"=>$URNM,
          "o"=>"1005",
        ],
        'Content_Type' => 'form-data',);
  
      if(index($UpPage->content,"Data File Not Found") != -1)
      {
        $failcount = $failcount + 1;
        print "   >>>>> Upload Failed!\n";
        print "   >>>>> Likely '$station' is an invalid station name.\n";
      }
      elsif(index($UpPage->content,"Upload Fail") != -1)
      {
        $failcount = $failcount + 1;
        print "   >>>>> Upload Failed!\n";
        foreach (split(/\n/,$UpPage->content))
        {
          if($_ =~ /error/i)
          {
            $_ =~ s/<\/?[a-zA-Z]+>//g;    # remove html tags
            print "   >>>>> $_\n";
          }
        }
      }
      else
      {
        $goodcount = $goodcount + 1;
      }

      last;
    }
    closedir(DIR);

    # If we didn't find a matching filename, tell the user

    if ($found == 0)
    {
      print "No file found for time series: $localsn\n";
      $notfcount = $notfcount + 1;
    }
  }

  # Loop over contour plot files listed on the upload page

  print "\n";
  print "Processing contour plot files.\n";

  foreach my $linecp (split(/\n/,$Page->content))
  {
    $linecp =~ /name="G1038([^"]+)"/ || next;
    my ($station) = ($1);
    my $localsn = $station;
    my $found = 0;

    # Scan the disk directory until we find a matching filename

    opendir (DIR, $directory) or die $!;
    while (my $file = readdir(DIR))
    {
      if (index($file,$localsn) == -1)
      {
        next;
      }
      $found = 1;

      if ($dryrun != 0)
      {
        print "Found contour plot file: $file\n";
        $goodcount = $goodcount + 1;
        last;
      }

      # Found matching file, now upload it

      print "Uploading contour plot file: $file\n";

      my $UpPage = $Browser->post(
        $SCEC_SEAS,
        [
          "file"=>[$file],
          "G1040"=>"Click Once to Upload",
          "u"=>$UNAME,
          "p"=>$PSSWD,
          "m"=>$TPV,
          "s"=>$station,
          "urv"=>$URNM,
          "o"=>"1005",
        ],
        'Content_Type' => 'form-data',);
  
      if(index($UpPage->content,"Data File Not Found") != -1)
      {
        $failcount = $failcount + 1;
        print "   >>>>> Upload Failed!\n";
        print "   >>>>> Likely '$station' is an invalid contour plot name.\n";
      }
      elsif(index($UpPage->content,"Upload Fail") != -1)
      {
        $failcount = $failcount + 1;
        print "   >>>>> Upload Failed!\n";
        foreach (split(/\n/,$UpPage->content))
        {
          if($_ =~ /error/i)
          {
            $_ =~ s/<\/?[a-zA-Z]+>//g;    # remove html tags
            print "   >>>>> $_\n";
          }
        }
      }
      else
      {
        $goodcount = $goodcount + 1;
      }

      last;
    }
    closedir(DIR);

    # If we didn't find a matching filename, tell the user

    if ($found == 0)
    {
      print "No file found for contour plot: $localsn\n";
      $notfcount = $notfcount + 1;
    }
  }

  # Display final summary

  print "\n";
  if ($dryrun == 0)
  {
    if ($goodcount != 0)
    {
      print "Upload succeeded for $goodcount file(s).\n";
    }
    if ($failcount != 0)
    {
      print "Upload failed for $failcount file(s).\n";
    }
    if ($goodcount == 0 && $failcount == 0)
    {
      print "No files were uploaded.\n";
    }
  }
  else
  {
    print "Found $goodcount file(s).\n";
  }
  if ($notfcount != 0)
  {
    print "Failed to find $notfcount file(s).\n";
  }
  print "\n";

}
else
{
  # Come here if we failed to contact the web server when reading the upload page

  print "Failed to communicate with web server.\n";
  print $Page->message;
  print "\n";

  exit 2;
}

# end of script

exit 0;

