#!/bin/bash -e

# slow-slip events along a single fault
# h* = 3750 m; W = 10 km; Ru = 2.66; Rb = 0.285

# use .grd input files for physical parameters

selfdir=$(dirname $0)

# output directory
WDIR=$selfdir/output1-binary

if [ ! -e "$WDIR" ]; then
	echo "adding directory $WDIR"
	mkdir -p "$WDIR"
fi

# number of fault patches
N1=256
N2=128
# patch size
DX=400

# .grd file names
TAU0="$WDIR/tau0.grd"
MU0="$WDIR/mu0.grd"
SIG="$WDIR/sig.grd"
A="$WDIR/a.grd"
B="$WDIR/b.grd"
L="$WDIR/L.grd"
V0="$WDIR/Vo.grd"
DAMPING="$WDIR/damping.grd"
VL="$WDIR/Vl.grd"
RAKE="$WDIR/rake.grd"
DIRICHLET="$WDIR/bc.grd"

echo "# initial shear stress $TAU0"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '
	function abs(x){return (x>0)?x:-x};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# coordinates
			x1=(i1-1-n1/2)*dx; 
			x2=(i2-1-n2/2)*dx; 
			# effective normal stress (MPa)
			sig=1e2; 
			# reference coefficient of friction (unitless)
			mu0=0.6; 
			# rate-dependent parameter (unitless)
			a=1e-2; 
			# state-dependent parameter (unitless)
			b=(abs(x1)<37.5e3 && abs(x2)<5e3)?a+4.0e-3:a-4e-3;
			# characteristic weakening distance (m)
			L=0.05;
			# reference slip-rate (m/s)
			Vo=1e-6; 
			# loading rate (m/s)
			Vl=1e-9;
			# normal of initial shear traction (MPa); use -1 to use steady-state value
			if (1==boxcar((x1+20e3)/15e3)*boxcar((x2-0e3)/15e3)){
				# initial stress triggers nucleation (MPa)
				tau0=mu0*sig*(1.1e-9/Vo)^(a/mu0)*(L/Vl)^(b/mu0);
			} else {
				tau0=-1;
			}
			printf "%5d %5d %13.5e\n",i1,i2,tau0;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$TAU0"

echo "# reference friction coefficient $MU0"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# reference coefficient of friction (unitless)
			mu0=0.6; 
			printf "%5d %5d %13.5e\n",i1,i2,mu0;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$MU0"

echo "# normal stress $SIG"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# effective normal stress (MPa)
			sig=1e2; 
			printf "%5d %5d %13.5e\n",i1,i2,sig;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$SIG"

echo "# friction parameter $A"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# rate-dependent parameter (unitless)
			a=1e-2; 
			printf "%5d %5d %13.5e\n",i1,i2,a;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$A"

echo "# friction parameter $B"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '
	function abs(x){return (x>0)?x:-x};
	{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# coordinates
			x1=(i1-1-n1/2)*dx; 
			x2=(i2-1-n2/2)*dx; 
			# rate-dependent parameter (unitless)
			a=1e-2; 
			# state-dependent parameter (unitless)
			b=(abs(x1)<37.5e3 && abs(x2)<5e3)?a+4.0e-3:a-4e-3;
			printf "%5d %5d %13.5e\n",i1,i2,b;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$B"

echo "# characteristic weakening distance $L"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# characteristic weakening distance (m)
			L=0.05;
			printf "%5d %5d %13.5e\n",i1,i2,L;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$L"

echo "# reference slip-rate $V0"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# reference slip-rate (m/s)
			Vo=1e-6; 
			printf "%5d %5d %13.5e\n",i1,i2,Vo;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$V0"

echo "# radiation damping coefficient $DAMPING"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# radiation damping coefficient ( G/(2 Vs) in units of MPa/m*s )
			damping=5;
			printf "%5d %5d %13.5e\n",i1,i2,damping;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$DAMPING"

echo "# long-term slip rate $VL"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# loading rate (m/s)
			Vl=1e-9;
			printf "%5d %5d %13.5e\n",i1,i2,Vl;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$VL"

echo "# rake of long-term slip rate $RAKE"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# rake angle for the loading rate (degree)
			rake=0;
			printf "%5d %5d %13.5e\n",i1,i2,rake;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$RAKE"

echo "# Dirichlet boundary condition $DIRICHLET"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# whether to apply Dirichlet boundary condition (constant velocity Vl)
			dirichlet=((i2<10) || (i2>=(n2-10)) || (i1<10) || (i1>=(n1-10)))?1:0;
			printf "%5d %5d %13.5e\n",i1,i2,dirichlet;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$DIRICHLET"

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (lambda, rigidity)
30e3 30e3
# time interval
3.15e10
# number of faults
1
# grid dimension (N1,N2)
$N1 $N2
# sampling (dx1,dx2)
$DX $DX
# .grd file for tau0
$TAU0
# .grd file for mu0
$MU0
# .grd file for sigma
$SIG
# .grd file for a
$A
# .grd file for b
$B
# .grd file for L
$L
# .grd file for Vo
$V0
# .grd file for G/(2Vs)
$DAMPING
# .grd file for Vl
$VL
# .grd file for rake
$RAKE
# .grd file for boundary condition
$DIRICHLET
# number of observation patches
1
# n fault i1 i2 rate
  1     1 $(echo "" | awk -v n1=$N1 -v n2=$N2 '{print n1/2+1,n2/2+1,1}')
# number of profiles
2
# n fault index direction rate
  1     1  $(echo $N2 | awk '{print int($1/2)+1}') 1 20
  2     1  $(echo $N1 | awk '{print int($1/2)+1}') 2 20
# number of events
0
EOF

# use --verbose=2 to output all parameters
# use --verbose=1 to output only parameters different from previous patch
mpirun -n 2 motorcycle-3d-ratestate $* \
	--grd-input \
	--verbose 1 --epsilon 1e-6 --friction-law 1 \
	--export-netcdf --export-state \
	--maximum-step 3.15e7 --maximum-iterations 40000 \
	$WDIR/in.param

# plot time series of peak velocity with gnuplot
if command -v gnuplot &> /dev/null
then
	gnuplot <<EOF
set terminal postscript
set out '$WDIR/peak-velocity.ps'
set logscale y
set xlabel 'Time (year)'
set ylabel 'Peak velocity (m/s)'
plot '$WDIR/time.dat' using (\$1/3.15e7):3 with line title ''
EOF
fi

# plot time series of instantaneous velocity at fault center with gnuplot
if command -v gnuplot &> /dev/null
then
	gnuplot <<EOF
set terminal postscript
set out '$WDIR/fault-01-central-velocity.ps'
set xlabel 'Time (year)'
set ylabel 'Peak velocity log10(m/s)'
plot '$WDIR/patch-01-00129-00065.dat' using (\$1/3.15e7):8 with line title 'Fault center'
EOF
fi

# plot time series of vertical and horizontal cross-sections
if command -v gmt &> /dev/null
then
../../../bash/grdmap.sh -x -H -p -12/1/0.1 -c cycles.cpt $WDIR/fault-01-i1-0129-log10v.grd
../../../bash/grdmap.sh -x -H -p -12/1/0.1 -c cycles.cpt $WDIR/fault-01-i2-0065-log10v.grd
fi

# plot snapshots of rupture dynamics
if command -v gmt &> /dev/null
then
../../../bash/grdmap.sh -x -p -12/1/0.1 -c cycles.cpt $WDIR/fault-01-index-000????-log10v.grd
fi

