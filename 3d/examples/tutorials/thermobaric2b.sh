#!/bin/bash -e

# simulation for a single fault with hornblende gouge

WDIR="thermobaric2b"
IN="$WDIR/in.param"

if [ ! -e $WDIR ]; then
	echo "adding directory $WDIR"
	mkdir "$WDIR"
fi

# number of fault patches
N1=128
N2=128
# patch size
DX=200

cat <<EOF > $IN
# output directory
$WDIR
# elastic moduli, radiation damping, universal gas constant
30e3 30e3 5 8.31446261815324
# time interval
1e10
# number of faults
1
# grid dimension (N1,N2)
$N1 $N2
# sampling (dx1,dx2)
$DX $DX
# - - - - - - - - - - - - - - - - - - - - - - -
# R O C K   R H E O L O G Y
# - - - - - - - - - - - - - - - - - - - - - - -
# number of rocks
1
# index name
1 test1
# mu0   d0 sigma0 alpha beta
 0.60 1e-6    100  0.05  0.3
# number of flow laws
1
# n   Vo c0  n     Q     To zeta
  1 1e-6  0 70  10e3 293.15    0
# number of healing terms
2
# n f0     p   q    H     To
  1  1 10.65   0 40e3 273.15
  2  1 3.000   0 90e3 443.15
# reciprocal of characteristic strain for weakening (lambda)
10
# - - - - - - - - - - - - - - - - - - - - - - -
# L I T H O L O G Y
# - - - - - - - - - - - - - - - - - - - - - - -
#   n  rock
$(echo "" | awk -v n1="$N1" -v n2="$N2" -v dx="$DX" '
	BEGIN{
	c=1;
	r_p=-1;
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			x2=(i2-n2/2)*dx; 
			r=1;
			if (r_p == r){
				printf "%5d\n",-c;
			} else {
				printf "%5d %5d\n",c,r;
			}
			c++;
			r_p=r;
		}
	}
}')
# - - - - - - - - - - - - - - - - - - - - - - -
# T H E R M A L   P R O P E R T I E S
# - - - - - - - - - - - - - - - - - - - - - - -
#   n           D           W           w        rhoc          Tb
$(echo "" | awk -v n1="$N1" -v n2="$N2" -v dx="$DX" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	BEGIN{
	c=1;
	D_p=-1;
	W_p =-1;
	w_p=-1;
	rhoc_p=-1;
	Tb_p=-1;
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			# fault coordinates
			x1=(i1-n1/2)*dx; 
			x2=i2*dx; 
			# thermal diffusivity (m^2/s)
			D=1e-6;
			# thickness of damage zone (m)
			W=2;
			# thickness of shear zone (m)
			w=1e-0;
			# density * heat capacity
			rhoc=2.6;
		        # bath temperature
		        Tb=273.15+x2*20e-3;
			if ((D_p == D) && (W_p==W) && (w_p==w) && (rhoc_p==rhoc) && (Tb_p==Tb)){
				printf "%5d\n",-c;
			} else {
				printf "%5d %12.6e %12.6e %12.6e %12.6e %12.6e\n", 
	       				  c,     D,     W,     w,  rhoc,    Tb;
			}
			c++;
			D_p=D;
			W_p=W;
			w_p=w;
			rhoc_p=rhoc;
			Tb_p=Tb;
		}
	}
}')
# - - - - - - - - - - - - - - - - - - - - - - -
# M E C H A N I C A L   P R O P E R T I E S
# - - - - - - - - - - - - - - - - - - - - - - -
#   n      tau0      sig      h       Vl    Dirichlet
$(echo "" | awk -v n1="$N1" -v n2="$N2" -v dx="$DX" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	BEGIN{
	c=1;
	tau0_p=-1;
	sig_p =-1;
	h_p=-1;
	Vl_p=-1;
	dirichlet_p="T";
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=(i2-n2/2)*dx; 
			tau0=-1;
			sig=1e2; 
			h=1e-1;
			Vl=1e-9;
			rake=0;
			# whether to apply Dirichlet boundary condition (constant velocity Vl)
			dirichlet=((i2<3) || (i2>=(n2-3)) || (i1<3) || (i1>=(n1-3)))?"T":"F";
			if (1==boxcar((x1+2.5e3)/2.5e3)*boxcar((x2-0e3)/2.5e3)){
				# initial stress triggers nucleation (MPa)
				tau0=60;
			}
			if ((tau0_p == tau0) && (sig_p==sig) && (w_p==w) && (Vl_p==Vl) && (dirichlet_p==dirichlet)){
				printf "%5d %5.3f\n",-c,x2;
			} else {
				printf "%5d %12.6e %12.6e %12.6e %12.6e %12.6e %s\n", 
	        			  c,  tau0,   sig,     h,    Vl,  rake, dirichlet;
			}
			c++;
			tau0_p=tau0;
			sig_p=sig;
			h_p=h;
			Vl_p=Vl;
			dirichlet_p=dirichlet;
		}
	}
}')
# number of observation patches
1
# n fault i1 i2 rate
  1     1 $(echo "" | awk -v n1=$N1 -v n2=$N2 '{print n1/2+1,n2/2+1,1}')
# number of profiles
2
# n fault index direction rate
  1     1  $(echo $N2 | awk '{print int($1/2)+1}') 1 20
  2     1  $(echo $N1 | awk '{print int($1/2)+1}') 2 20
# number of events
0
EOF

mpirun -n 2 motorcycle-3d-thermobaric $* \
	--verbose 1 \
	--epsilon 1e-6 \
	--evolution-law 1 \
	--export-netcdf \
	--export-state \
	--maximum-step 3.15e7 \
	--maximum-iterations 1000000 \
	$WDIR/in.param

