#!/bin/bash -e

# slow-slip events along a single fault
# h* = 3750 m; W = 10 km; Ru = 2.66; Rb = 0.285

selfdir=$(dirname $0)

WDIR=$selfdir/test-results/output1

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir -p $WDIR
fi

N1=256
N2=128
DX=400

# build input file
cat <<EOF > "$WDIR/in.param"
# output directory
$WDIR
# elastic moduli (lambda, rigidity)
30e3 30e3
# time interval
3.15e10
# number of faults
1
# grid dimension (N1,N2)
$N1 $N2
# sampling (dx1,dx2)
$DX $DX
#   n  tau0   mu0   sig   a   b   L   Vo   G/(2Vs)   Vl  rake
$(echo "" | awk -v n1="$N1" -v n2=$N2 -v dx="$DX" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	BEGIN{
	c=1;
	tau0_p=-1;mu0_p =-1;sig_p=-1;
	a_p=-1;b_p=-1;L_p=-1;
	Vo_p=-1;damping_p=-1;
	Vl_p=-1;rake_p=-1;dirichlet_p="T";
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=(i2-n2/2)*dx; 
			tau0=-1;
			L=0.05;
			a=1e-2; 
			b=(abs(x1)<37.5e3 && abs(x2)<5e3)?a+4.0e-3:a-4e-3;
			mu0=0.6; 
			sig=1e2; 
			Vo=1e-6; 
			Vl=1e-9;
			rake=0;
			dirichlet=((i2<10) || (i2>=(n2-10)) || (i1<10) || (i1>=(n1-10)))?"T":"F";
			if (1==boxcar((x1+20e3)/10e3)*boxcar((x2-0e3)/10e3)){
				tau0=mu0*sig*(1.1e-9/Vo)^(a/mu0)*(L/Vl)^(b/mu0);
			}
			if ((tau0_p == tau0) && (mu0_p==mu0) && (sig_p==sig) && (a_p==a) && (b_p==b) && (L_p==L) && 
			    (Vo_p==Vo) && (damping_p==damping) && (Vl_p==Vl) && (rake_p==rake) && (dirichlet_p==dirichlet)){
				printf "%5d\n",-c;
			} else {
				printf "%5d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d %10.2e %10.2e %s\n", 
	         			c,  tau0,   mu0,   sig,     a,     b,     L,    Vo, 5, Vl, rake, dirichlet;
			}
			c++;
			tau0_p=tau0;mu0_p=mu0;sig_p=sig;
			a_p=a;b_p=b;L_p=L;
			Vo_p=Vo;damping_p=damping;
			Vl_p=Vl;rake_p=rake;dirichlet_p=dirichlet;
		}
	}
}')
# number of observation patches
1
# n fault i1 i2 rate
  1     1 `echo "" | awk -v n1=$N1 -v n2=$N2 '{print n1/2+1,n2/2+1,1}'`
# number of profiles
2
# n fault index direction rate
  1     1  `echo $N2 | awk '{print int($1/2)+1}'` 1 20
  2     1  `echo $N1 | awk '{print int($1/2)+1}'` 2 20
# number of events
0
EOF

mpirun -n 2 --allow-run-as-root motorcycle-3d-ratestate $* \
	--verbose 1 \
	--epsilon 1e-6 \
	--friction-law 1 \
	--export-netcdf \
	--export-state \
	--maximum-step 3.15e7 \
	--maximum-iterations 19000 \
	$WDIR/in.param

# test expected files
ls $WDIR/fault-01-i1-0129-log10v.grd $WDIR/fault-01-i2-0065-log10v.grd $WDIR/fault-01-index-0000001-log10v.grd 

# visualize output
gnuplot <<EOF
set term dumb
set logscale y
set ylabel 'Velocity (m/s)'
set xlabel 'Time (s)'
plot '$WDIR/time.dat' using (\$1/3.15e7):3 with line title ''
EOF

echo "# test successful" | tee $WDIR/report.xml

