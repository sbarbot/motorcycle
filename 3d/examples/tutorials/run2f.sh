#!/bin/bash -e

# earthquake cycles along 2 parallel faults
# separated by 15 km.
# h* = 375 m; W = 5 km; Ru = 12; Rb = 0.285

selfdir=$(dirname $0)

WDIR=$selfdir/output2f

if [ ! -e $WDIR ]; then
	echo "# adding directory $WDIR"
	mkdir "$WDIR"
fi

N1=512
N2=512
DX=25

if [ "0" == "1" ]; then
cat <<EOF > "$WDIR/in.param"
# output directory
$WDIR
# elastic moduli (lambda, rigidity)
30e3 30e3
# time interval
4e9
# number of faults
2
# grid dimension (N1,N2)
$N1 $N2
# sampling (dx1,dx2)
$DX $DX
#   n  tau0   mu0   sig   a   b   L   Vo   G/(2Vs)   Vl  rake
$(echo "" | awk -v n1="$N1" -v n2="$N2" -v dx="$DX" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	BEGIN{
	c=1;
	tau0_p=-1;mu0_p =-1;sig_p=-1;
	a_p=-1;b_p=-1;L_p=-1;
	Vo_p=-1;damping_p=-1;
	Vl_p=-1;rake_p=-1;dirichlet_p="T";
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=(i2-n2/2)*dx; 
			tau0=-1;
			L=0.005; 
			a=1e-2; 
			b=(abs(x1)<2.5e3 && abs(x2)<2.5e3)?a+4.0e-3:a-4e-3;
			mu0=0.6; 
			sig=1e2; 
			Vo=1e-6; 
			Vl=1e-9;
			rake=0;
			dirichlet=((i2<10) || (i2>=(n2-10)) || (i1<10) || (i1>=(n1-10)))?"T":"F";
			if (1==boxcar((x1-0.5e3)/1e3)*boxcar((x2-0.5e3)/1e3)){
				tau0=mu0*sig*(1.1e-9/Vo)^(a/mu0)*(L/Vl)^(b/mu0);
			}
			if ((tau0_p == tau0) && (mu0_p==mu0) && (sig_p==sig) && (a_p==a) && (b_p==b) && (L_p==L) && 
			    (Vo_p==Vo) && (damping_p==damping) && (Vl_p==Vl) && (rake_p==rake) && (dirichlet_p==dirichlet)){
				printf "%5d\n",-c;
			} else {
				printf "%5d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d %10.2e %10.2e %s\n", 
	         			c,  tau0,   mu0,   sig,     a,     b,     L,    Vo, 5, Vl, rake, dirichlet;
			}
			c++;
			tau0_p=tau0;mu0_p=mu0;sig_p=sig;
			a_p=a;b_p=b;L_p=L;
			Vo_p=Vo;damping_p=damping;
			Vl_p=Vl;rake_p=rake;dirichlet_p=dirichlet;
		}
	}
}')
# distance from fault 1
15e3
#   n  tau0   mu0   sig   a   b   L   Vo   G/(2Vs)   Vl  rake
$(echo "" | awk -v n1="$N1" -v n2="$N2" -v dx="$DX" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	BEGIN{
	c=1;
	tau0_p=-1;mu0_p =-1;sig_p=-1;
	a_p=-1;b_p=-1;L_p=-1;
	Vo_p=-1;damping_p=-1;
	Vl_p=-1;rake_p=-1;dirichlet_p="T";
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=(i2-n2/2)*dx; 
			tau0=-1;
			L=0.005; 
			a=1e-2; 
			b=(abs(x1)<2.5e3 && abs(x2)<2.5e3)?a+4.0e-3:a-4e-3;
			mu0=0.6; 
			sig=1e2; 
			Vo=1e-6; 
			Vl=1e-9;
			rake=-1;
			dirichlet=((i2<10) || (i2>=(n2-10)) || (i1<10) || (i1>=(n1-10)))?"T":"F";
			if ((tau0_p == tau0) && (mu0_p==mu0) && (sig_p==sig) && (a_p==a) && (b_p==b) && (L_p==L) && 
			    (Vo_p==Vo) && (damping_p==damping) && (Vl_p==Vl) && (rake_p==rake) && (dirichlet_p==dirichlet)){
				printf "%5d\n",-c;
			} else {
				printf "%5d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d %10.2e %10.2e %s\n", 
	         			c,  tau0,   mu0,   sig,     a,     b,     L,    Vo, 5, Vl, rake, dirichlet;
			}
			c++;
			tau0_p=tau0;mu0_p=mu0;sig_p=sig;
			a_p=a;b_p=b;L_p=L;
			Vo_p=Vo;damping_p=damping;
			Vl_p=Vl;rake_p=rake;dirichlet_p=dirichlet;
		}
	}
}')
# number of observation patches
2
# n nFault i1 i2 rate
  1      1 $(echo "" | awk -v n1="$N1" -v n2="$N2" '{print n1/2+1,n2/2+1,1}')
  2      2 $(echo "" | awk -v n1="$N1" -v n2="$N2" '{print n1/2+1,n2/2+1,1}')
# number of profiles
4
# n fault index direction rate
  1     1  $(echo $N2 | awk '{print int($1/2)+1}') 1 20
  2     1  $(echo $N1 | awk '{print int($1/2)+1}') 2 20
  3     2  $(echo $N2 | awk '{print int($1/2)+1}') 1 20
  4     2  $(echo $N1 | awk '{print int($1/2)+1}') 2 20
# number of events
0
EOF

mpirun -n 2 motorcycle-3d-ratestate \
	--verbose 1 \
	--epsilon 1e-6 \
	--export-netcdf \
	--maximum-step 3.15e7 \
	--maximum-iterations 1000000 \
	--friction-law 1 \
	$WDIR/in.param


if command -v gmt &> /dev/null
then
# convert time steps (integer) to time series (year)
for IN in "$WDIR"/fault-??-i?-????-log10v.grd; do
        OUT="$WDIR"/$(basename "$IN" -log10v.grd)-time-log10v.grd
	echo "# converting $IN to $OUT"
	xRange=$(gmt grdinfo -C "$IN" | awk '{print $2"/"$3}')
	steps=$(gmt grdinfo -C "$IN" | awk '{print $5*20}')
	yRange=$(grep -v "#" "$WDIR"/time.dat | awk -v i="$steps" 'NR==i{print $1/3.15e7;next}')
	dt=$(echo "$yRange" | awk '{print $1/500}')
	join -o 1.2,2.2,1.3 <(gmt grd2xyz "$IN" | awk '{printf "%06d %06d %20.16f\n",$2,$1,$3}' | sort) \
		<(grep -v "#" "$WDIR"/time.dat | awk '0==NR%20 {printf "%06d %20.17f\n",NR/20,$1/3.15e7}') |
		gmt blockmode -E -I$DX/$dt -R$xRange/0/$yRange | awk '{print $1,$2,$NF}' | \
		gmt surface -I$DX/$dt -R$xRange/0/$yRange -G"$OUT"
	yRange=""
done
fi
fi

if command -v gnuplot &> /dev/null
then
OUT="$WDIR"/peak-velocity.ps
echo "# processing $OUT"
gnuplot <<EOF
set term postscript
set out '$OUT'
set logscale y
set xrange [20:100]
set xlabel 'Time (year)'
set ylabel 'Velocity (m/s)'
plot '$WDIR/time-weakening.dat' u (\$1/3.15e7):3 w l t 'F1', '' u (\$1/3.15e7):4 w l t 'F2'
EOF
fi

