#!/bin/bash -e

# slow slip along 2 parallel faults
# h* = 1500 m; W = 5 km; Ru = 3; Rb = 0.285

selfdir=$(dirname $0)

WDIR=$selfdir/output2

if [ ! -e "$WDIR" ]; then
	echo "adding directory $WDIR"
	mkdir -p "$WDIR"
fi

# number of fault patches
N1=128
N2=128
# patch size
DX=100

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (lambda, rigidity)
30e3 30e3
# time interval
3.15e10
# number of faults
2
# grid dimension (N1,N2)
$N1 $N2
# sampling (dx1,dx2)
$DX $DX
#   n  tau0   mu0   sig   a   b   L   Vo   G/(2Vs)   Vl  rake
$(echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	BEGIN{
	c=1;
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=(i2-n2/2)*dx; 
			# normal of initial shear traction (MPa); use -1 to use steady-state value
			tau0=-1;
			# characteristic weakening distance (m)
			L=0.02; 
			# rate-dependent parameter (unitless)
			a=1e-2; 
			# state-dependent parameter (unitless)
			b=(abs(x1)<2.5e3 && abs(x2)<2.5e3)?a+4.0e-3:a-4e-3;
			# reference coefficient of friction (unitless)
			mu0=0.6; 
			# effective normal stress (MPa)
			sig=1e2; 
			# reference slip-rate (m/s)
			Vo=1e-6; 
			# loading rate (m/s)
			Vl=1e-9;
			# rake angle for the loading rate (degree)
			rake=0;
			# radiation damping coefficient ( G/(2 Vs) in units of MPa/m*s )
			damping=5;
			# whether to apply Dirichlet boundary condition (constant velocity Vl)
			dirichlet=((i2<10) || (i2>=(n2-10)) || (i1<10) || (i1>=(n1-10)))?"T":"F";
			if (1==boxcar((x1-0.5e3)/1e3)*boxcar((x2-0.5e3)/1e3)){
				# initial stress triggers nucleation (MPa)
				tau0=mu0*sig*(1.1e-9/Vo)^(a/mu0)*(L/Vl)^(b/mu0);
			}
			printf "%5d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d %10.2e %10.2e %s\n", 
	         		c,  tau0,   mu0,   sig,     a,     b,     L,    Vo, damping, Vl, rake, dirichlet;
			c++;
		}
	}
}')
# distance from fault 1
15e3
#   n  tau0   mu0   sig   a   b   L   Vo   G/(2Vs)   Vl  rake
$(echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	BEGIN{
	c=1;
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=(i2-n2/2)*dx; 
			# normal of initial shear traction (MPa); use -1 to use steady-state value
			tau0=-1;
			# characteristic weakening distance (m)
			L=0.02; 
			# rate-dependent parameter (unitless)
			a=1e-2; 
			# state-dependent parameter (unitless)
			b=(abs(x1)<2.5e3 && abs(x2)<2.5e3)?a+4.0e-3:a-4e-3;
			# reference coefficient of friction (unitless)
			mu0=0.6; 
			# effective normal stress (MPa)
			sig=1e2; 
			# reference slip-rate (m/s)
			Vo=1e-6; 
			# loading rate (m/s)
			Vl=1e-9;
			# rake angle for the loading rate (degree)
			rake=-1;
			# radiation damping coefficient ( G/(2 Vs) in units of MPa/m*s )
			damping=5;
			dirichlet=((i2<10) || (i2>=(n2-10)) || (i1<10) || (i1>=(n1-10)))?"T":"F";
			printf "%5d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d %10.2e %10.2e %s\n", 
	         		c,  tau0,   mu0,   sig,     a,     b,     L,    Vo, damping, Vl, rake, dirichlet;
			c++;
		}
	}
}')
# number of observation patches
2
# n nFault i1 i2 rate
  1      1 $(echo "" | awk -v n1=$N1 -v n2=$N2 '{print n1/2+1,n2/2+1,1}')
  2      2 $(echo "" | awk -v n1=$N1 -v n2=$N2 '{print n1/2+1,n2/2+1,1}')
# number of profiles
4
# n fault index direction rate
  1     1  $(echo $N2 | awk '{print int($1/2)+1}') 1 20
  2     1  $(echo $N1 | awk '{print int($1/2)+1}') 2 20
  3     2  $(echo $N2 | awk '{print int($1/2)+1}') 1 20
  4     2  $(echo $N1 | awk '{print int($1/2)+1}') 2 20
# number of events
0
EOF

# use --verbose=2 to output all parameters
# use --verbose=1 to output only parameters different from previous patch
mpirun -n 2 motorcycle-3d-ratestate \
	--verbose 1 \
	--epsilon 1e-4 \
	--export-netcdf \
	--maximum-step 3.15e7 \
	--maximum-iterations 100000 \
	--friction-law 1 \
	$WDIR/in.param

# plot time series of peak velocity for faults 1 and 2 with gnuplot
if command -v gnuplot &> /dev/null
then
	gnuplot <<EOF
set terminal postscript
set out '$WDIR/peak-velocity.ps'
set logscale y
set xlabel 'Time (year)'
set ylabel 'Peak velocity (m/s)'
plot '$WDIR/time.dat' using (\$1/3.15e7):3 with line title 'Fault 1', '' using (\$1/3.15e7):4 with line title 'Fault 2'
EOF
fi

# plot time series of instantaneous velocity at center of faults 1 and 2 with gnuplot
if command -v gnuplot &> /dev/null
then
	gnuplot <<EOF
set terminal postscript
set out '$WDIR/fault-01-central-velocity.ps'
set xlabel 'Time (year)'
set ylabel 'Peak velocity log10(m/s)'
plot '$WDIR/patch-01-00065-00065.dat' using (\$1/3.15e7):8 with line title 'Fault 1 center', '$WDIR/patch-02-00065-00065.dat' using (\$1/3.15e7):8 with line title 'Fault 2 center'
EOF
fi

# plot time series of vertical and horizontal cross-sections
if command -v gmt &> /dev/null
then
../../../bash/grdmap.sh -x -H -p -12/1/0.1 -c cycles.cpt $WDIR/fault-01-i1-0065-log10v.grd
../../../bash/grdmap.sh -x -H -p -12/1/0.1 -c cycles.cpt $WDIR/fault-01-i2-0065-log10v.grd
fi

# plot snapshots of rupture dynamics for fault 1
if command -v gmt &> /dev/null
then
../../../bash/grdmap.sh -x -p -12/1/0.1 -c cycles.cpt $WDIR/fault-01-index-0001???-log10v.grd
fi

# plot snapshots of rupture dynamics for fault 2
if command -v gmt &> /dev/null
then
../../../bash/grdmap.sh -x -p -12/1/0.1 -c cycles.cpt $WDIR/fault-02-index-0001???-log10v.grd
fi

