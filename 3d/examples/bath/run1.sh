#!/bin/bash -e

trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# simulates thermal instabilities with velocity-strengthening properties.

WDIR="output1"

if [ ! -e "$WDIR" ]; then
	echo "# adding directory $WDIR"
	mkdir -p "$WDIR"
fi

N1=256
N2=128
DX=150
FW=1e3

# input file
cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# Lame parameter (MPa), rigidity (MPa), universal gas constant
40e3 40e3 8.314
# time interval (s)
1e10
# number of faults
1
# grid dimension (N1,N2)
$N1 $N2
# sampling (dx1,dx2)
$DX $DX
#   n  tau0   mu0   sig   a   b   L   Vo   G/(2Vs)   Vl  rake
$(echo "" | awk -v n1="$N1" -v n2="$N2" -v dx="$DX" -v fw="$FW" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	BEGIN{
	c=1;
	tau0_p=-1;mu0_p =-1;sig_p=-1;
	a_p=-1;b_p=-1;L_p=-1;
	Vo_p=-1;damping_p=-1;
	Vl_p=-1;rake_p=-1;dirichlet_p="T";
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=10e3+i2*dx; 
			tau0=-1;
			mu0=0.5449;
			sig=19.92;
			a=1.02e-2;
			b=6e-3;
        		L=3.1e-3;
			Vo=1e-6;
			Vl=1e-9;
			rake=0;
			dirichlet=((i2<10) || (i2>=(n2-10)) || (i1<10) || (i1>=(n1-10)))?"T":"F";
			if (1==boxcar((x1+5e3)/5e3)*boxcar((x2-(17e3+fw/2))/fw)){
				tau0=(mu0+(a-b)*log(1e-12/Vo))*sig;
			}
			if ((tau0_p == tau0) && (mu0_p==mu0) && (sig_p==sig) && (a_p==a) && (b_p==b) && (L_p==L) && 
			    (Vo_p==Vo) && (damping_p==damping) && (Vl_p==Vl) && (rake_p==rake) && (dirichlet_p==dirichlet)){
				printf "%5d\n",-c;
			} else {
				printf "%5d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d %10.2e %10.2e %s\n", 
	         			c,  tau0,   mu0,   sig,     a,     b,     L,    Vo, 5, Vl, rake, dirichlet;
			}
			c++;
			tau0_p=tau0;mu0_p=mu0;sig_p=sig;
			a_p=a;b_p=b;L_p=L;
			Vo_p=Vo;damping_p=damping;
			Vl_p=Vl;rake_p=rake;dirichlet_p=dirichlet;
		}
	}
}')
# thermal properties
# n        Q        H        D        W        w     rhoc       Tb
$(echo "" | awk -v dx="$DX" -v n1="$N1" -v n2="$N2" -v fw="$FW" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	BEGIN{
	c=1;
	Q_p=-1;H_p=-1;D_p=-1;
	W_p=-1;w_p=-1;rhoc_p=-1;
	Tb_p=-1;
	}{
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=10e3+i2*dx; 
			Q=90.03e3;
			H=59.75e3;
			D=7.43e-7;
			W=1.70;
			w=((abs(x1)<7.5e3) && (x2>=17e3) && (x3<(17e3+fw)))?1.4e-4:1e-0;
			rhoc=2.6;
			Tb=400+273.15;
	                if ( (Q_p==Q) && (H_p==H) && (D_p==D) && (W_p==W) && (w_p==w) && (rhoc_p==rhoc) && (Tb_p==Tb) ){
				printf "%5d\n",-c;
			} else {
				printf "%06d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e\n", 
	         			c, Q, H, D, W, w, rhoc, Tb;
			}
			c++;
			Q_p=Q;H_p=H;D_p=D;W_p=W;w_p=w;rhoc_p=rhoc;Tb_p=Tb;
		}
	}
}')
# number of observation patches
1
# n fault i1 i2 rate
  1     1 $(echo "" | awk -v n1=$N1 -v n2=$N2 '{print n1/2+1,n2/2+1,1}')
# number of profiles
2
# n fault index direction rate
  1     1  $(echo $N2 | awk '{print int($1/2)+1}') 1 20
  2     1  $(echo $N1 | awk '{print int($1/2)+1}') 2 20
# number of events
0
EOF

mpirun -n 2 motorcycle-3d-ratestate-bath $* \
	--verbose 1 --epsilon 1e-6 --friction-law 1 \
	--export-netcdf --export-state --export-temperature \
	--maximum-step 3.15e7 --maximum-iterations 100000 \
	$WDIR/in.param

