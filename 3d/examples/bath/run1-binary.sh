#!/bin/bash -e

trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# simulates thermal instabilities with velocity-strengthening properties.
# use GMT-compatible .grd file to input physical parameters

WDIR="output1-binary"

if [ ! -e "$WDIR" ]; then
	echo "# adding directory $WDIR"
	mkdir -p "$WDIR"
fi

N1=256
N2=128
DX=150
FW=1e3

# .grd file names
TAU0="$WDIR/tau0.grd"
MU0="$WDIR/mu0.grd"
SIG="$WDIR/sig.grd"
A="$WDIR/a.grd"
B="$WDIR/b.grd"
L="$WDIR/L.grd"
V0="$WDIR/Vo.grd"
DAMPING="$WDIR/damping.grd"
VL="$WDIR/Vl.grd"
RAKE="$WDIR/rake.grd"
DIRICHLET="$WDIR/bc.grd"
# thermodynamic properties
Q="$WDIR/Q.grd"
H="$WDIR/H.grd"
D="$WDIR/D.grd"
WD="$WDIR/WD.grd"
WS="$WDIR/ws.grd"
RHOC="$WDIR/rhoc.grd"
TB="$WDIR/Tb.grd"

echo "# initial shear stress $TAU0"
echo "" | awk -v n1="$N1" -v n2="$N2" -v dx="$DX" -v fw="$FW" '
	function abs(x){return (x>0)?x:-x};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# coordinates
			x1=(i1-1-n1/2)*dx; 
			x2=10e3+(i2-1)*dx; 
			# effective normal stress (MPa)
			sig=19.92;
			# reference coefficient of friction (unitless)
			mu0=0.5449;
			# rate-dependent parameter (unitless)
			a=1.02e-2;
			# state-dependent parameter (unitless)
			b=6e-3;
			# characteristic weakening distance (m)
        		L=3.1e-3;
			# reference slip-rate (m/s)
			Vo=1e-6; 
			# loading rate (m/s)
			Vl=1e-9;
			# normal of initial shear traction (MPa); use -1 to use steady-state value
			if (1==boxcar((x1+5e3)/5e3)*boxcar((x2-(17e3+fw/2))/fw)){
				# initial stress triggers nucleation (MPa)
				tau0=(mu0+(a-b)*log(1e-12/Vo))*sig;
			} else {
				tau0=-1;
			}
			printf "%5d %5d %13.5e\n",i1,i2,tau0;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$TAU0"

echo "# reference friction coefficient $MU0"
echo "" | awk -v n1="$N1" -v n2="$N2" -v dx="$DX" '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# reference coefficient of friction (unitless)
			mu0=0.5449;
			printf "%5d %5d %13.5e\n",i1,i2,mu0;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$MU0"

echo "# normal stress $SIG"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# effective normal stress (MPa)
			sig=19.92;
			printf "%5d %5d %13.5e\n",i1,i2,sig;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$SIG"

echo "# friction parameter $A"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# rate-dependent parameter (unitless)
			a=1.02e-2;
			printf "%5d %5d %13.5e\n",i1,i2,a;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$A"

echo "# friction parameter $B"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '
	function abs(x){return (x>0)?x:-x};
	{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# state-dependent parameter (unitless)
			b=6e-3;
			printf "%5d %5d %13.5e\n",i1,i2,b;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$B"

echo "# characteristic weakening distance $L"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# characteristic weakening distance (m)
        		L=3.1e-3;
			printf "%5d %5d %13.5e\n",i1,i2,L;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$L"

echo "# reference slip-rate $V0"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# reference slip-rate (m/s)
			Vo=1e-6; 
			printf "%5d %5d %13.5e\n",i1,i2,Vo;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$V0"

echo "# radiation damping coefficient $DAMPING"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# radiation damping coefficient ( G/(2 Vs) in units of MPa/m*s )
			damping=5;
			printf "%5d %5d %13.5e\n",i1,i2,damping;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$DAMPING"

echo "# long-term slip rate $VL"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# loading rate (m/s)
			Vl=1e-9;
			printf "%5d %5d %13.5e\n",i1,i2,Vl;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$VL"

echo "# rake of long-term slip rate $RAKE"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# rake angle for the loading rate (degree)
			rake=0;
			printf "%5d %5d %13.5e\n",i1,i2,rake;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$RAKE"

echo "# Dirichlet boundary condition $DIRICHLET"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# whether to apply Dirichlet boundary condition (constant velocity Vl)
			dirichlet=((i2<10) || (i2>=(n2-10)) || (i1<10) || (i1>=(n1-10)))?1:0;
			printf "%5d %5d %13.5e\n",i1,i2,dirichlet;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$DIRICHLET"

# # # # # # # # # # # #
# thermal properties
# # # # # # # # # # # #

echo "# activation energy $Q"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# activation energy
			Q=90.03e3;
			printf "%5d %5d %13.5e\n",i1,i2,Q;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$Q"

echo "# activation energy $H"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# activation energy
			H=59.75e3;
			printf "%5d %5d %13.5e\n",i1,i2,H;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$H"

echo "# thermal diffusivity $D"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# thermal diffusivity
			D=7.43e-7;
			printf "%5d %5d %13.5e\n",i1,i2,D;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$D"

echo "# membrane diffusion width $WD"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX '{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			# membrane diffusion width
			W=1.70;
			printf "%5d %5d %13.5e\n",i1,i2,W;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$WD"

echo "# active shear zone with $WS"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX -v fw=$FW '
	function abs(x){return (x>0)?x:-x};
	{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=10e3+i2*dx; 
			# active shear zone width
			w=((abs(x1)<7.5e3) && (x2>=17e3) && (x3<(17e3+fw)))?1.4e-4:1e-0;
			printf "%5d %5d %13.5e\n",i1,i2,w;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$WS"

echo "# active shear zone with $RHOC"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX -v fw=$FW '
	function abs(x){return (x>0)?x:-x};
	{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=10e3+i2*dx; 
			# volumetric heat capacity
			rhoc=2.6;
			printf "%5d %5d %13.5e\n",i1,i2,rhoc;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$RHOC"

echo "# active shear zone with $TB"
echo "" | awk -v n1=$N1 -v n2=$N2 -v dx=$DX -v fw=$FW '
	function abs(x){return (x>0)?x:-x};
	{
	for (i2=1;i2<=n2;i2++){
		for (i1=1;i1<=n1;i1++){
			x1=(i1-n1/2)*dx; 
			x2=10e3+i2*dx; 
			# bath temperature
			Tb=400+273.15;
			printf "%5d %5d %13.5e\n",i1,i2,Tb;
		}
	}
}' | gmt xyz2grd -I1 -R1/$N1/1/$N2 -G"$TB"

# input file
cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# Lame parameter (MPa), rigidity (MPa), universal gas constant
40e3 40e3 8.314
# time interval (s)
1e10
# number of faults
1
# grid dimension (N1,N2)
$N1 $N2
# sampling (dx1,dx2)
$DX $DX
# # # # # # # # # # # #
# frictional properties
# # # # # # # # # # # #
# .grd file for tau0
$TAU0
# .grd file for mu0
$MU0
# .grd file for sigma
$SIG
# .grd file for a
$A
# .grd file for b
$B
# .grd file for L
$L
# .grd file for Vo
$V0
# .grd file for G/(2Vs)
$DAMPING
# .grd file for Vl
$VL
# .grd file for rake
$RAKE
# .grd file for boundary condition
$DIRICHLET
# # # # # # # # # # # #
# thermal properties
# # # # # # # # # # # #
# .grd file for Q
$Q
# .grd file for H
$H
# .grd file for D
$D
# .grd file for W
$WD
# .grd file for w
$WS
# .grd file for rhoc
$RHOC
# .grd file for Tb
$TB
# number of observation patches
1
# n fault i1 i2 rate
  1     1 $(echo "" | awk -v n1=$N1 -v n2=$N2 '{print n1/2+1,n2/2+1,1}')
# number of profiles
2
# n fault index direction rate
  1     1  $(echo $N2 | awk '{print int($1/2)+1}') 1 20
  2     1  $(echo $N1 | awk '{print int($1/2)+1}') 2 20
# number of events
0
EOF

mpirun -n 2 motorcycle-3d-ratestate-bath $* \
	--grd-input \
	--verbose 1 --epsilon 1e-6 --friction-law 1 \
	--export-netcdf --export-state --export-temperature \
	--maximum-step 3.15e7 --maximum-iterations 100000 \
	$WDIR/in.param

