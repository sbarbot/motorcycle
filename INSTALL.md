# BUILD

Before building Motorcycle, you must have

  * Fortran compiler (gcc-9 and above) (https://gcc.gnu.org/)
  * netcdf and netcdff libraries (https://www.unidata.ucar.edu/software/netcdf/)
  * fftw3 library (https://www.fftw.org/)
  * openmpi library (https://www.open-mpi.org/)

The compilation is conducted with *CMake* or *Makefile*. To compile and install all the binary files at once, use

    mkdir -p release && cd release
    cmake ..
    make
    make install

For a faster build, use `make -jN`, where N is the number of parallel threads. For an even faster build with *Ninja*, use

    mkdir -p release && cd release
    cmake -G Ninja ..
    ninja
    ninja install

Alternatively, use individual `makefiles`. To conform with your system configuration, make a copy of a template Makefile and link with a generic name. For example,

    cd motorcycle/2d/antiplane
    cp makefile_brew_serial makefile_my
    ln -s makefile_my makefile

Then, update the makefile with your system preferences (e.g., `vi makefile`), including compiler name, include and library paths. Finally, build the *motorcycle-ap-ratestate-serial*, *motorcycle-ap-ratestate-cz-serial*, and *motorcycle-ap-ratestate-bath-serial* codes with

    make all

The binary files will be located in `motorcycle/2d/antiplane/build/`. The `-serial` suffix indicates serial or shared-memory parallelism with *OpenMP*. Shared Fortran module files will be placed in `motorcycle/share/lib`.

To compile the remaining binaries, repeat the operation with

    cd motorcycle/2d/planestrain
    cp makefile_brew_serial makefile_my
    ln -s makefile_my makefile
    make all

For 3d calculations, compile the parallel implementation with

    cd motorcycle/3d
    cp makefile_brew makefile_my
    ln -s makefile_my makefile
    make all

and the 3d serial implementation with

    cd motorcycle/3d-serial
    cp makefile_brew makefile_my
    ln -s makefile_my makefile
    make 

The binary files will be located in the corresponding `build` directories.

# VISUALIZATION

For visualization purposes, it is recommended to install

  * gnuplot (http://www.gnuplot.info/)
  * GMT6 (http://gmt.soest.hawaii.edu/doc/latest/gmt.html)
  * xpdf (https://www.xpdfreader.com/)

The simulation data consist of netcdf binary files compatible with the Generic Mapping Tools version 4.5 and above, with the `.grd` file extension. Additional simulation data is exported in ASCII format in files with the `.dat` extension.

The `.grd` files can be plotted rapidly using GMT5 and above using the bash script `grdmap.sh` located in the `bash` directory. For example,

    grdmap.sh -H -p -12/1/0.01 -c cycles.cpt fault-01-i1-0129-log10v.grd

produces the postscript file `fault-01-i1-0129-log10v.grd-plot.ps` and the pdf file `fault-01-i1-0129-log10v.grd-plot.pdf` containing time series of cross-section of a fault plane (or all the fault plane for two-dimensional models in anti-plane and in-plane strain conditions.) If the program `xpdf` is available, the `.pdf` file will be loaded automatically, unless the `-x` option is added.

    grdmap.sh -p -12/1/0.01 -c cycles.cpt fault-01-index-0000201-log10v.grd

will produce the postscript and pdf files of a snapshot of the entire fault 1. Multiple snapshots can be animated into a movie of rupture dynamics using standard tools such as `ffmpeg`.

Time series of peak velocity on fault 1 can always be visualized with `gnuplot` using

    > gnuplot
    set logscale y
    plot 'time.dat' using 1:3 with lines

Some tutorial scripts produce postscript and pdf files automatically.

# EXECUTION

Example input files are located in the `examples/tutorials` directories. To run the bash scripts, add the code path to the `PATH` environment variable,

    export PATH=$PATH:motorcycle/2d/antiplane/build
    export PATH=$PATH:motorcycle/2d/planestrain/build
    export PATH=$PATH:motorcycle/3d/build

Then, run the bash scripts. For two-dimensional models in antiplane strain, the bash scripts `run1.sh` and `run2.sh` simulate seismic cycles on a single fault and on two parallel faults, respectively.

    cd motorcycle/2d/antiplane/examples/tutorials
    ./run1.sh
    ./run2.sh

For two-dimensional models in condition of in-plane strain, the bash scripts `run1.sh` and `run2.sh` simulate seismic cycles on more unstable faults, producing more complex sequences of frictional instabilities for a single fault and two parallel faults, respectively.

    cd motorcycle/2d/planestrain/examples/tutorials
    ./run1.sh
    ./run2.sh

For three-dimensional models, `run1.sh` computes cycles of slow-slip events on a single fault; `run2.sh` computes cycles of slow-slip events on two parallel faults. (Because slow-slip events are conveniently faster to simulate than earthquakes, they are used in tutorials of three-dimensional models.)

    cd motorcycle/3d/examples/tutorials
    ./run1.sh
    ./run2.sh

# DOXYGEN

You can generate a doxygen (https://doxygen.nl/) interface with the command

    doxygen ./doxygen

in the directories `2d/antiplane`, `2d/planestrain`, and `3d`. Other documentation is available in the man and markdown formats.
